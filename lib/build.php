<?php
include_once (__DIR__ . '/../lib/content.php');
include_once (__DIR__ . '/../lib/image.php');

/**
 * Deployment Build
 */
class Build
{
	private $errors;
	
	private $warnings;
	
	private $messages;
	
	private $tags;
	
	private $sitemap;
	
	private $sitemaps_pages;
	
	private $maps;
	
	private $extras;
	
	private $lang;
	private $lang_mapping;
	private $lang_mapping_reverse;
	private $labels;
	
	private $content_dir;
	private $build_dir;
	
	/**
	 * @var Content
	 */
	private $c;
	
	function __construct()	{
		$this->errors = array();
		$this->warnings = array();
		$this->messages = array();
	}
	
	private function InitLang($lang) {
		$this->tags = array();
		$this->extras = array();
		$this->sitemap = new stdClass();
		$this->sitemap->nodes = array();
		$this->sitemap->links = array();
		$this->sitemaps_pages = array();
		$this->maps = array();
		$this->messages[] = "";
		$this->messages[] = "## $lang";
		$this->c = new Content($lang,false,false);
		$this->lang = $lang;
		$this->content_dir = CONTENT_DIR . '/' . $lang;
		$this->build_dir = BUILD_DIR . '/' . $lang;
		$this->lang_mapping = array();
		$this->lang_mapping_reverse = array();
		$this->labels = json_decode(file_get_contents("{$this->content_dir}/labels.json"));
		
		if($lang != 'en') {
			$mapping = json_decode(file_get_contents("{$this->content_dir}/mapping.json"));
			foreach($mapping as $keylang => $pagelang) {
				$this->lang_mapping[$keylang] = $pagelang;
				$this->lang_mapping_reverse[$pagelang] = $keylang;
			}
		} else {
			$mapping = json_decode(file_get_contents(CONTENT_DIR . '/it/mapping.json'));
			foreach($mapping as $keylang => $pagelang) {
				$this->lang_mapping[$keylang] = $pagelang;
				$this->lang_mapping_reverse[$keylang] = $pagelang;
			}
		}
		if(!file_exists(BUILD_DIR . "/sitemaps")) {
			mkdir(BUILD_DIR . "/sitemaps", 0777, true);
		}
		if(!file_exists("{$this->build_dir}/tags")) {
			mkdir("{$this->build_dir}/tags", 0777, true);
		}
		if(!file_exists("{$this->build_dir}/extra")) {
			mkdir("{$this->build_dir}/extra", 0777, true);
		}
	}
	
	public function Run($lang) {
		$this->messages = array();
		$this->InitLang($lang);
		$francesco = $this->NavPageFromFile('francesco');
		$about = $this->NavPageFromFile('about');
		$archive = $this->NavPageFromFile('archive');
		$projects = $this->NavPageFromFile('projects');
		$oud = $this->NavPageFromFile('oud');
		$events = $this->NavPageFromFile('events');
		$home = $this->NavHome();

		$home_extra = new stdClass();
		$home_extra->section = 'home';
		$home_extra->article = 'index';
		$home_extra->items = array();
		$home_extra->items[] = $about;
		$home_extra->items[] = $francesco;

		$nav = new stdClass();
		
		// manual nav for home
		$nav->home = new stdClass();
		$nav->home->index = $this->NavHome();
		$nav->home->index->down = clone($about);
		$nav->home->index->right = clone($projects);
		$nav->home->index->sequence = 0;
		$nav->home->pin = $this->NavHome();
		$nav->home->pin->down = clone($about);
		$nav->home->pin->right = clone($projects);
		$nav->home->pin->sequence = 0;
		
		// Posts
		$nav->posts = new stdClass();
		$posts = $this->LoadArticles('posts',1);
		if(count($posts) > 0) {
			$sitemaps_urls = array();
			$sitemaps_docs = array();
			ksort($posts);
			$counter = 0;
			$prev_post = null;
			foreach($posts as $ts=>$post) {
				$post_nav = $this->NavPage('posts',$post);
				// All posts link about
				$post_nav->down = clone($about);
				if($counter>0) {
					$post_nav->left = $this->NavPage('posts',$prev_post);
				}
				$prev_post = $post;
				$post_nav->sequence = count($posts) - $counter;
				$counter++;
				if($counter<count($posts)) {
					$keys = array_flip(array_keys($posts));
					$values = array_values($posts);
					$next = $values[$keys[$ts]+1];
					$post_nav->right = $this->NavPage('posts',$next);
				} else {
					$post_nav->right = clone($home);
				}
				$nav->posts->{$post->filename} = $post_nav;
				if(isset($post->tags) && is_array($post->tags) && count($post->tags)>0) {
					foreach($post->tags as $tag) {
						$this->TagAdd($tag,'posts',$post->filename);
					}
				}
				$sitemaps_urls[] = array('url' => HOST_LIVE . $post_nav->url, 'ts' => $ts);
				preg_match_all("/\(\/docs\/([^\)]*)\)/", $post->markdown, $matches);
				if(isset($matches[1]) && is_array($matches[1]) && count($matches[1]) > 0) {
					for($j = 0; $j < count($matches); $j++) {
						if(isset($matches[1][$j])) {
							$sitemaps_docs[] = array('url' => HOST_LIVE . '/docs/' . $matches[1][$j], 'ts' => $ts);
						}
					}
				}
				if(isset($post->extra)) {
				    $extra_post_file = "{$this->content_dir}/extra/$post->extra.json";
				    $extra_post = json_decode(file_get_contents($extra_post_file));
				    $post_extra = new stdClass();
				    $post_extra->section = 'posts';
				    $post_extra->article = $post->filename;
				    $post_extra->items = $extra_post;
				    $this->extras[] = $post_extra;
				}
			}
			$this->messages[] = "Posts: " . count($posts);
			$this->maps[] = $this->WriteMap($sitemaps_urls, 'posts');
			$this->maps[] = $this->WriteMap($sitemaps_docs, 'docs');
			$rss = $this->Rss($posts);
		} else {
			$this->errors[] = "No posts";
		}

		$post->title_short = 'Posts';
		$nav->home->index->left = $this->NavPage('posts',$post);
		$nav->home->pin->left = $this->NavPage('posts',$post);
				
		$home_extra->items[] = $this->NavPage('posts',$post);
		$home_extra->items[] = $projects;
		$home_extra->items[] = $oud;
		$home_extra->items[] = $events;
		$home_extra->items[] = $archive;
		
		// take all archive posts and sort them by year
		// Archive
		$nav->archive = new stdClass();
		$tot_posts = count($posts);
		$posts = $this->LoadArticles('archive',-1);
		$tot_archive = count($posts);
		if(count($posts) > 0) {
			$sitemaps_urls = array();
			ksort($posts);
			foreach($posts as $ts=>$post) {
				$post_nav = $this->NavPage('archive',$post);
				$post_nav->up = clone($archive);
				$post_nav->sequence = -1;
				$nav->archive->{$post->filename} = $post_nav;
				if(isset($post->tags) && is_array($post->tags) && count($post->tags)>0) {
					foreach($post->tags as $tag) {
						$this->TagAdd($tag,'archive',$post->filename);
					}
				}
				$sitemaps_urls[] = array('url' => HOST_LIVE . $post_nav->url, 'ts' => $ts);
			}
			$this->messages[] = "Archive: " . count($posts);
			$this->maps[] = $this->WriteMap($sitemaps_urls, 'archive');
				
			// archive home
			$archive_nav = clone($archive);
			$archive_nav->up = clone($projects);
			$archive_nav->sequence = -1;
			$nav->archive->index = $archive_nav;
		} else {
			$this->errors[] = "No archive";
		}

		// manual nav for about
		$nav->about = new stdClass();
		$nav->about->index = clone($about);
		$nav->about->index->up = clone($home);
		$nav->about->index->down = clone($francesco);
		$nav->about->index->left = $this->NavPageFromFile('about','info');
		$nav->about->index->right = $this->NavPageFromFile('about','contact');

		$nav->about->index->sequence = 0;
		$nav->about->info = $this->NavPageFromFile('about','info');
		$nav->about->info->right = clone($about);
		$nav->about->info->sequence = 1;
		$nav->about->contact = $this->NavPageFromFile('about','contact');
		$nav->about->contact->left = clone($about);
		$nav->about->contact->sequence = -1;
		
		// manual nav for francesco
		$nav->francesco = new stdClass();
		$nav->francesco->index = clone($francesco);
		$nav->francesco->index->up = clone($about);
		$nav->francesco->index->left = $this->NavPageFromFile('francesco','gallery');
		$nav->francesco->index->right = $this->NavPageFromFile('francesco','press');
		
		$nav->francesco->index->sequence = 0;
		$nav->francesco->gallery = $this->NavPageFromFile('francesco','gallery');
		$nav->francesco->gallery->right = clone($francesco);
		$nav->francesco->gallery->sequence = 1;
		$nav->francesco->press = $this->NavPageFromFile('francesco','press');
		$nav->francesco->press->left = clone($francesco);
		$nav->francesco->press->sequence = -1;
		
		// manual nav for projects
		$nav->projects = new stdClass();
		$nav->projects->index = clone($projects);
		$nav->projects->index->left = clone($home);
		$nav->projects->index->right = clone($oud);
		$nav->projects->index->down = clone($archive);
		$nav->projects->index->up = $this->NavPageFromFile('projects','composition');

		$nav->projects->index->sequence = -1;
		$nav->projects->composition = $this->NavPageFromFile('projects','composition');
		$nav->projects->composition->down = clone($projects);
		$nav->projects->composition->sequence = -1;
		
		// manual nav for oud
		$nav->oud = new stdClass();
		$nav->oud->index = clone($oud);
		$nav->oud->index->left = clone($projects);
		$nav->oud->index->right = clone($events);
		$nav->oud->index->up = $this->NavPageFromFile('oud','materials');
		$nav->oud->index->down = $this->NavPageFromFile('oud','lessons');
		$nav->oud->index->sequence = -2;

		$nav->oud->lessons = $this->NavPageFromFile('oud','lessons');
		$nav->oud->lessons->up = clone($oud);
		$nav->oud->lessons->sequence = -2;
		$nav->oud->materials = $this->NavPageFromFile('oud','materials');
		$nav->oud->materials->down = clone($oud);
		$nav->oud->materials->sequence = -2;

		// manual nav for events
		$nav->events = new stdClass();
		$nav->events->index = clone($events);
		$nav->events->index->left = clone($oud);
		$nav->events->index->down = $this->NavPageFromFile('events','past');
		$nav->events->index->sequence = -3;

		$nav->events->past = $this->NavPageFromFile('events','past');
		$nav->events->past->up = clone($events);
		$nav->events->past->sequence = -3;

		// default
		$nav->default = new stdClass();
		$nav->default->index = new stdClass();
		$nav->default->index->left = clone($home);
		$nav->default->index->sequence = -1;
		
		// tags
		$nav->tags = new stdClass();
		$nav->tags->index = $this->NavHome('Tags','tags');
		$nav->tags->index->down = clone($home);
		$nav->tags->index->sequence = 0;
		$home_tags = $this->NavHome('Tags','tags');
				
		// extra
		$archive = $this->LoadArticles('archive');
		krsort($archive);
		$archive_extra = new stdClass();
		$archive_extra->section = 'archive';
		$archive_section = 'archive';
		$archive_extra->article = 'index';
		$archive_extra->items = array();
		$this->PageLang($archive_section);
		foreach($archive as $ts=>$archive_post) {
			$item = new stdClass();
			$item->title = $archive_post->title;
			$item->image = $this->ArticleListingImage($archive_post, 'archive', $archive_post->filename);
			$this->PageLang($archive_post->filename);
			$item->url = "/{$this->lang}/$archive_section/{$archive_post->filename}/";
			$item->description = $archive_post->description;
			$archive_extra->items[] = $item;
		}
		$this->extras[] = $archive_extra;
		
		// events
		$events_array = json_decode(file_get_contents("{$this->content_dir}/events.json"));
		$events_extra = new stdClass();
		$events_extra->section = 'events';
		$events_extra->article = 'index';
		$events_extra->items = array();
		$events_past_extra = new stdClass();
		$events_past_extra->section = 'events';
		$events_past_extra->article = 'past';
		$events_past_extra->items = array();
		// leave events for 5 days
		$ts_now = time() - 5 * 86400;
		foreach($events_array as $event) {
			$ts = strtotime($event->date);
			$event->ts = $ts;
			if(!isset($event->caption)) {
				$event->caption = $event->title;
			}
			$event->description = $this->c->Markdown2Html($event->description );
			if($ts > $ts_now) {
				$events_extra->items[] = $event; 
			} else {
				$events_past_extra->items[] = $event;
			}
		}
		$this->extras[] = $events_extra;
		$this->extras[] = $events_past_extra;
		$this->messages[] = "Events: " . count($events_array);
		
		// press
		$press_array = json_decode(file_get_contents("{$this->content_dir}/press.json"));
		$press_extra = new stdClass();
		$press_extra->section = 'francesco';
		$press_extra->article = 'press';
		$press_extra->items = array();
		foreach($press_array as $press) {
			$ts = strtotime($press->date);
			$press->ts = $ts;
			if(isset($press->post)) {
				$press->post_data = json_decode(file_get_contents("{$this->content_dir}/posts/{$press->post}.json")); 
			}
			$press_extra->items[] = $press; 
		}
		$this->extras[] = $press_extra;
		$this->extras[] = $events_past_extra;
		$this->messages[] = "Press: " . count($press_array);
		
		// gallery
		$gallery_extra = new stdClass();
		$gallery_extra->section = 'francesco';
		$gallery_extra->article = 'gallery';
		$gallery_extra->items = array();
		$gallery_files = glob(__DIR__ . '/../content/images/g/*.*');
		foreach($gallery_files as $gallery_file) {
			$gallery_filename = pathinfo($gallery_file,PATHINFO_BASENAME);
			$exif = @exif_read_data($gallery_file,'IFD0',true);
			if(!is_null($exif) && isset($exif['IFD0']['ImageDescription']) ) {
				$comment = trim($exif['IFD0']['ImageDescription']);
				if(isset($exif['IFD0']['Artist']) && strlen($exif['IFD0']['Artist'])>0) {
					$comment .= " - {$this->labels->photo_by} {$exif['IFD0']['Artist']}";
				}
				$gallery_extra->items[] = (object)["image"=>"g/$gallery_filename","title"=>$comment];
			} else {
				$this->errors[] = "Missing EXIF data in g/$gallery_filename";
			}
		}
		$this->messages[] = "Gallery: " . count($gallery_extra->items);
		$this->extras[] = $gallery_extra;

		// pins
		$pin_extra = new stdClass();
		$pin_extra->section = 'home';
		$pin_extra->article = 'pin';
		$pin_extra->items = array();
		$pin_files = glob(__DIR__ . '/../content/images/p/*.*');
		foreach($pin_files as $pin_file) {
			$pin_filename = pathinfo($pin_file,PATHINFO_BASENAME);
			$pin = pathinfo($pin_file,PATHINFO_FILENAME);
			$exif = @exif_read_data($pin_file,'IFD0',true);
			if(!is_null($exif) && isset($exif['IFD0']['ImageDescription']) && isset($exif['IFD0']['Artist'])) {
				$caption = trim($exif['IFD0']['ImageDescription']);
				$author = $exif['IFD0']['Artist'];
				$pin_obj = new stdClass();
				$pin_obj->caption = $caption;
				$pin_obj->author = $author;
				$image = new Image('pin',"p/{$pin_filename}");
				$pin_obj->width = $image->GetRealWidth();
				$pin_obj->height = $image->GetRealHeight();
				$pin_obj->size = number_format(filesize($pin_file)/(1024*1024),1);
				$pin_extra->items[$pin] = $pin_obj;
			} else {
				$this->warnings[] = "Missing EXIF data in p/$pin_filename";
			}
		}
		$this->extras[] = $pin_extra;
		$this->messages[] = "Pins: " . count($pin_extra->items);

		// Tags
		$tags_index = new stdClass();
		$tags_index->title = "Tags";
		$tags_index->level = 2;
		$tags_index->template = "tags";
		$tags_index->background = (object)["image"=>"sunset.jpg","style"=>"dark"];

		$home_extra->items[] = $this->NavPage('tags',$tags_index);

		$this->extras[] = $home_extra;
		
		$this->WriteFile("tags/index.json",json_encode($tags_index));
		$tags_index_extra = new stdClass();
		$tags_index_extra->items = array();
		
		$tags = array();
		$tags_array = json_decode(file_get_contents("{$this->content_dir}/tags.json"));
		$ontology = json_decode(file_get_contents("{$this->content_dir}/ontology.json"));
		
		$sitemaps_urls = array();
		foreach($tags_array as $tag_definition) {
			$url = HOST_LIVE . "/{$this->lang}/tags/{$tag_definition->tag}/";
			$sitemaps_urls[] = array('url' => $url, 'ts' => 0);
			$tags[$tag_definition->tag] = $tag_definition;
		}
		$this->maps[] = $this->WriteMap($sitemaps_urls, 'tags');
		
		foreach($this->tags as $tag=>$tagged_pages) {
			if(isset($tags[$tag])) {
				$tag_obj = $tags[$tag];
				$tag_obj->template = "tag";
				$tag_obj->level = 3;
				$tag_obj->sequence = 0;
				$related = array();
				if(isset($ontology->parents[0]->$tag) && is_array($ontology->parents[0]->$tag)) {
					$tag_obj->children = $ontology->parents[0]->$tag;
					foreach($ontology->parents[0]->$tag as $child) {
						if(isset($this->tags[$child])) {
							$related[] = $child;
						}
					}
				} else {
					foreach($ontology->parents as $parents) {
						foreach($parents as $parent=>$children) {
							if(in_array($tag, $children)) {
								// $tag_obj->parent = $parent;
								$related[] = $parent;
							}
						}
					}
				}
				foreach($ontology->relationships as $related_tags) {
					if(in_array($tag,$related_tags)) {
						foreach($related_tags as $related_tag) {
							if($related_tag!=$tag) {
								$related[] = $related_tag;
							}
						}
					}
				}
				$tag_obj->related = array_unique($related);
				$this->WriteFile("tags/{$tag}.json",json_encode($tag_obj));
				
				$tag_extra = new stdClass(); 
				$tag_extra_items = array();
				foreach($tagged_pages as $tagged_page) {
					$this->PageLang($tagged_page->section);
					$this->PageLang($tagged_page->article);
					$tagged_page_metadata = json_decode(file_get_contents("{$this->content_dir}/{$tagged_page->section}/{$tagged_page->article}.json"));
					// replace name (needed for SEO only)
					$tagged_page_metadata->description = str_replace(Constants::$TAGMETA[$this->lang],"",$tagged_page_metadata->description);
					$tagged_page_extra = $tagged_page_metadata;
					$tagged_page_extra->section = $tagged_page->section;
					$tagged_page_extra->article = $tagged_page->article;
					$tagged_page_extra->url = "/{$this->lang}/{$tagged_page->section}/{$tagged_page->article}/";
					$tagged_page_extra->image = $this->ArticleListingImage($tagged_page_metadata, $tagged_page->section, $tagged_page->article);
					$ts = strtotime($tagged_page_extra->date);
					$tag_extra_items[$ts] = $tagged_page_extra;
				}
				krsort($tag_extra_items);
				$tag_extra->items = array_values($tag_extra_items);
				$nav->tags->$tag = $this->NavPageFromFile('tags', $tag);
				$nav->tags->$tag->sequence = 0;
				$nav->tags->$tag->down = $home_tags;
				$tags_index_extra->items[] = (object)["tag"=>$tag,"weight"=> count($tagged_pages)];
				$this->WriteFile("extra/tags/{$tag}.json",json_encode($tag_extra));
			} else {
				$this->errors[] = "Tag $tag undefined in {$tagged_pages[0]->section}/{$tagged_pages[0]->article}";
			}
		}
		
		$this->WriteFile("extra/tags/index.json",json_encode($tags_index_extra));
		$this->messages[] = "Tags: " . count($this->tags);
		
		$help_md = file_get_contents("{$this->content_dir}/help.md");
		$help_html = $this->c->Markdown2Html($help_md);
		$this->WriteFile("help.html",$help_html);
		
		if($lang != 'en') {
			foreach(get_object_vars($nav) as $key=>$obj) {
				unset($nav->$key);
				$this->PageLang($key);
				foreach(get_object_vars($obj) as $key2=>$obj2) {
					unset($obj->$key2);
					$this->PageLang($key2);
					$obj->$key2 = $obj2;
				}
				$nav->$key = $obj;
			}
		}
		
		// feedback
		if(count($this->errors) > 0) {
			foreach($this->errors as $error) {
				echo "$error\n";
			}
			exit(42);
		} else {
			if(count($this->warnings) > 0) {
				foreach($this->warnings as $warning) {
					echo "[WARN] $warning\n";
				}
			}
			$this->WriteFile('nav.json',json_encode($nav));
			$this->SitemapCreate($nav);
			$this->maps[] = $this->WriteMap($this->sitemaps_pages, 'pages');
			$this->messages[] = "Sitemaps: " . count($this->maps);
			foreach($this->messages as $message) {
				echo "$message\n";
			}
			$this->WriteFile('sitemap.json',json_encode($this->sitemap));
			$this->WriteFile('posts.rss',$rss);
			foreach($this->extras as $extra) {
				$this->PageLang($extra->section);
				$this->PageLang($extra->article);
				$this->WriteFile("extra/{$extra->section}/{$extra->article}.json",json_encode($extra));
			}
			$this->SitemapsIndex($this->maps);
		}
	}

	private function ArticleListingImage($metadata,$section,$article) {
		$this->PageLang($section);
		$this->PageLang($article);
		$obj = new stdClass();
		$obj->section = $section;
		$obj->article = $article;
		$obj->metadata = $metadata;
		$markdown = file_get_contents("{$this->content_dir}/{$section}/{$article}.md");
		$content = $this->c->ContentRender($markdown,$obj);
		$image = $content->image;
		$image = str_replace('/inline/','/listitem/',$image);
		$image = str_replace('/small/','/listitem/',$image);
		return $image;
	}
	
	/**
	 * Load all content files in a specific directory
	 * 
	 * @param string $dir
	 * @return array
	 */
	private function LoadArticles($dir,$expected_level=null) {
		$this->PageLang($dir);
		$articles_files = glob("{$this->content_dir}/$dir/*.json");
		$articles = array();
		foreach($articles_files as $article_file) {
			$filename = pathinfo($article_file,PATHINFO_FILENAME);
			if($filename != 'index') {
				if(file_exists("{$this->content_dir}/$dir/{$filename}.md")) {
					$metadata = json_decode(file_get_contents($article_file));
					if(isset($metadata->background) && isset($metadata->background->image)) {
						if(!file_exists("{$this->content_dir}/../images/bg/{$metadata->background->image}")) {
							$this->errors[] = "Background {$metadata->background->image} not found for {$filename}.json";
						}
					}
					$markdown = file_get_contents("{$this->content_dir}/$dir/{$filename}.md");
					$json_error = json_last_error();
					if($json_error==0) {
						if(!(isset($metadata->hidden) && $metadata->hidden)) {
							if(isset($metadata->level)) {
								if(is_null($expected_level) || $metadata->level == $expected_level) {
									if(isset($metadata->date)) {
										$ts = strtotime($metadata->date);
										if($ts > 0) {
											$article = $metadata;
											$article->markdown = $markdown;
											$article->filename = $filename;
											$articles[$ts] = $article;
										} else {
											$this->errors[] = "Bad timestamp in {$filename}.json";
										}
									} else {
										$this->errors[] = "Date not set in {$filename}.json";
									}
								} else {
									$this->errors[] = "Level {$metadata->level} different from $expected_level in {$filename}.json";
								}
							} else {
								$this->errors[] = "Level not set in {$filename}.json";
							}
						}
					} else {
						$this->errors[] = "Bad json in {$filename}.json - $json_error";
					}
				} else {
					$this->errors[] = "Missing content file {$filename}.md";
				}
			}
		}
		return $articles;
	}
	
	private function NavHome($title='Home',$section='') {
		$nav_page = new stdClass();
		$nav_page->title = $title;
		$nav_page->section = ($section!=''? $section:'home');
		$nav_page->url = "/{$this->lang}/" . ($section!=''? "$section/":'');
		$nav_page->url_lang = $this->UrlLang($section,'');
		return $nav_page;
	}
	
	private function NavPage($section,$page) {
		$this->PageLang($section);
		$this->PageLang($page->filename);
		$nav_page = new stdClass();
		$nav_page->title = isset($page->title_short)? $page->title_short : $page->title;
		$nav_page->level = isset($page->level)? $page->level : 1;
		$article = isset($page->filename) && $page->filename!='index'? "{$page->filename}/" : '';
		$nav_page->url = "/{$this->lang}/{$section}/$article";
		$nav_page->section = $section;
		$nav_page->url_lang = $this->UrlLang($section,$article);
		return $nav_page;
	}
	
	private function UrlLang($section,$article) {
		$article = trim($article,'/');
		$section = $this->PageLangReverse($section);
		$article = $this->PageLangReverse($article);
		return '/' . ($this->lang=='en'?'it':'en') . '/' . ($section!=''? "$section/":'') . ($article!=''? "$article/":'');
	}
	
	private function NavPageFromFile($section,$file='index') {
		$this->PageLang($section);
		$this->PageLang($file);
		$metadata = json_decode(file_get_contents("{$this->content_dir}/$section/{$file}.json"));
		$metadata->filename = $file;
		if(!isset($metadata->description)) {
			$this->errors[] = "Missing description in $section/$file";
		}
		return $this->NavPage($section,$metadata);
	}
	
	private function PageLang(&$page) {
		if($this->lang != 'en' && $page!='') {
			if(isset($this->lang_mapping[$page])) {
				$page = $this->lang_mapping[$page];
			}
		}
	}

	private function PageLangReverse($page) {
		$page = trim($page,'/');
		if($page!='') {
			if(isset($this->lang_mapping_reverse[$page])) {
				$page = $this->lang_mapping_reverse[$page];
			}
		}
		return $page;
	}

	private function Rss($posts) {
		$today = date("r");
		$metadata = json_decode(file_get_contents("{$this->content_dir}/home/index.json"));

		$rss = "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>
<rss version=\"2.0\" xmlns:atom=\"http://www.w3.org/2005/Atom\">
	<channel>
		<title>Musilogue</title>
		<description>" . htmlspecialchars($metadata->description) . "</description>
		<image>
			<url>".HOST_LIVE."/images/small/c/musilogue.png</url>
			<title>Musilogue</title>
			<link>".HOST_LIVE."/{$this->lang}/</link>
		</image>				
		<language>".Constants::$LOCALES[$this->lang]."</language>
		<link>".HOST_LIVE."/</link>
		<lastBuildDate>$today</lastBuildDate>
		<pubDate>$today</pubDate>
		<ttl>1800</ttl>
		<atom:link href=\"".HOST_LIVE."/posts_{$this->lang}.rss\" rel=\"self\" type=\"application/rss+xml\" />";
		krsort($posts);
		foreach($posts as $ts=>$post) {
			$url = HOST_LIVE . "/{$this->lang}/posts/{$post->filename}/";
			$post_ts = date("r",$ts); 
			$rss .= "
		<item>
			<title>" . htmlspecialchars($post->title) . "</title>
			<description>" . htmlspecialchars($post->description) . "</description>
			<link>{$url}</link>
			<guid isPermaLink=\"true\">{$url}</guid>
			<pubDate>$post_ts</pubDate>
		</item>";
		}
		$rss .= "
	</channel>
</rss>";
		return $rss;
	}
	
	private function SitemapNodeAdd($index,$navnode) {
		$xyscale = 120;
		$this->PageLang($navnode->section);
		$this->PageLang($index);
		$node = new stdClass();
		$node->id = $index == 'index' ? $navnode->section : $index;
		$node->name = $navnode->title;
		$node->url = $navnode->url;
		$node->section = $navnode->section;
		$node->x = (1 - $navnode->sequence) * $xyscale + 50;
		$node->y = - ((isset($navnode->level)? $navnode->level : 1 ) * $xyscale);
		$this->sitemap->nodes[] = $node;
	}
	
	private function SitemapCreate($nav) {
		$topnavs = get_object_vars($nav);
		$index_only = Constants::$SITEMAP_NODES_NO_EXPAND[$this->lang];
		foreach($topnavs as $topnav) {
			$navnodes = get_object_vars($topnav);
			foreach($navnodes as $key=>$navnode) {
				if(isset($navnode->title) && isset($navnode->url)) {
					if(!in_array($navnode->section, $index_only) || $key == 'index') {
						$this->SitemapNodeAdd($key, $navnode);
						$this->sitemaps_pages[] = array('url' => HOST_LIVE . $navnode->url, 'ts' => 0);
					}
					if($navnode->section == 'posts' && $navnode->sequence == 1) {
						$navnode->title = 'Posts';
						$this->SitemapNodeAdd('posts', $navnode);
					}
				}
			}
		}
		$this->SitemapLinkAdd('home', 'about');
		$this->SitemapLinkAdd('home', 'projects');
		$this->SitemapLinkAdd('about', 'francesco');
		$this->SitemapLinkAdd('projects', 'oud');
		$this->SitemapLinkAdd('projects', 'archive');
		$this->SitemapLinkAdd('oud', 'events');
		$this->SitemapLinkAdd('about', 'info');
		$this->SitemapLinkAdd('about', 'contact');
		$this->SitemapLinkAdd('francesco', 'gallery');
		$this->SitemapLinkAdd('francesco', 'press');
		$this->SitemapLinkAdd('projects', 'composition');
		$this->SitemapLinkAdd('oud', 'lessons');
		$this->SitemapLinkAdd('oud', 'materials');
		$this->SitemapLinkAdd('events', 'past');
		$this->SitemapLinkAdd('home', 'tags');
		$this->SitemapLinkAdd('home', 'posts');
	}
	
	private function SitemapLinkAdd($source,$target) {
		$this->PageLang($source);
		$this->PageLang($target);
		$link = new stdClass();
		$link->source = $source;
		$link->target = $target;
		$this->sitemap->links[] = $link;
	}
	
 	private function SitemapsDateISO8601($timestamp=0) {
 		$timestamp = ($timestamp==0)? time() : $timestamp;
 		return date('Y-m-d\TH:i:s',$timestamp) . substr_replace(date('O',$timestamp),':',3,0);
 	}

	private function SitemapsIndex($maps) {
 		$index = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
 		$index .= "<sitemapindex xmlns=\"http://www.sitemaps.org/schemas/sitemap/0.9\">\n";
 		foreach($maps as $map) {
 			$index .= "	<sitemap>\n";
 			$index .= "		<loc>" . HOST_LIVE . '/' . $map . "</loc>\n";
 			$index .= "		<lastmod>" . $this->SitemapsDateISO8601() . "</lastmod>\n";
 			$index .= "	</sitemap>\n";
 		}
 		$index .= "</sitemapindex>\n";
		$this->WriteFile("../sitemaps/sitemap_{$this->lang}_index.xml", $index);
	}
	
	private function TagAdd($tag,$section,$article) {
		$this->PageLang($section);
		$this->PageLang($article);
		$newtag = new stdClass();
		$newtag->section = $section;
		$newtag->article = $article;
		if(!isset($this->tags[$tag])) {
			$this->tags[$tag] = array();
		}
		$this->tags[$tag][] = $newtag;
	}
	
	private function WriteFile($filename,$content,$zip=false) {
		$save_dir = dirname("{$this->build_dir}/{$filename}");
		if(!file_exists($save_dir)) {
			mkdir($save_dir, 0777, true);
		}
		if($zip) {
			$fp = gzopen("{$this->build_dir}/{$filename}", 'w');
			gzwrite($fp, $content);
			gzclose($fp);
		} else {
			$fp = fopen("{$this->build_dir}/{$filename}", 'w');
			flock($fp,LOCK_EX);
			fwrite($fp, $content);
			flock($fp,LOCK_UN);
			fclose($fp);
		}
	}	

	private function WriteMap($urls,$name) {
		$this->PageLang($name);
		$sitemap = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
		$sitemap .= "<urlset xmlns=\"http://www.sitemaps.org/schemas/sitemap/0.9\">\n";
		foreach($urls as $url)
		{
			$sitemap .= "	<url>\n";
			$sitemap .= "		<loc>{$url['url']}</loc>\n";
			$sitemap .= "	</url>\n";
		}
		$sitemap .= "</urlset>\n";
		$filename = "sitemaps/sitemap_{$this->lang}_{$name}.xml.gz";
		$this->WriteFile("../$filename", $sitemap, true);
		return $filename;
	}
 	
}
?>
