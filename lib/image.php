<?php
ini_set("memory_limit","80M");

/**
 * Image Management
 */
class Image
{
	private $orig_file;

	private $cached_file;
	
	private $recipes;
	
	private $recipes_desktop = array(
			'full' => array('mode'=>'0','width'=>1920,'height'=>1920),
			'listitem' => array('mode'=>'2','width'=>200,'height'=>200),
			'inline' => array('mode'=>'0','width'=>300,'height'=>300),
			'inlinevert' => array('mode'=>'0','width'=>200,'height'=>400),
			'inlinezoom' => array('mode'=>'0','width'=>800,'height'=>800),
			'gallery' => array('mode'=>'0','width'=>300),
			'pin' => array('mode'=>'0','width'=>800,'height'=>500),
			'small' => array('mode'=>'0','width'=>120,'height'=>120),
			'orig' => array()
	);
	
	private $recipes_mobile = array(
			'full' => array('mode'=>'0','width'=>800,'height'=>800),
			'listitem' => array('mode'=>'2','width'=>200,'height'=>200),
			'inline' => array('mode'=>'0','width'=>300,'height'=>300),
			'inlinevert' => array('mode'=>'0','width'=>200,'height'=>400),
			'inlinezoom' => array('mode'=>'0','width'=>800,'height'=>800),
			'gallery' => array('mode'=>'0','width'=>300),
			'pin' => array('mode'=>'0','width'=>800,'height'=>500),
			'small' => array('mode'=>'0','width'=>80,'height'=>80),
			'orig' => array()
	);
	
	private $recipe;
	
	private $jpg_quality;
	
	function __construct($recipe,$file,$is_mobile=false) {
		$this->recipes = $is_mobile? $this->recipes_mobile : $this->recipes_desktop;
		$this->jpg_quality = $is_mobile? 40 : 60;
		$this->recipe = $recipe;
		$this->orig_file = __DIR__ . "/../content/images/$file";
		$this->cached_file = __DIR__  . "/../cache/" . ($is_mobile?'m':'d') . "/$recipe/$file";
	}
	
	public function CheckFile() {
		if (!file_exists($this->orig_file))
		{
			header("Status: 404 Not Found");
			//echo "<br/>ORIGFILE={$this->orig_file}";
			exit;
		}
	}
	
	public function CheckRecipe() {
		if(!isset($this->recipes[$this->recipe])) {
			header("Status: 403 Forbidden");
			exit;
		}
	}
	
	public function GetWidth() {
		if(isset($this->recipes[$this->recipe])) {
			return $this->recipes[$this->recipe]['width'];
		}
	}
	
	private function GetHeight() {
		if(isset($this->recipes[$this->recipe])) {
			return $this->recipes[$this->recipe]['width'];
		}
	}
	
	public function GetRealWidth() {
		$extension = strtolower(pathinfo($this->orig_file,PATHINFO_EXTENSION));
		if($extension == 'svg') {
			$width = $this->GetWidth();
		} else {
			if(!file_exists($this->cached_file)) {
				$this->ConvertCache();
			}
			$image = new Imagick($this->cached_file);
			$width = $image->GetImageWidth();
		}
		return $width;
	}

	public function GetRealHeight() {
		$extension = strtolower(pathinfo($this->orig_file,PATHINFO_EXTENSION));
		if($extension == 'svg') {
			$height = $this->GetHeight();
		} else {
			if(!file_exists($this->cached_file)) {
				$this->ConvertCache();
			}
			$image = new Imagick($this->cached_file);
			$height = $image->GetImageHeight();
		}
		return $height;
	}

	public function Convert() {
		$extension = strtolower(pathinfo($this->orig_file,PATHINFO_EXTENSION));
		$orig_ts = filemtime($this->orig_file);
		if($extension == 'svg') {
			header("Cache-Control: max-age=2592000");
			header('Content-Type: image/svg+xml');
			echo file_get_contents($this->orig_file);
		} else {
			if(!file_exists($this->cached_file) || $orig_ts > filemtime($this->cached_file)) {
				$image = $this->ConvertCache();
			} else {
				$image = new Imagick($this->cached_file);
			}
			header("Cache-Control: max-age=2592000");
			header('Content-Type: image/'.$image->getImageFormat());
			echo file_get_contents($this->cached_file);
		}
		return true;
	}

	private function ConvertCache() {
		# check if new directory would need to be created
		$save_dir = dirname($this->cached_file);
		if(!file_exists($save_dir)) {
			mkdir($save_dir, 0777, true);
		}
			
		list($orig_width, $orig_height, $type, $attr) = getimagesize($this->orig_file);
			
		$target_width = isset($this->recipes[$this->recipe]['width'])? $this->recipes[$this->recipe]['width'] : $orig_width;
		$target_height = isset($this->recipes[$this->recipe]['height'])? $this->recipes[$this->recipe]['height'] : $orig_height;
		$mode = isset($this->recipes[$this->recipe]['mode'])? $this->recipes[$this->recipe]['mode'] : 0;
			
		$new_width = $target_width;
		$new_height = $target_height;
	
		$image = new Imagick($this->orig_file);
			
		# preserve aspect ratio, fitting image to specified box
		if ($mode == "0")
		{
			$new_height = $orig_height * $new_width / $orig_width;
			if ($new_height > $target_height)
			{
				$new_width = (int)($orig_width * $target_height / $orig_height);
				$new_height = (int)$target_height;
			}
		}
		# zoom and crop to exactly fit specified box
		else if ($mode == "2")
		{
			// crop to get desired aspect ratio
			$desired_aspect = $target_width / $target_height;
			$orig_aspect = $orig_width / $orig_height;
				
			if ($desired_aspect > $orig_aspect)
			{
				$trim = $orig_height - ($orig_width / $desired_aspect);
				$image->cropImage($orig_width, $orig_height-$trim, 0, $trim/2);
			}
			else
			{
				$trim = $orig_width - ($orig_height * $desired_aspect);
				$image->cropImage($orig_width-$trim, $orig_height, $trim/2, 0);
			}
		}
		# mode 3 (stretch to fit) is automatic fall-through as image will be blindly resized
		# in following code to specified box
		$image->resizeImage($new_width, $new_height, imagick::FILTER_LANCZOS, 1);
		
		$image->setImageCompression(Imagick::COMPRESSION_JPEG);
		$image->setImageCompressionQuality($this->jpg_quality);
			
		$image->writeImage($this->cached_file);
		
		return $image;
	}
	
}
?>
