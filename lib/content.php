<?php
define("BUILD_DIR", __DIR__ . '/../build');
define("CONTENT_DIR", __DIR__ . '/../content');

require_once (__DIR__ . '/../lib/constants.php');
require_once (__DIR__ . '/../others/vendor/autoload.php');

/**
 * Content
 */
class Content
{
	private $is_ajax;
	private $is_mobile;
	private $lang;
	
	/**
	 * @var ParsedownExtra
	 */
	private $PS;
	
	private $video;
	private $images;

	function __construct($lang,$is_ajax,$is_mobile) {
		$this->lang = $lang;
		$this->is_ajax = $is_ajax;
		$this->is_mobile = $is_mobile;
		
		$this->PS = new ParsedownExtra();
	}
	
	public function ContentRender($markdown,$obj) {
		$this->images = array();
		$this->video = null;
		$content = new stdClass();
		if(isset($obj->metadata->aside) && isset($obj->metadata->aside->image)) {
			if(!isset($obj->metadata->aside->width)) {
				$obj->metadata->aside->width = $this->ImageWidth('inline','c/'.$obj->metadata->aside->image);
				$obj->metadata->aside->real_width = $this->ImageWidth('orig','c/'.$obj->metadata->aside->image);
			}
			$obj->metadata->aside->extension = $this->ImageExtension($obj->metadata->aside->image);
			$obj->metadata->aside->zoom = $obj->metadata->aside->extension != 'svg';
			$this->images[] = "/images/inline/c/{$obj->metadata->aside->image}";
		}
		if(isset($obj->pin)) {
			$this->images[] = "/images/pin/p/{$obj->pin}.jpg";
			$pin = new stdClass();
			$pin->src = "/images/pin/p/{$obj->pin}.jpg";
			$pin->download = "/images/orig/p/{$obj->pin}.jpg";
			$obj->metadata->pin = $pin;
			$obj->nav->url_lang = '/' . ($obj->lang=='en'?'it':'en') . "/pin/{$obj->pin}";
		}
		$content->metadata = $obj->metadata;
		$markdown = $this->ContentParser($markdown);
		$markdown = $this->PS->text($markdown);
		$markdown = $this->ImageWidthFix($markdown);
		if(isset($this->aside) && !isset($obj->metadata->aside)) {
			$content->aside = $this->aside;
		}
		$content->markdown = $markdown;
		if(isset($this->video)) {
			$content->type = "video";
			$content->image = "https://img.youtube.com/vi/{$this->video}/hqdefault.jpg";
			$content->video = $this->video;
		} else {
			$content->type = "article";
			if(count($this->images) > 0) {
				$content->image = HOST_LIVE . $this->images[0];
			} else {
				// fallback on background
				if(isset($obj->metadata->background)) {
					$content->image = HOST_LIVE . "/images/inline/bg/{$obj->metadata->background->image}";
				} else {
					$content->image = HOST_LIVE . '/images/inline/c/musilogue_w.png';
				}
			}
		}
		$extra_file = BUILD_DIR . "/{$this->lang}/extra/{$obj->section}/{$obj->article}.json";
		if(file_exists($extra_file)) {
			$content->extra = json_decode(file_get_contents($extra_file));
			if(isset($obj->pin)) {
				$obj->metadata->pin->caption = $content->extra->items->{$obj->pin}->caption;
				$obj->metadata->pin->author = $content->extra->items->{$obj->pin}->author;
				$obj->metadata->title = $obj->metadata->pin->caption;
				$obj->metadata->description = 'Musilogue Pin - Photo by ' . $obj->metadata->pin->author;
				$obj->metadata->pin->size = $content->extra->items->{$obj->pin}->size;
				$obj->metadata->pin->width = $content->extra->items->{$obj->pin}->width;
				$obj->metadata->pin->height = $content->extra->items->{$obj->pin}->height;
				$obj->metadata->pin->type = $content->extra->items->{$obj->pin}->width / $content->extra->items->{$obj->pin}->height > 1.1 ? 'landscape' : 'portrait';
			}
		} elseif(isset($obj->metadata->extra_tag)) {
			$extra_tag_file = BUILD_DIR . "/{$this->lang}/extra/tags/{$obj->metadata->extra_tag}.json";
			if(file_exists($extra_tag_file)) {
				$content->extra = json_decode(file_get_contents($extra_tag_file));
			}
		}
		if($obj->section == 'posts') {
		    if(isset($obj->metadata->extra) && isset($content->extra)) {
		        $content->extra->{$obj->metadata->extra} = $content->extra->items;
		        unset($content->extra->items);
		    }
			$press = json_decode(file_get_contents(CONTENT_DIR . "/$this->lang/press.json"));
			$press_links = array();
			foreach($press as $p) {
				if(isset($p->post) && $p->post == $obj->article) {
					$press_links[] = $p;
				}
			}
			if(count($press_links)>0) {
				$content->press = $press_links;
			}
		}
		$content->labels = json_decode(file_get_contents(CONTENT_DIR . "/$this->lang/labels.json"));
		if($this->is_ajax) {
			$content = $this->Render($obj->metadata->template, $content);
		}
		return $content;
	}
	
	public function Markdown2Html($markdown) {
		return $this->PS->text($markdown);
	}
	
	public function Render($template,$content) {
		$dust = new \Dust\Dust();
		$fdate = new ContentFormatDate($this->lang);
		$fdate->lang = $this->lang;
		$dust->filters['d'] = $fdate;
		$cpl = $dust->compileFile(__DIR__ . "/../templates/{$template}.dust");
		return $dust->renderTemplate($cpl, $content);
	}
	
	private function ContentParser($content) {
		preg_match_all("/\[embed:([^\]]*)\]/", $content, $matches);
		if(isset($matches[1]) && is_array($matches[1]) && count($matches[1]) > 0) {
			$counter = 1;
			foreach($matches[1] as $match) {
				$match_info = parse_url($match);
				// external link
				if(isset($match_info['host'])) {
					if(strpos($match_info['host'], 'youtube.com') !== false ) {
						parse_str( $match_info['query'], $vars );
						if(isset($vars['v'])) {
							$this->video = $vars['v'];
							$embed = "<div class=\"videoWrapper\"><div id=\"youtube$counter\" class=\"youtube\" video-hash=\"{$vars['v']}\"></div></div>";
							$content = str_replace("[embed:$match]",$embed,$content);
						}
					}
				} else {
					// local embed - do nothing for now
					$extension = pathinfo($match_info['path'],PATHINFO_EXTENSION);
					$embed_file = __DIR__ . "/../content/{$this->lang}/embed/" . basename($match_info['path']);
					if(file_exists($embed_file)) {
						//$embed = file_get_contents($embed_file);
						//$content = str_replace("[embed:$match]",$embed,$content);
					}
				}
				$counter++;
			}
		}
		return $content;
	}
	
	/**
	 * For each image in the content, get the size and add the relative width attribute
	 * 
	 * @param string $content
	 * @return string
	 */
	private function ImageWidthFix($content) {
		preg_match_all("/<img src=\"\/images\/([^\/]*)\/c\/([^\"]*)\"/", $content, $matches);
		$images = array();
		if(isset($matches[1]) && is_array($matches[1]) && count($matches[1]) > 0) {
			for($j = 0; $j < count($matches); $j++) {
				if(isset($matches[1][$j]) && isset($matches[2][$j])) {
					$width = $this->ImageWidth($matches[1][$j],'c/'.$matches[2][$j]);
					$id = strtolower(str_replace('.', '-', $matches[2][$j]));
					$content = str_replace("src=\"/images/{$matches[1][$j]}/c/{$matches[2][$j]}\"", "src=\"/images/{$matches[1][$j]}/c/{$matches[2][$j]}\" width=\"{$width}\" id=\"{$id}\"", $content);
					$extension = $this->ImageExtension($matches[2][$j]);
					// add to images list
					if($extension != 'svg') {
						$this->images[] = "/images/{$matches[1][$j]}/c/{$matches[2][$j]}";
					}
				}
			}
		}
		return $content;
	}

	private function ImageExtension($image) {
		return strtolower(pathinfo($image,PATHINFO_EXTENSION));
	}
	
	private function ImageWidth($recipe,$image) {
		require_once (__DIR__ . '/../lib/image.php');
		$i = new Image($recipe,$image,$this->is_mobile);
		return $i->GetRealWidth();
	}
	
	public function URL($section,$article,$pin) {
		$url = HOST_LIVE . "/{$this->lang}/";
		if($section == 'home') {
			$url .= $article!='index'? "pin/$pin/" : '';
		} else {
			$url .= "$section/" . ($article!='index'? "$article/":'');
		}
		return $url;
	}
	
}

class ContentFormatDate implements \Dust\Filter\Filter {
	public $lang;
	public function apply($value) {
		$ts = strtotime($value);
		if($ts>0) {
			$month = Constants::$MONTHS[$this->lang][date("n",$ts) - 1];
			if(date("G",$ts) == "1") {
				return $month . date (" Y",$ts);
			} else {
				return date("j ",$ts) . $month . date(" Y",$ts);
			}
		} else {
			return $value;
		}
	}
}

?>
