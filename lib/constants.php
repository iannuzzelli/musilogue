<?php
define("HOST_LIVE", 'https://musilogue.com');
class Constants {
	public static $CONTENT_LANGUAGES = array('en', 'it');
	public static $LOCALES = array( 'en' => 'en_GB', 'it' => 'it_IT');
	public static $TAGMETA = array( 'en' => 'by Francesco Iannuzzelli', 'it' => 'di Francesco Iannuzzelli');
	public static $KEYWORDS = array( 
		'en' => array('Francesco Iannuzzelli','Francesco','Iannuzzelli','music','musician','composer','italian','oud','ud','london','courses','maqam','middle-east','persian','iraq','music-theory','teaching','tuition','lessons','composition','orchestration'),
		'it' => array('Francesco Iannuzzelli','Francesco','Iannuzzelli','musica','musicista','compositore','italiano','oud','ud','londra','corsi','makam','medio-oriente','persiano','irak','teoria musicale','insegnamento','lezioni','composizione','orchestrazione')
	);
	public static $MONTHS = array( 
		'en' => ['January','February','March','April','May','June','July','August','September','October','November','December'],
		'it' => ['Gennaio','Febbraio','Marzo','Aprile','Maggio','Giugno','Luglio','Agosto','Settembre','Ottobre','Novembre','Dicembre']
	);
	public static $SITEMAP_NODES_NO_EXPAND = array(
		'en' => array('archive','posts','tags', 'home'),
		'it' => array('archivio','posts','tags', 'home')
	);
}
?>
