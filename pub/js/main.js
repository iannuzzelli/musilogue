var UniteGalleryAPI;
var UniteGalleryLightbox;
var sitemapD3;
var soundPlayer;
var YT;

$(function() {
	onloadJS();
});

// YouTube stuff
var youtubePlayers = [];
function onYouTubeIframeAPIReady() {
	youtubeLoad();
}
function youtubeLoad() {
	if(YT != null) {
		$(".youtube").each(function(i) {
			youtubeHash = this.getAttribute("video-hash");
			var index = i+1;
			youtubePlayers.push(new YT.Player('youtube'+index, {
				height: '315',
				width: '560',
				videoId: youtubeHash,
				origin: 'https://musilogue.com',
				events: {
					'onStateChange': onPlayerStateChange
				}
			}));
		});
	}
}
function onPlayerStateChange(event) {
	if (event.data == YT.PlayerState.PLAYING) {
		if(window.sm2BarPlayers.length) {
			window.sm2BarPlayers[0].actions.pause();
		}
		console.log('Playing YouTube video "' + event.target.j.videoData.title + '"');
	}
}
function youtubeStop() {
	if(youtubePlayers.length) {
		for (var i = 0; i < youtubePlayers.length; i++) {
			if($.isFunction(youtubePlayers[i].getPlayerState) ) {
				yState = youtubePlayers[i].getPlayerState();
				if(yState == 1) {
					youtubePlayers[i].stopVideo();
				}
			}
		}
		youtubePlayers = [];
	}
}

function onloadJS() {

	// globals
	var content = $('#content');
	var contentNew = $('#content2');
	var navs = ['up','down','left','right'];
	var backHomeTitle = 'Home';
	var backHomeUrl = '/';
	
	var data = null;
	
	var lang = 'en';
	
	var isMobile = false;

	var isVerticalScroll = false;
	
	var contentOffsetTop = content.offset().top;
	var contentOffsetLeft = content.offset().left;
	
	UniteGalleryLightbox = false;
	UniteGalleryActive = false;
	
	// previous level
	var levelCurrent = 1;
	var sequenceCurrent = parseInt($(document.body).attr('data-sequence'));
	
	// Events infinite scroll
	var itemsPerScroll = 4;
	var scrollTimer, lastScrollFireTime = 0;
	var scrollListener = function () {
		$('.content').on("scroll", function () {
			var minScrollTime = 100;
			var now = new Date().getTime();
			if (!scrollTimer) {
				if (now - lastScrollFireTime > (3 * minScrollTime)) {
				processScroll();
				lastScrollFireTime = now;
			}
			scrollTimer = setTimeout(function() {
				scrollTimer = null;
				lastScrollFireTime = new Date().getTime();
				processScroll();
				}, minScrollTime);
			}
		});
	};		
	function processScroll() {
		if ($('#content').scrollTop() >= $('#content ul.items').height() - $('#content').height() - 100) {
			itemsTotalScroll = $('#content ul.items li').length;
			shown = $('#content ul.items li.show').length;
			toshow = shown + itemsPerScroll;
			if(shown < itemsTotalScroll) {
				$('#content ul.items li:lt('+toshow+')').addClass("show");
			}
		}
	}
	
	// Reload state when clicking on browser buttons
	window.onpopstate = function(e){
	    if(e.state){
			navsDisableAll();
	    	contentUpdate(e.state.data);
	    }
	};
	
	// check screen width on resize
	window.onresize = function() {
		mobileCheck();
	}
	
	// Load content for internal links via AJAX
	$(document).on("click",".capture a[href^='/']",function(e) {

		var contentUrl = $(this).attr("href");
		var langSwitch = $(this).attr("id") == 'icon-lang-link';
		
		if(sitemapD3) {
			simulation.stop();
			sitemapD3 = false;
		}

		if(/\/audio\//.test(contentUrl)) {
			e.preventDefault();
			var audioExt = contentUrl.split('.').pop();
			if(audioExt == 'mp3' && !$('.sm2-bar-ui').hasClass('playlist-open')) {
				$('ul.sm2-playlist-bd').html('');
				$('ul.sm2-playlist-bd').append('<li><a href="' + contentUrl + '">' + this.textContent + '</li>');
				window.sm2BarPlayers[0].playlistController.refresh();
				window.sm2BarPlayers[0].playlistController.playItemByOffset(window.sm2BarPlayers[0].playlistController.data.playlist.length - 1);
				$('.sm2-bar-ui .sm2-menu').hide();
			}
			if(audioExt == 'json') {
				$.ajax({
					url : contentUrl,
					type : "GET",
					cache : false,
					success : function(data) {
						$('ul.sm2-playlist-bd').html('');
						$.each(data, function(k, track) {
							$('ul.sm2-playlist-bd').append('<li><a href="/audio/' + track.filename + '">' + track.title + '</li>');
						});
						window.sm2BarPlayers[0].playlistController.refresh();
						window.sm2BarPlayers[0].playlistController.playItemByOffset(window.sm2BarPlayers[0].playlistController.data.playlist.length - data.length);
						$('.sm2-bar-ui .sm2-menu').show();
					},
					error: function(XMLHttpRequest, textStatus, errorThrown) { 
						console.log("Could not load playlist " + contentUrl);
					}
				});

			}
		} else if(!/^\/docs\//.test(contentUrl) && !/^\/rubab\//.test(contentUrl) && !/\/images\//.test(contentUrl) && !langSwitch) {
			e.preventDefault();
			navigate(contentUrl);
		}
	});
	
	function navigate(contentUrl) {
		var params = contentUrl.split('/');
		
		lang = params[1]?  params[1] : 'en';
		var section = params[2]?  params[2] : 'home';
		var article = params[3]?  params[3] : 'index';
		var requestUrl = '/content.php?echo=1&lang=' + lang + '&p1=' + section + '&p2=' + article;
		requestUrl += params[4]? '&p3=' + params[4] : '';
		requestUrl += isMobile? '&m=1' : '';
		
		navsDisableAll();
		
		$.ajax({
			url : requestUrl,
			type : "GET",
			cache : false,
			success : function(response) {
				data = JSON.parse(response);
				// Override UP arrow when going to about from posts
				if(data.section_en=='about' && data.article_en=='index') {
					if($('body.s-posts').length) {
						old_metadata_encoded = $(document.body).attr('data-metadata');
						old_metadata = JSON.parse(old_metadata_encoded);
						data.nav.up.section = 'posts';
						backHomeTitle = old_metadata.title_short? old_metadata.title_short : old_metadata.title;
						backHomeUrl = '/' + lang + '/posts/' + old_metadata.article;
					} else if($('body.s-home').length) {
						backHomeTitle = 'Home';
						backHomeUrl = '/' + lang + '/';
					}
				}
				// update URL history
				history.pushState({'data':data}, '', contentUrl);
				// update content
				contentUpdate(data);
			},
			error: function(XMLHttpRequest, textStatus, errorThrown) { 
				data = JSON.parse(XMLHttpRequest.responseText);
				history.pushState({'data':data}, '', contentUrl);
				// update content to show error page
				contentUpdate(data);
				console.log("Response: " + XMLHttpRequest.responseText + ' - ' + errorThrown);
			}
		});
	}

	// SoundManager2
	soundManager.setup({
		flashVersion: 9,
		url: '/js/sm2swf/',
		onready: function() {
			$('.sm2-playlist-bd').html('<li><a href="/audio/Samai%20Hijaz.mp3">Samai Hijaz</li>');
			window.sm2BarPlayers[0].playlistController.refresh();
		}		
	});

	window.sm2BarPlayers.on = {
		play: function (player) {
			youtubeStop();
			// get current sound
			var currentSound = window.sm2BarPlayers[0].playlistController.getItem().textContent;
			console.log('Playing "' + currentSound + '"');
		},
		resume: function (player) {
			youtubeStop();
			// get current sound
			var currentSound = window.sm2BarPlayers[0].playlistController.getItem().textContent;
			console.log('Resume "' + currentSound + '"');
		}
	};

	// External links
	$(document).on("click","#container a[href^='http']",function(e) {
		e.preventDefault();
		var clickedLink = $(this).attr("href");

		// Soundcloud links in the player
		if(/soundcloud.com\//.test(clickedLink)) {
			this.href = this.href.replace(clickedLink, "#");
			var consumer_key = 'o7pZ0a0QUuTmzXb0NRNwSZ4ls3fSUDQL';
			$.when($.getJSON('https://api.soundcloud.com/resolve?url=' + clickedLink + '&format=json&consumer_key=' + consumer_key + '&callback=?', function(track) {
				trackUrl = track.stream_url + '?consumer_key=' + consumer_key;
				trackTitle = track.title;
            })).done(function() {
				$('ul.sm2-playlist-bd').html('');
				$('ul.sm2-playlist-bd').append('<li><a href="' + trackUrl + '">' + trackTitle + '</li>');
				window.sm2BarPlayers[0].playlistController.refresh();
				window.sm2BarPlayers[0].playlistController.playItemByOffset(window.sm2BarPlayers[0].playlistController.data.playlist.length - 1);
				$('.sm2-bar-ui .sm2-menu').hide();
            }).fail(function() {
				console.log('Error: could not load ' + clickedLink);
			});
		} else {
			// all other links in external window
			window.open(clickedLink, '_blank');
		}
	});
	

	init();


	if(!isMobile) {

		// Help
		$('#icon-help').click(function() {
			var helpPage = '/help_' + lang + '.html';
			$.featherlight(helpPage, ({ variant: 'capture dark help'}));
		});

		// Navigate with arrow keys
		$("body").keydown(function(e) {
			switch (e.keyCode) {
				case 37: // left
					if($('#nav-left a').length) {
						$('#nav-left a').trigger("click");
					}
				break;
				case 38: // up
					if($('#nav-up a').length && !isVerticalScroll) {
						$('#nav-up a').trigger("click");
					}
				break;
				case 39: // right
					if($('#nav-right a').length && !UniteGalleryLightbox) {
						$('#nav-right a').trigger("click");
					}
				break;
				case 40: // down
					if($('#nav-down a').length && !isVerticalScroll) {
						$('#nav-down a').trigger("click");
					}
				break;
			}
		});	
	}

	
	function init() {
		mobileCheck();
/*		if(isMobile) {
			$.ajax({
				url      : '/js/jquery.mobile.custom.min.js',
				dataType : 'script'
			});
		}
*/		contentNew.hide();
		contentFormat();
		metadata_encoded = $(document.body).attr('data-metadata');
		metadata = JSON.parse(metadata_encoded);
		lang = metadata.lang;
		contentUpdateBackground(metadata);
		contentUpdatePost(metadata);
	}
	
	function contentAnimate(data,levelCurrent,levelNew,sequenceCurrent,sequenceNew) {
		var contentAnimationSpeed = 800;
		var contentAnimationEasing = 'linear';
		content = $('#content');
		contentNew = $('#content2');
		
		var transitionJump = Math.abs(levelNew - levelCurrent) > 1 || Math.abs(sequenceNew - sequenceCurrent) > 1;
		if((data.section=='tags' && data.article=='index' && levelCurrent != 3) || isMobile) {
			transitionJump = true;
		}
		contentNew.html(data.content);
		var contentParent = content.parent();
		content.css({position: 'absolute'});

		if(transitionJump) {
			contentNew.hide().appendTo(contentParent).css({position: 'absolute'});
			content.fadeOut();
			content.css({position: null});
			navsUpdate(data.nav,data.navsection);
			contentUpdatePost(data.metadata);
			contentNew.fadeIn()
			contentNew.css({position: null});
			isVerticalScroll = $('#content2').height() < $('#content2 article').height();
		} else {
			if(levelCurrent != levelNew) {
				// vertical
				var contentParentHeight = levelNew > levelCurrent? contentParent.height() : - contentParent.height();
				contentNew.hide().appendTo(contentParent).css({top: -contentParentHeight, position: 'absolute'});
				content.animate({top: contentParentHeight}, contentAnimationSpeed, contentAnimationEasing, function(){
					content.hide();
					content.css({top: contentOffsetTop, position: null});
					navsUpdate(data.nav,data.navsection);
					contentUpdatePost(data.metadata);
				});
				contentNew.show().animate({top: contentOffsetTop}, contentAnimationSpeed, function(){
					contentNew.css({top: contentOffsetTop, position: null});
				});
			} else {
				var contentParentWidth = sequenceNew < sequenceCurrent? contentParent.width() : - contentParent.width();
				var contentWidth = content.width();
				// Set new content position out of screen
				contentNew.hide().appendTo(contentParent).css({left: contentParentWidth, width: contentWidth, position: 'absolute'});
				// Hide previous page
				content.animate({left : -contentParentWidth}, contentAnimationSpeed, contentAnimationEasing, function(){
					content.hide();
					content.css({left: contentOffsetLeft, position: null});
					navsUpdate(data.nav,data.navsection);
					contentUpdatePost(data.metadata);
				});
				// Show new page
				contentNew.show().animate({left: contentOffsetLeft}, contentAnimationSpeed, function(){
					contentNew.css({left: contentOffsetLeft, position: null});
				});
			}
		}
		// swap IDs
		content.attr('id','content3');
		contentNew.attr('id','content');
		content.attr('id','content2');
	}
	
	function contentUpdate(data) {
		var current = $.featherlight.current();
		if(current != null) {
			current.close();
		}
		
		youtubeStop();
		$('.youtube').remove();
		contentUpdateBackground(data.metadata);
		
		levelCurrent = parseInt($(document.body).attr('data-level'));
		var levelNew = data.metadata.hasOwnProperty('level')? data.metadata.level : 1;
		sequenceCurrent = parseInt($(document.body).attr('data-sequence'));
		var sequenceNew = data.nav.hasOwnProperty('sequence')? data.nav.sequence : data.nav.sequence_max;

		contentUpdatePre();
		$(document.body).attr('class','s-'+data.section_en+' a-'+data.article_en+' l-'+levelNew+' t-'+data.metadata.template);
		$(document.body).attr('data-level',levelNew);
		$(document.body).attr('data-sequence',sequenceNew);
		data.metadata.article = data.article;
		$(document.body).attr('data-metadata',JSON.stringify(data.metadata));
		
		contentAnimate(data,levelCurrent,levelNew,sequenceCurrent,sequenceNew);
		contentFormat();
	}
	
	function contentUpdatePre() {
		document.title = '...';
		$('#credits').hide();
		$('#navsection').html('');
		$('#navsection').hide();
		$('.scaling-svg').fadeOut();
	}

	function contentUpdateBackground(metadata) {
		// set background
		if(metadata.hasOwnProperty('background') && metadata.background.hasOwnProperty('image')) {
			if(metadata.background.hasOwnProperty('style') && metadata.background.style == "dark") {
				$("#container").addClass("dark");
			} else {
				$("#container").removeClass("dark");
			}
			backgroundImg = (isMobile? '/mimages' : '/images') + '/full/bg/' + metadata.background.image;
			$.backstretch(backgroundImg, {speed: 400});
		} else {
			$(".backstretch").remove();
		}
	}

	function contentUpdatePost(metadata) {
		isVerticalScroll = $('#content').height() < $('#content article').height();
		$('#content').focus();

		document.title = metadata.title;
		// update hreflang
		$('#icon-lang-link').attr("href", metadata.url_lang);
		$('link[hreflang]').attr("href", metadata.url_lang);
		$('link[rel="canonical"]').attr("href", metadata.url);
		$('meta[name="description"]').attr("content", metadata.description);

		// credits
		if(metadata.hasOwnProperty('background') && metadata.background.hasOwnProperty('credits')) {
			$('#credits').html(metadata.background.credits);
			$('#credits').show();
		}

		if(!isMobile) {
			// inline images
			$(".content  p > img").each(function() {
				var width = this.getAttribute("width");
				if(!$(this).hasClass('noshadow')) {
					var alt = this.getAttribute("alt");
					$(this).wrap('<div class="inline-image" data-content="'+alt+' (Click to enlarge)" style="width: '+width+'px"></div>');
					$(this).attr('title', alt + ' (Click to enlarge)');
					$(this).addClass('shadow');
				}
			});
			$('.content .inline-image').click(function() {
				var src = $(this).find('img').attr("src").replace('inline', 'inlinezoom');
				var alt = $(this).find('img').attr("alt");
				img_html = '<div class="img-box">';
				img_html += '<img class="shadow" src="' + src + '">';
				img_html += '<div class="img-caption">' + alt + '</div>';
				img_html += '</div>';
				$.featherlight(img_html);
			});
		}

		// youtube video
		$(".youtube").each(function() {
			var youtubeScripts = $('script[src="https://www.youtube.com/iframe_api"]').length;
			if(youtubeScripts == 0) {
				var tag = document.createElement('script');
				tag.src = "https://www.youtube.com/iframe_api";
				var firstScriptTag = document.getElementsByTagName('script')[0];
				firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
			} else {
				youtubeLoad();
			}
		});
		
		// gallery
		if(metadata.template == 'gallery') {
			UniteGalleryAPI = $("#gallery").unitegallery({
				gallery_theme:"tiles",
				theme_appearance_order: "shuffle",
				tiles_col_width: 300,
				tiles_align: 'left',
				tiles_space_between_cols: 6
			});
			UniteGalleryAPI.on("open_lightbox",function(){
				UniteGalleryLightbox = true;
			});
			UniteGalleryAPI.on("close_lightbox",function(){
				UniteGalleryLightbox = false;
			});
			UniteGalleryActive = true;
		} else if(UniteGalleryAPI != null && UniteGalleryActive) {
			UniteGalleryAPI.destroy();
			UniteGalleryActive = false;
			UniteGalleryLightbox = false;
		}
		
		// tags cloud
		if(metadata.template == 'tags') {
			drawWordCloud();
		}
		
		// events infinite scroll
		if(metadata.template == 'events') {
			if(metadata.article_en == 'past') {
				$('.content ul.past li:lt('+itemsPerScroll+')').addClass("show");
				scrollListener();
			} else {
				$('.content ul.items li').addClass("show");
			}
		} else {
			$(window).unbind('scroll');
		}

		// track 404 events
		if(metadata.error) {
			var referrer = document.referrer;
			if (referrer === '') {
				referrer = 'No Referrer';
			}
		}
	}
	
	function contentFormat() {
		$(".content a[href^='http']").not('.sharing').each(function() {
		    var href = this.getAttribute("href");
		    if(/soundcloud.com/.test(href)) {
				$(this).attr('title','Click to listen');
				$("<span class='fab fa-soundcloud'></span>").insertBefore($(this));
			} else if(/youtube.com/.test(href)) {
				$(this).attr('title','Click to see video');
				$("<span class='fab fa-youtube'></span>").insertBefore($(this));
			} else if(/youtu.be/.test(href)) {
				$(this).attr('title','Click to see video');
				$("<span class='fab fa-youtube'></span>").insertBefore($(this));
			} else if(/facebook.com/.test(href)) {
				$(this).attr('title','Go to Facebook');
				$("<span class='fab fa-facebook'></span>").insertBefore($(this));
			} else if(/twitter.com/.test(href)) {
				$(this).attr('title','Go to Twitter');
				$("<span class='fab fa-twitter'></span>").insertBefore($(this));
			} else if(/pinterest.com/.test(href)) {
				$(this).attr('title','Go to Pinterest');
				$("<span class='fab fa-pinterest'></span>").insertBefore($(this));
			} else if(/instagram.com/.test(href)) {
				$(this).attr('title','Go to Instagram');
				$("<span class='fab fa-instagram'></span>").insertBefore($(this));
			} else if(/.rss/.test(href)) {
				$(this).attr('title','RSS feed');
				$("<span class='fa fa-rss'></span>").insertBefore($(this));
			} else {
				$(this).attr('title','Opens in new window');
				$("<span class='fa fa-external-link-alt'></span>").insertBefore($(this));
			}
		});
		$(".content a[href^='/docs/']").each(function() {
			var href = this.getAttribute("href");
			var ext = href.split('.').pop().toUpperCase();
			$(this).attr('title','Open ' + ext + ' file');
			$(this).attr('target','_blank');
			$("<span class='fa fa-download'></span>").insertBefore($(this));
			$(this).after(document.createTextNode(' ['+ext+']'));
		});
		$(".content a[href^='/rubab/']").each(function() {
			$("<span class='fa fa-download'></span>").insertBefore($(this));
		});
		$(".content a[href^='/audio/']").each(function() {
			$("<span class='fa fa-play'></span>").insertBefore($(this));
		});
	}

	function mobileCheck() {
		zindex = $('.content').css("z-index");
		isMobile = zindex == 2;
		if(isMobile) {
			$('.sm2-bar-ui').addClass('compact');
			$('#icon-lang a .icon-title').text($('#icon-lang a .icon-title').text().substr(0,2));
		} else {
			navsUpdate(false,false);
			$('.sm2-bar-ui').removeClass('compact');
		}
	}
	
	function navsUpdate(nav,navsection) {
		if(isMobile) {
			if(navsection) {
				$.each(navsection, function( index, section) {
					$('#navsection').append('<a ' + (section.url==window.location.pathname? ' class="selected"':'') + ' href="' + section.url + '">' + section.title + '</a>');
				});
				$('#navsection').fadeIn();
			}
		} else {
			if(nav) {
				$.each(navs, function( index, value ) {
					if(nav.hasOwnProperty(value)) {
						if(value == 'up' && nav[value].title == 'Home') {
							nav[value].title = backHomeTitle;
							nav[value].url = backHomeUrl;
						}
						$('#nav-'+value).html('<a href="' + nav[value].url + '" title="' + nav[value].title + '"><svg class="icon"><use xlink:href="#' + value + '"/></svg><div class="title">' + nav[value].title + '</div></a>');
					}
				});
			}
			$.each(navs, function( index, value ) {
				if(!$.trim( $('#nav-'+value).html() ).length ) {
					$('#nav-'+value).hide();
				} else {
					$('#nav-'+value).fadeIn();
				}
			});
		}
	}
	
	function navsDisableAll() {
		$.each(navs, function( index, value ) {
			$('#nav-'+value).html('');
			$('#nav-'+value).hide();
		});
	}
	
	// SITEMAP 

	var nodeSize = 15;
	var linkDistance = 100;
	var data, link, node, simulation, drag;
	sitemapD3 = false;
	
	// Sitemap
	$('#icon-sitemap').click(function() {
		var sitemapFile = '/sitemap.html';
		$.featherlight(sitemapFile, ({
			variant: 'capture',
			afterOpen: function(event){
				var svg = d3.select("svg#sitemap");
				var width = +svg.attr("width");
				var height = +svg.attr("height");
				
				sitemapD3 = true;
				
				linkSvg = svg.append("g");
				nodeSvg = svg.append("g");

				drag = d3.drag().on("start", dragstarted).on("drag", dragged).on("end", dragended);

				simulation = d3.forceSimulation().force("link",
						d3.forceLink().id(function(d) {
							return d.id;
						})).force("charge", d3.forceManyBody()).force("center",
						d3.forceCenter(width / 2, height / 2));

				simulation.force("link").distance(linkDistance);
				
				d3.json('/json/' + lang + '/sitemap', function(error, json) {
					if (error)
						throw error;

					data = json;
					sitemapUpdate();
				});

			}
		}));
	});
	
	function dragstarted(d) {
		if (!d3.event.active)
			simulation.alphaTarget(0.3).restart()
		d.fx = d.x;
		d.fy = d.y;
	}

	function dragged(d) {
		d.fx = d3.event.x;
		d.fy = d3.event.y;
	}

	function dragended(d) {
		if (!d3.event.active)
			simulation.alphaTarget(0);
		d.fx = null;
		d.fy = null;		
	}

	function sitemapTick() {
		if(sitemapD3) {
			data.nodes.forEach(function(d) {
				if (d.isCircle) {
					d.leftX = d.rightX = d.x;
				} else {
					d.leftX = d.x - d.textLength / 2 + nodeSize / 2;
					d.rightX = d.x + d.textLength / 2 - nodeSize / 2;
				}
			});

			link.call(sitemapNodeEdge);
			
			node.attr("transform", function(d) {
				return "translate(" + d.x + "," + d.y + ")";
			});
		} else {
			simulation.stop();
			sitemapD3 = false;
		}
	}

	function sitemapUpdate() {
		link = linkSvg.selectAll("line").data(data.links);

		var linkEnter = link.enter().append("line").attr("class", "link-line");

		link.exit().remove();
		
		link = linkEnter.merge(link);

		node = nodeSvg.selectAll("g").data(data.nodes);

		var nodeEnter = node.enter().append("g").attr("class", "node").call(drag);

		node.exit().remove();

		nodeEnter
			.append("a").attr("xlink:href", function (d) { return d.url; })
			.append("rect").attr("class", function(d) {
				return 'node-rect s-' + d.section + (d.url==window.location.pathname? ' selected':'');
			}).attr("y", -nodeSize).attr("height", nodeSize * 2).attr("rx",	nodeSize).attr("ry", nodeSize)
			.on("click", function(d) {
				d.fx = null;
				d.fy = null;
				simulation.stop();
			});

		nodeEnter.append("text").attr("class", "node-text");

		node = nodeEnter.merge(node);

		node.select(".node-text").text(function(d) {
				return d.name;
		}).each(
			function(d) {

				var circleWidth = nodeSize * 2, textLength = this.getComputedTextLength(), textWidth = textLength + nodeSize;

				if (circleWidth > textWidth) {
					d.isCircle = true;
					d.rectX = -nodeSize;
					d.rectWidth = circleWidth;
				} else {
					d.isCircle = false;
					d.rectX = -(textLength + nodeSize) / 2;
					d.rectWidth = textWidth;
					d.textLength = textLength;
				}
		});

		node.select(".node-rect").attr("x", function(d) {
			return d.rectX;
		}).attr("width", function(d) {
			return d.rectWidth;
		});

		simulation.nodes(data.nodes).on("tick", sitemapTick);

		simulation.force("link").links(data.links);

	}

	function sitemapNodeEdge(selection) {
		selection.each(function(d) {
			var sourceX, targetX, midX, dy, dy, angle;

			if (d.source.rightX < d.target.leftX) {
				sourceX = d.source.rightX;
				targetX = d.target.leftX;
			} else if (d.target.rightX < d.source.leftX) {
				targetX = d.target.rightX;
				sourceX = d.source.leftX;
			} else if (d.target.isCircle) {
				targetX = sourceX = d.target.x;
			} else if (d.source.isCircle) {
				targetX = sourceX = d.source.x;
			} else {
				midX = (d.source.x + d.target.x) / 2;
				if (midX > d.target.rightX) {
					midX = d.target.rightX;
				} else if (midX > d.source.rightX) {
					midX = d.source.rightX;
				} else if (midX < d.target.leftX) {
					midX = d.target.leftX;
				} else if (midX < d.source.leftX) {
					midX = d.source.leftX;
				}
				targetX = sourceX = midX;
			}

			dx = targetX - sourceX;
			dy = d.target.y - d.source.y;
			angle = Math.atan2(dx, dy);

			d.sourceX = sourceX + Math.sin(angle) * nodeSize;
			d.targetX = targetX - Math.sin(angle) * nodeSize;
			d.sourceY = d.source.y + Math.cos(angle) * nodeSize;
			d.targetY = d.target.y - Math.cos(angle) * nodeSize;

		}).attr("x1", function(d) {
			return d.sourceX;
		}).attr("y1", function(d) {
			return d.sourceY;
		}).attr("x2", function(d) {
			return d.targetX;
		}).attr("y2", function(d) {
			return d.targetY;
		});
	}
	
	function drawWordCloud() {
		var svg_location = "#cloud";
		var width = $(document).width() / 2;
		var height = $(document).height() / 2;

		var fill = d3.scaleOrdinal(d3.schemeCategory20);

		var word_entries = d3.entries(tags_count);

		var xScale = d3.scaleLinear()
			.domain([0, d3.max(word_entries, function(d) {
				return d.value;
			})
			])
			.range([20,40]);

		d3.layout.cloud().size([width, height])
			.timeInterval(20)
			.words(word_entries)
			.fontSize(function(d) { return xScale(+d.value); })
			.text(function(d) { return d.key; })
			.rotate(0)
			.font("Impact")
			.on("end", draw)
			.start();

		function draw(tags) {
			d3.select(svg_location).append("svg")
				.attr("width", width)
				.attr("height", height)
			.append("g")
				.attr("transform", "translate(" + [width >> 1, height >> 1] + ")")
			.selectAll("text")
				.data(tags)
			.enter().append("text")
				.style("font-size", function(d) { return xScale(d.value) + "px"; })
				.style("font-family", "Impact")
				.style("fill", function(d, i) { return fill(i); })
				.attr("text-anchor", "middle")
				.attr("transform", function(d) {
				return "translate(" + [d.x, d.y] + ")rotate(" + d.rotate + ")";
				})
				.text(function(d) { return d.key; })
				.on("click", function (d, i){
					navigate('/' + lang + '/tags/' + d.key.substring(1) + '/');
			});
		}
		d3.layout.cloud().stop();
	}
	
}

