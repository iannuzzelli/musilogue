<?php
include_once(__DIR__ . '/../lib/image.php');

// sanitize
$recipe = preg_replace("/[^a-zA-Z0-9]/", "", $_GET['recipe']);
$file = preg_replace("/[^a-zA-Z0-9._-]/", "", $_GET['file']);
$dir = preg_replace("/[^a-z]/", "", $_GET['dir']);
$is_mobile = isset($_GET['mobile']) && $_GET['mobile'] == "1";

$i = new Image($recipe,"$dir/$file",$is_mobile);

$i->CheckRecipe();
$i->CheckFile();

$i->Convert();
?>
