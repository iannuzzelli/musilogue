<?php
require_once (__DIR__ . '/../lib/content.php');

$force404 = false;
// check old URLs without language
if(isset($_GET['lang']) && $_GET['lang']!='' && !in_array($_GET['lang'],Constants::$CONTENT_LANGUAGES)) {
	$section_old = preg_replace("/[^a-zA-Z0-9]/", "", $_GET['lang']);
	$article_old = isset($_GET['p1']) && $_GET['p1']!=''? preg_replace("/[^a-zA-Z0-9_-]/", "", $_GET['p1']) : 'index';
	// if they point to something existing, redirect 301 with /en, otherwise 404
	if($section_old == 'pin') {
		$file_exists = file_exists(CONTENT_DIR . "/images/p/{$article_old}.jpg");
	} else {
		$file_exists = file_exists(CONTENT_DIR . "/en/$section_old/{$article_old}.json");
	}
	if($file_exists) {
		header('Location: /en' . $_SERVER['REQUEST_URI'], true, 301);
		exit();
	} else {
		$force404 = true;
	}
}

$lang = isset($_GET['lang']) && in_array($_GET['lang'],Constants::$CONTENT_LANGUAGES)? $_GET['lang'] : 'en';
$section = isset($_GET['p1']) && $_GET['p1']!=''? $_GET['p1'] : 'home';
$article = isset($_GET['p2']) && $_GET['p2']!=''? $_GET['p2'] : 'index';
$debug = isset($_GET['p3']) && $_GET['p3']=='debug';
$is_mobile = isset($_GET['m']) && $_GET['m']=='1';
$ajax_call = isset($_GET['echo']) && $_GET['echo']=='1';

// Sanitize path (allow only letters and numbers
$section = preg_replace("/[^a-zA-Z0-9]/", "", $section);
$article = preg_replace("/[^a-zA-Z0-9_-]/", "", $article);

$obj = new stdClass();
$obj->lang = $lang;
$obj->locale = Constants::$LOCALES[$lang];
$obj->keywords = implode(', ',Constants::$KEYWORDS[$lang]);
$obj->host = HOST_LIVE;

if($section == 'pin') {
	$section = 'home';
	$metadata = "/$lang/home/pin.json";
	$file_exists = file_exists(CONTENT_DIR . "/images/p/{$article}.jpg");
	$obj->pin = $article;
	$article = 'pin';
} else {
	$metadata = "/$lang/$section/{$article}.json";
	$file_exists = file_exists(CONTENT_DIR . $metadata);
}

$obj->section = $section;
$obj->article = $article;
$nav = json_decode(file_get_contents(BUILD_DIR . "/$lang/nav.json"));
if($lang != 'en') {
	$mapping = array_flip(json_decode(file_get_contents(__DIR__ . "/../content/$lang/mapping.json"), true));
	$obj->section_en = isset($mapping[$section])? $mapping[$section] : $section;
	$obj->article_en = isset($mapping[$article])? $mapping[$article] : $article;
} else {
	$obj->section_en = $section;
	$obj->article_en = $article;
}

if($file_exists && !$force404) {
	$obj->metadata = json_decode(file_get_contents(CONTENT_DIR . $metadata));
	if(!isset($obj->metadata->level)) {
		$obj->metadata->level = 1;
	}
	if(!isset($obj->metadata->template)) {
		$obj->metadata->template = 'default';
	}
	$obj->nav = isset($nav->$section) && isset($nav->$section->$article) ? $nav->$section->$article : $nav->default->index;
	if($section!='home' && (!$ajax_call || $is_mobile)) {
		$navsection = array();
		foreach(get_object_vars($nav->$section) as $nav_key => $navsection_item) {
			$item = new stdClass();
			$item->title = $navsection_item->title;
			$item->url = $navsection_item->url;
			if($article == $nav_key) {
				$item->selected = true;
			}
			$navsection[] = $item;
		}
		if($section == 'posts' || $section == 'archive') {
			krsort($navsection);
		}
		$obj->navsection = array_values($navsection);
	}
	if(!isset($obj->nav->sequence)) {
		$obj->nav->sequence = 0;
	}
	$markdown = file_exists(CONTENT_DIR . "/$lang/$section/{$article}.md")? file_get_contents(CONTENT_DIR . "/$lang/$section/{$article}.md") : '';
} else {
	header ("HTTP/1.1 404 Not Found");
	$obj->section = 'error';
	$obj->metadata = json_decode(file_get_contents(CONTENT_DIR . "/$lang/error/index.json"));
	$obj->metadata->error = 1;
	$obj->nav = $nav->default->index;
	$obj->navsection[] = $obj->nav->left;
	$markdown = file_get_contents(CONTENT_DIR . "/$lang/error/index.md");
}

$c = new Content($lang,$ajax_call,$is_mobile);

$obj->metadata->url = $c->URL($section,$article,isset($obj->pin)?$obj->pin:null);
$obj->metadata->url_lang = (isset($obj->nav->url_lang)? $obj->nav->url_lang : "/$lang/");
$obj->metadata->url_lang_code = $lang=='en'?'it':'en';
$obj->metadata->lang = $obj->lang;
$obj->metadata->lang_label = $lang=='en'? 'Italiano' : 'English';
$obj->metadata->article = $obj->article;
$obj->metadata->article_en = $obj->article_en;

$obj->content = $c->ContentRender($markdown,$obj);

$obj->metadata_encoded = json_encode($obj->metadata);

$obj_encoded = json_encode($obj); 

// AJAX call
if($ajax_call) {
	echo $obj_encoded;
}
?>