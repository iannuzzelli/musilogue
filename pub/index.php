<?php
// get content json (use $nav and $obj_encoded only)
include("content.php");

// get all the rest
$obj = new stdClass();
$obj->page = json_decode($obj_encoded,true);

if($debug) {
	header('Content-Type: application/json');
	$obj->nav = $nav;
	echo json_encode($obj, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES);
} else {
	echo $c->Render('layout', $obj);
}
?>
