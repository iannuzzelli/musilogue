<?php
$lang = isset($_GET['lang']) && in_array($_GET['lang'],array('en','it'))? $_GET['lang'] : 'en';
$file = preg_replace("/[^a-zA-Z0-9._-]/", "", $_GET['file']);

$content_dir = __DIR__ . '/../build';

$json_file = "$content_dir/{$lang}/{$file}.json";

$allowed_files = array('nav','sitemap');

if(!in_array($file, $allowed_files)) {
	header("Status: 403 Forbidden");
	return 0;
}
if (!file_exists($json_file))
{
	header("Status: 404 Not Found");
	// echo "<br/>$json_file";
	return 0;
}
header('Content-Type: application/json');
echo file_get_contents($json_file);

return true;
?>
