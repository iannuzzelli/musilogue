<?php

$keys = array('A','Bb','B','C','Db','D','Eb','E','F','Gb','G','Ab');

$scales = array(
    'major',
    'melodic minor',
    'natural minor',
    'harmonic minor',
    'dominant 7th',
    'augmented',
    'diminished / octatonic',
    'altered / superlocrian'
);

$seen = array();

echo "<ul>";
for ($i=0; $i<10; $i++) {
    $random_key = array_rand($keys);
    $random_scale = array_rand($scales);
    echo "<li>{$keys[$random_key]} {$scales[$random_scale]}</li>\n";
}
echo "</ul>";
?>