
## Guida

Questo sito può sembrare a prima vista poco comprensibile :)

Si tratta in realtà di una scelta intenzionale, essendo concepito come uno spazio aperto dove i visitatori possano vagare e scroprire liberamente la mia musica.

E' possibile navigare in **quattro direzioni** (alto, basso, sinistra, destra) quando un'icona è visibile nel relativo lato dello schermo.

E' anche possibile usare i **tasti freccia della tastiera** invece di cliccare sulle icone di navigazione.

Tutti i link audio vengono riprodotti dal player in basso a sinistra, ed è possibile continuare la navigazione mentre la musica suona. Tutti i link esterni si aprono in una nuova finestra del browser.

Le sezione dei **posts** si estende alla sinistra dell'[homepage](/it/) in ordine cronologico.

I posts sono riconoscibili dal loro sfondo chiaro, mentre tutte le altre pagine hanno uno sfondo scuro.

I posts e le pagine dell'archivio storico sono anche taggate con **parole chiave**, offrendo un'approccio alternativo alla navigazione. Tutte le parole chiave sono visibili nella pagina dei [tags](/it/tags/), raggiungibile dall'icona in basso a destra.

La **mappa del sito**, la cui icona è anch'essa presente in basso a destra, offre una visione più generale della struttura del sito. Per brevità, non include i posts e le pagine dell'archivio storico.

La homepage è intenzionalmente vuota.

