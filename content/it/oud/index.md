# oud

Ho scoperto l'Oud durante i miei viaggi in Palestina e grazie all'influenza dei miei numerosi amici Arabi. Al tempo stesso, l'Oud mi ha portato a scoprire molte altre culture e incontrare molti altri amici, e il viaggio continua ancora.

Sempre curioso riguardo altre tradizioni musicali e strumenti, mi sono innamorato del suono dell'Oud e della sua storia, specializzandomi nello stile iracheno.

Originariamente chitarrista classico, ho studiato l'Oud per più di 20 anni con diversi insegnanti dall'Irak e dalla Turchia, e ancora oggi approfondisco i miei studi e la mia tecnica, analizzando e trascrivendo opere di suonatori di Oud passati e attuali.

I vari progetti musicali relativi all'Oud disponibili su questo sito sono identificati con la parola chiave [oud](/it/tags/oud/).



