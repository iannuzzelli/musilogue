# oud <br>ricerca e <br>materiali

In aggiunta ad alcuni articoli scritti durante i miei studi alla SOAS University, diverse guide e analisi musicali sono disponibili su questo sito, identificate con la parola chiave [ricerca](/it/tags/ricerca/). [Contattami](/it/musilogue/contatti/) pure per domande, suggerimenti o proposte di ricerca.

