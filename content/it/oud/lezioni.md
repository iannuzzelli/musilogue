# Lezioni <br>di Oud

Ho insegnato musica per molto tempo, e sono sempre felice all'idea di lavorare insieme a studenti che abbiano voglia di scoprire uno strumento o una musica nuova.

Se interessato, prepara per favore una proposta di lavoro e [contattami](/it/musilogue/contatti/), poi possiamo parlarne di persona o per telefono.

Alcuni degli argomenti di cui mi occupo e che gradisco insegnare e approfondire insieme ai miei studenti sono i diversi stili dell'Oud (arabo, turco, iracheno, persiano), i sistemi modali in uso nel Vicino e Medio Oriente, analisi musicale, i Makam iracheni, _taqasim_ (improvvisazione), repertorio avanzato per Oud e le opere di Serif Muhiddin Haydar / Targan e Jamil Bashir.

Le lezioni individuali possono svolgersi di persona oppure online (Skype / Zoom / Google Meet). Preferisco non insegnare principianti, gradisco un minimo di familiarità con la notazione musicale e con uno strumento (non necessariamente l'Oud).

