# progetti <br>in corso

Al momento sono principalmente impegnato come [compositore](/it/progetti/composizione/), scrivendo musica che fonde le mie diverse esperienze in musica modale, contrappuntistica e orchestrale.

Sono particolarmente interessato a ensembles di strumenti medio-orientali, a comporre per media e a contribuire a progetti artistici di impatto sociale.

Occasionalmente suono dal vivo in [concerti](/it/eventi/), come solista o in piccoli gruppi. Continuo con i miei studi musicali, sia in maniera indipendente che con diversi insegnanti, seguendo i miei vari interessi, come i Makam iracheni, la tradizione persiana, la musica classica ottomana, e strumenti regionali come l'Oud, il Setar, e il Rubab afgano.

Spero che le pagine di questo sito possano rendere un'idea dei miei interessi musicali e delle mie attività. [Scrivimi](/it/musilogue/contatti/) pure per informazioni o proposte di collaborazione.
