# composizione

Amo comporre e ho viaggiato attraverso diversi generi, dal Progressive Rock alla musica modale orientale.

Ho studiato composizione, armonia e contrappunto con Aldo Rossi a Milano, e ho continuato i miei studi nel Regno Unito (Goldsmiths College, SOAS) e specificatamente in composizione e orchestrazione per film e televisione alla [Berklee Online](https://online.berklee.edu/).

Gli articoli contenenti composizioni originali su questo sito sono identificati con la parola chiave [composizione](/it/tags/composizione/) e elencati qui sotto.
