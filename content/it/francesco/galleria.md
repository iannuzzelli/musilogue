
*Puoi utilizzare queste immagini secondo la licenza [Creative Commons Attribuzione - Non commerciale - Condividi allo stesso modo 4.0 Internazionale](https://creativecommons.org/licenses/by-nc-sa/4.0/deed.it).*

*Altre informazioni sulla licenza d'uso sono disponibili nella pagina su [termini e condizioni](/it/musilogue/info/). Per immagini in alta risoluzione, [contattami](/it/musilogue/contatti/)*
