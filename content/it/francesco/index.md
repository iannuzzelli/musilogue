
# francesco iannuzzelli

![Francesco Iannuzzelli](/images/small/c/francesco.jpg){.img-left .noshadow} 
Nato a Milano, in Italia, dopo un ventennio a Londra ora vivo a Rotterdam.

Originariamente chitarrista, mi sono perfezionato in chitarra classica e chitarra Jazz con Alberto Pizzigoni a Milano, e ho studiato altri strumenti occidentali come violino, contrabbasso e pianoforte, fino a quando non ho scoperto l'Oud durante i miei viaggi come attivista pacifista in Medio-Oriente nel 2003.

Da allora ho studiato musica classica Araba, Ottomana e Persiana sull'Oud con diversi insegnanti turchi, iracheni e iraniani, completando un Master in Music Performance alla SOAS University di Londra, con specializzazione in musica Medio-Orientale. Tutt'oggi mi occupo di varie attività educative e musicali mentre approfondisco i miei studi su altri strumenti (Setar, Tar, Rubab, Tambur) e tradizioni (Iran, Afghanistan).

Ho studiato composizione e contrappunto per diversi anni con Aldo Rossi a Milano, e ho scritto, arrangiato e prodotto musica per diversi progetti e ensemble. Ho studiato orchestrazione per Film e TV alla Berklee Online e al momento lavoro per commissione o per libera iniziativa su progetti che fondono le mie diverse esperienze. Nel 2015 ho vinto la [Competizione Internazionale di Composizione dell'Accademia Araba di Musica](/it/posts/samai_hijaz/).

Maggiori informazioni sulle mie qualifiche e esperienze sono disponibili nel [CV](/docs/cv_it.pdf).
