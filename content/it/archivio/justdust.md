# just dust

![Just Dust al Baritono, CPM, Milano, 1992](/images/inline/c/jd_baritono.jpg){.shadow}

I Just Dust erano Massimo Morichelli al basso e alla voce, Francesco Iannuzzelli alla chitarra, Massimiliano Valerio alle tastiere e Matteo Pozzi alla batteria.

Per un breve periodo negli anni '90 hanno proposto nella scena underground di Milano misto di progressive rock e blues con influenze classiche.

![Preludio all'alba - Cover](/images/small/c/jd_preludio.jpg){.img-left .noshadow}

La loro prima e unica opera è stata *"Preludio all'alba"* (copertina qui a sinistra ), pubblicata da M.A.P. nel 1992. Alcuni brani:

- [Hebel](/audio/Hebel.mp3)
- [Libellule Turchesi](/audio/Libellule%20turchesi.mp3)
- [Innocence](/audio/Innocence.mp3)

Inoltre è qui possibile ascoltare "Along the War", registrata dal vivo al Magia Music Meeting (Milano) il 2 Aprile 1992: [Along the War](/audio/Along%20the%20War.mp3)
