# dafarahn

Dafarahn era un duo Oud e Contrabbasso, composto da Francesco Iannuzzelli e dalla contrabbassista francese Lucile Belliveau, che è rimasto in attività dal 2014 al 2016, suonando in varie città europee in collaborazione con diversi percussionisti.

Il loro repertorio include adattamenti moderni di repertorio classico iracheno, persiano e ottomano, insieme a composizioni originali.

![Pomegranate Tiles](/images/small/c/pomegranate_tiles.jpg){.img-left .noshadow} Nel 2014 hanno pubblicato il loro primo disco, [Pomegranate Tiles](https://dafarahn.musilogue.com/#music), registrato al SOAS Studio di Londra. 

Nel 2015 sono stati selezionati per il Nour Festival di Londra, insieme al percussionista iraniano Hadi Alizadeh.

Maggiori informazioni, audio e video sono disponibili sul sito https://dafarahn.musilogue.com/it

![Dafarahn](/images/inline/c/dafarahn.jpg){.hidden}
