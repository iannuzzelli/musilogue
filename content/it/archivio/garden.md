# garden of <br>remembrance

Garden of Remembrance è un concept-disc, registrato in casa con le risorse disponibili allora (anno 2000), ovvero un campionatore EMU 3000, nel sue semplici librerie digitali e una chitarra classica amplificata.

La musica è basata su vari eventi svoltisi in 4 giorni a Londra, è può essere ascoltata nella seguente playlist di SoundCloud

<iframe id="sc-playlist" scrolling="no" frameborder="no" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/playlists/268569111&amp;color=ff5500&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false"></iframe>

![Garden of Remembrance - Cover](/images/inline/c/garden.jpg){.hidden}
