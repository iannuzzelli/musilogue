# chitarra araba

![Chitarra](/images/inline/c/guitar.jpg){.shadow .img-left} 

L'idea di usare la chitarra per repertorio del Medio-Oriente non è certamente nuova. Non solo esistono opere ispirate alla musica orientale (come il famoso Capriccio Arabo di Francisco Tarrega per chitarra classica), ma molti chitarristi hanno adattato modalità e melodie di origine Araba alla chitarra, spesso mescolandole con tecniche di Flamenco.

Più che Flamenco e nostalgie andaluse, ero interessato all'uso del finger-picking, e ho provato a fare alcune trascrizioni e arrangiamenti di repertorio mediorientale. Questo progetto è stato ormai abbandonato, avendo riscontrato che la presenza dei tasti è troppo limitante, e al tempo stesso non trovando interessante il suono della chitarra senza tasti.

Ad ogni modo, qui ci sono alcuni esempi:

[Chi Mali Wali](/audio/Chi%20Mali%20Wali.mp3) (Canzone tradizionale irachena)

[Chi Mali Wali - Spartito per chitarra](/docs/chi_mali_wali_guitar.pdf)

[Longa Hijaz Kar Kurd](/audio/Longa%20Hijaz%20Kar%20Kurd.mp3) (di Kemani Sebuh Efendi)

[Longa Hijaz Kar Kurd - Spartito e tablatura per chitarra](/docs/longa_hijaz_kar_kurd_guitar.pdf)
