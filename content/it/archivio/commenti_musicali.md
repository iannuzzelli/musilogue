# commenti <br>musicali

Negli anni '90 la Fonit Cetra, casa discografica della RAI, ha pubblicato diversi CD con musica originale da essere utilizzata per commenti sonori di produzioni televisive.

Due di questi CD includono composizioni del mio insegnante di chitarra Alberto Pizzigoni, del mio insegnante di composizione Aldo Rossi e mie. Il primo CD era in stile Jazz, e il secondo Barocco. Da quest'ultimo, ecco una breve danza [Loure](/audio/Loure.mp3)

![Commenti Musicali - Cover](/images/small/c/commenti_musicali.jpg){.noshadow}
