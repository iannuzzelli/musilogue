
# contatti

telefono / whatsapp: <span class="fa fa-phone"></span><a href="tel:+393483006094">+39 3483006094</a>

email: <span class="fa fa-envelope"></span>[francesco@musilogue.com](mailto:francesco@musilogue.com)

telegram: https://t.me/iannuzzelli

facebook: https://www.facebook.com/iannuzzelli

## segui

RSS feed: https://musilogue.com/posts_it.rss

SoundCloud: https://soundcloud.com/musilogue

Canale Youtube: https://www.youtube.com/@musilogue

Instagram: https://instagram.com/musilogue/

Pinterest: https://pinterest.com/musilogue/
