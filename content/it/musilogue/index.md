
Musilogue è un monologo che vorrebbe diventare un dialogo in musica.

In altre parole, è un modo per condividere musica, scritti e informazioni varie sulla mia ([francesco](/it/francesco/)) attività musicale, per renderla disponibile online e chissà anche per fare in modo che succedano cose interessanti.

Musilogue è anche produzione musicale, un sito internet, forse un giorno una piattaforma aperta a collaborazioni esterne.

Per il momento, è prevalentemente uno spazio dove vagare e scoprire qualcosa sulla mia musica.

I contenuti del sito non sono presentati in maniera tradizionale, ma sparsi in uno spazio aperto che può essere esplorato cliccando sui bordi dello schermo o usando i tasti-freccia della tastiera.

Per facilitare la navigazione, puoi voler leggere la guida o usare la mappa del sito, ambedue presenti in basso a destra.

