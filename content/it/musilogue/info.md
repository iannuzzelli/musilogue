
# termini e condizioni

Progettazione e implementazione del sito a cura di Francesco Iannuzzelli. Codice sorgente disponibile su https://gitlab.com/iannuzzelli/musilogue e rilasciato con licenza GPLv3. Altri pacchetti software sono elencati nel [README](https://gitlab.com/iannuzzelli/musilogue/blob/master/README.md).

Tutti i contenuti (immagini, testi, musica, video) sono &copy; Francesco Iannuzzelli.

Le tracce musicali, le composizioni, gli spartiti e gli scritti originali come le analisi musicali, le guide e le trascrizioni, sono tutti rilasciati secondo la licenza [Creative Commons Attribuzione - Non commerciale - Condividi allo stesso modo 4.0 Internazionale](https://creativecommons.org/licenses/by-nc-sa/4.0/deed.it), a meno che non sia indicato diversamente.

Ciò significa che sei libero di usarli, adattarli e condividerli, fin tanto che citi la provenienza e li condividi alle medesime condizioni. Non è
però concesso il loro uso a scopi commerciali. In caso, [contattami](/it/musilogue/contatti/).

Alcune immagini non sono mie e l'autore è indicato nella didascalia; queste immagini *non* sono disponibili ad essere riutilizzate. Tutte le immagini senza crediti sono di Francesco Iannuzzelli e, insieme a quelle esplicitamente attribuite a me, sono disponibili secondo la medesima licenza [Creative Commons](https://creativecommons.org/licenses/by-nc-sa/4.0/deed.it).

Questo sito non usa cookies e quindi non appare quel noioso avviso. Terze parti come YouTube e Google possono usarne.

Sono l'unico responsabile dei contenuti di questo sito. Lamentele, suggerimenti, correzioni e, perché no, complimenti, [sono benvenuti](/it/musilogue/contatti/). 
