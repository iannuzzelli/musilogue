# Restauro di un antico Rubab afgano

Breve storia di un antico Rubab afgano, trovato in una casa d'aste a Berlino e restaurato dalle sapienti mani di [Javier González](https://www.facebook.com/javier.gonzalez.37454961)

Quando comprai questo Rubab nell'agosto 2021 all'[Historia Auktionhaus](https://www.the-saleroom.com/en-gb/auction-catalogues/historia-auktionhaus-berlin/catalogue-id-srhist10035/lot-c31a5973-554b-4a3b-8211-ad6e010f0aee) di Berlino, fui subito colpito dalle sue caratteristiche. Si trattava chiaramente di uno strumento antico, verosimilmente del 19esimo secolo, di ottima manifattura, ma in pessime condizioni.

Sembrava aver sofferto diversi traumi nella sua vita, ed esser stato restaurato più volte. Questi lavori di restauro erano stati svolti da esperti, che ne avevano preservato le qualità estetiche e sonore. Ora aveva bisogno di un altro esperto per esser riportato in vita, e sapevo a chi rivolgermi: Javier González, che aveva già restaurato un antico Rubab per me in passato.

Le condizioni originali dello strumento sono visibili nelle [foto](https://www.facebook.com/media/set/?set=a.10222909193745449&type=3) scattate da Javier stesso. La parte superiore della tastiera era gravemente danneggiata. Diversi intarsi erano andati persi, quasi tutti i piroli avevano bisogno di essere rinforzati, e la pelle ovviamente andava sostituita. Anche la cassa armonica necessitava di attenzione. Javier accettò la sfida con grande pazienza e dedizione.

Un anno più tardi, lo strumento è pronto, tornato in vita con un suono magnifico. La qualità del lavoro di Javier è ben visibile in questo [album fotografico](https://www.facebook.com/media/set/?set=a.10222909320348614&type=3). La tastiera è stata accuratamente restaurata, i piroli delle corde simpatiche funzionano perfettamente, e dei moderni piroli [pegheds](http://www.pegheds.com/) rendono l'accordatura delle corde principali un piacere. Javier ha anche sostituito le teste dei piroli, mantenendo quelli originali sopra l'anima moderna.

![Piroli meccanici per Rubab](/images/inline/c/rubab_berlin_1.jpg)

Gli intricati intarsi sono stati tutti puliti e reinseriti con una nuova resina. La cassa armonica riportata alla sua bellezza originaria. E molti altri dettagli che si possono vedere nelle foto.

![Dettaglio della tastiera del Rubab restaurato](/images/inline/c/rubab_berlin_2.jpg)

Seguendo il consiglio di Javier, ho scelto di sostituire la pelle con una sintetica, per diversi motivi. Innanzitutto, il principio morale di evitare di usare pelli animali per strumenti musicali. Ma anche la necessità di una pelle più resistente e stabile nel clima freddo umido in cui vivo nel Nord Europa. Javier ha dedicato anni a perfezionare le sue pelli sintetiche per Rubab, e lo standard che ha raggiungo è ammirabile. È ben difficile distinguerla dal suono di una pelle animale. In aggiunta, sembra avere una migliore risposta dinamica.

![Pelle sintetica per Rubab](/images/inline/c/rubab_berlin_3.jpg)

Le tre corde melodiche sono la muta [Aquila Afghan Rabab Sugar Set](https://aquilacorde.com/musica-moderna/afghan-rabab/).

È stato un piacere visitare Javier di persona e vedere il suo workshop, vicino Valencia. Ci tengo ad esprimere la mia più sincera gratitudine per il lavoro che ha svolto con immensa passione e attenzione. Le soluzioni innovative che ha trovato per il restauro e il miglioramento del Rubab afgano sono straordinarie.

Mi sembra giusto condividerne il suono: ecco una modesta registrazione, con microfono diretto e senza effetti, della canzone afgana "Yo Palom Yaar Lavangeen"

[embed:https://www.youtube.com/watch?v=LNGmKBCHEN4]

La nostra comune passione per il Rubab e la cultura musicale che rappresenta ha reso questa storia possibile. Ed è significativo che la sua prima apparizione pubblica sia stata in un evento per famiglie di profughi afgani a Manchester. Ricordiamoci dell'Afghanistan, e continuiamo a sostenere la sua gente in questo periodo difficile.
