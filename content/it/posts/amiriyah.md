# Successe a Amiriyah

[embed:https://www.youtube.com/watch?v=91ZrY04yKiE]

Il 13 Febbraio 1991 il [rifugio antiareo nel quartiere di Amiriyah](https://en.wikipedia.org/wiki/Amiriyah_shelter_bombing) a Baghdad fu colpito da due bombe GBU-97, sganciate da due bombardieri statunitensi.

Il rifugio era da anni utilizzato da civili, prevalentemente donne e bambini, durante i frequenti bombardamenti di Bagdhad. Oltre 400 persone furono uccise, probabilmemte molte di più, visto che il registro fu incenerito dall'esplosione. Il comando militare statunitense era a conoscenza che si trattava di una struttura civile, ma lo promosse ad obiettivo militare senza avere prove concrete.

Il virtuoso dell'Oud iracheno [Naseer Shamma](https://www.shammamusic.com/) ha composto questo pezzo, intitolato "Happened at Al-Amiriyah" (Successe a Almiriyah), per mantenere la memoria, in musica, di quanto accadde quel giorno. Quando scoprii la sua registrazione, in un CD pubblicato dall'[Institut Du Monde Arabe](https://www.discogs.com/release/2299199-Naseer-Shamma-Le-Luth-De-Bagdad-The-Baghdad-Lute), rimasi fortemente colpito dalla sua potenza emotiva.

Nell'anniversario di quel giorno, e in un periodo, come quello attuale, in cui continuiamo ad assistere a massacri di civili in Medio Oriente, ho cercato di esprimere sentimenti simili di dolore e di disdegno verso questa violenza brutale, affrontando questo pezzo strumentale con l'Oud. Non ho la forza spirituale per proporre il finale positivo ed energetico con cui solitamente Shamma conclude il pezzo. Non credo neanche di averne il diritto. Provo più semplicemente a trasmettere una flebile speranza.

Dedico la mia umile esecuzione al popolo iracheno, e al popolo palestinese.

N.B. L'Oud a sette cori con cui suono è accordato in Mi, in quanto non avevo una muta di corde adatta per la tipica accordatura irachena in Fa.
