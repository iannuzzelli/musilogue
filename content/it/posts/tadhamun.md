# 10 anni di Tadhamun

[embed:https://www.youtube.com/watch?v=zZZCQJszd4M]

In concomitanza con il decennale della fondazione dell'associazione di donne irachene [Tadhamun](http://solidarityiraq.blogspot.it/), si è svolto alla galleria P21 di Londra un incontro con testimonianze di donne arabe (Irak, Tunisia e Palestina), poesia e musica.

Con il mio intervento musicale ho voluto ricordare le storie poco note di alcune cantanti e compositrici irachene, come Masouda Al-Amarathly e Salima Murat.
