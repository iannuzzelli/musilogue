# Beating for Beirut

[embed:https://www.youtube.com/watch?v=OhWeEeexhOw]

Più di 15 artisti e musicisti da tutto il mondo hanno suonato per Beirut, raccogliendo fondi per [First Aid Renovation](https://www.facebook.com/FirstAidRenovation/), un'iniziativa creata dall'attivista e accademica Sally Hammoud, insieme ai suoi studenti e colleghi, allo scopo di aiutare le famiglie colpite dall'esplosione di Beirut e ripristinare le loro case.

I fondi sono stati raccolti con un [concerto virtuale](https://www.live-actions.ardkon.com/BeatingForBeirut) e questo è stato il notro contributo musicale, che include un paio di composizioni di Marcel Khalife.

Con [Rachel Beckles Willson](https://www.rachelbeckleswillson.com/) (udu) e Francesco Iannuzzelli (oud).
