# Ascoltate le donne

Uno tra i tanti dolorosi effetti del lockdown è stato l'incremento della violenza domestica contro le donne. Questo nostro concerto da casa vuole offrire un momento di riflessione su due aspetti strettamente connessi nella vita delle donne: i loro corpi, oggetto di violenza e di controllo, e la loro creatività, soppressa, sdiminuita o fatta propria dagli uomini. Il programma del concerto mette in risalto la bellezza di voci e di storie femminili nascoste, accompagnando le ascoltatrici e gli ascoltatori in un viaggio in musica dall'Afghanistan alla Sicilia, passando attraverso l'Irak, l'Iran, la Turchia e la Grecia. 

Con [Rachel Beckles Willson](https://www.rachelbeckleswillson.com/) (voce, sax, oud) e Francesco Iannuzzelli (oud, setar, chitarra, rubab). Online dal vivo sulla piattaforma [Living Room Live](https://www.livingroom-live.com/)

[embed:https://www.youtube.com/watch?v=e7HQjWkLlhk]

### Eya fratres personemus

Un brano dal Tropario di Catania, un codice del dodicesimo secolo creato per la cattedrale di Catania, in Sicilia. Celebra Sant'Agata, che rifiutò le avances del prefetto romano Quinziano e di conseguenza venne brutalmente torturata e condannata a morte; morì in prigione. È la santa protettrice delle vittime di stupro, delle malate di tumore al seno, delle nutrici e dei fonditori di campane (per via della forma dei suoi seni recisi). Il canto ricorda gli avvenimenti del 1126, quando alcune reliquie di Santa Agata vennero riportate in Sicilia, dopo che erano state trafugate e portate a Costantinopoli dal generale bizantino Maniace nel 1040.

### Souda Shlahhani (‘Come posso essere stata così distratta?’) e Einah Shakabrah (‘I suoi occhi affascinanti’): due canzoni di Massuda Al-Amarathl

Massuda Al-Amarathl nacque nell'Iraq meridionale all'inizio del ventesimo secolo. Di origini africane e appartenente a una famiglia di schiavi, visse nella comunità tollerante di Al Abu Hammed. Visse sotto sembianze maschili e diventò famosa come cantante e compositrice. Alla fine, coinvolta in un triangolo amoroso con due altre donne, venne uccisa da una di esse.

### Semai Evcara di Dilhayat Kalfa

Questo brano strumentale classico dall'Istanbul ottomana fu composto dalla prima compositrice della tradizione ottomana. Il genere dei compositori non è sempre chiaro nei documenti storici, ma solo un'altra compositrice è stata riconosciuta con certezza. Dilhayat Kalfa visse nel diciottesimo secolo, suonò il liuto a manico lungo noto come tanbur, e a lei viene attribuita la creazione del "makam" (modo) Evcara, in cui è composto questo brano musicale.

### Gül olsam ya sünbül olsam beni koklar mıydın (‘Mi annuseresti se fossi una rosa?’) di Neveser Kökdeş 

Neveser Kökdeş visse come compositrice durante il periodo iniziale della repubblica turca, e cercò di creare musica che riflettesse il nuovo contesto secolare, e per questo venne aspramente criticata. Ebbe una vita difficile, perso il marito mentre era incinta, e una paralisi facciale sfigurò il suo volto. Dopo la perdita del fratello, cominciò a distruggere, bruciandole, le sue numerose composizioni, di cui solo un centinaio sono rimaste. Questa canzone d'amore parla di bellezza fisita: "Se fossi una rosa, mi annuseresti, se fossi sul tuo petto, mi accarezzeresti?")

### Morgh-e Sahar (‘Uccello dell'alba’), di Mortażā Naydāwud, su versi del poeta Moḥammad-Taqi Malek-al-Šoʿarāʾ Bahār

Abbiamo scelto questa canzone classica e intensamente amata in Iran perché fu eseguita per la prima volta dalla famosa mezzo-soprano Qamar-al-Moluk Vaziri nel suo debutto al Grand Hotel di Tehran nel 1924. In questa occasione storica fu la prima donna a cantare in pubblico senza velo, circostanza che - fino alla rivoluzione del 1979 - ha avuto una profonda influenza. È nota come la "regina della musica persiana".

### Eisai Fantis (‘Sei un furfante’), di Grigoris Asikis

Questa canzone è fortemente associata alla cantante di rebetico Rita Abatzi, nata a Smirne (oggi Ismir) verso la fine dell'era ottomana, ed in seguito costretta a emigrare per via della separazione tra Grecia e Turchia del 1923. La sua carriera discografica come cantante di rebetico ebbe come unica rivale Roza Eskenazi. Estremamente popolare negli anni Trenta, smise di cantare dopo la Seconda Guerra Mondiale. Questo brano parla del tradimento di una donna da parte di un "birbante" e della sua vendetta con una (metaforica) bomba.

### Cu ti lu dissi (‘Chi te lo ha detto?’), di Rosa Balistreri

Questa famosissima canzone della leggendaria cantante siciliana parla del primo amore, del suo amore lancinante, e della difficoltà del lasciarsi. La vita turbolenta di Rosa Balistreri fu drammatica come la sua voce. Sposata a sedici anni, tentò di uccidere il proprio marito quando perse al gioco il corredo della figlia. Si costituì, finendo in prigione (il marito sopravvisse). Incinta del figlio di una famiglia nobile presso cui lavorava, venne accusata di furto e nuovamente arrestata, perdendo il bambino, in seguito fu anche vittima degli abusi di un prete...

### Koodake Afghan Lallo

Concludiamo il nostro programma tumultuoso con una rilassante ninna-nanna dall'Afghanistan, che abbiamo scoperto tra le registrazioni di Farida Mahwash, cantante nata a Kabul nel 1947. Mahwash è l'unica donna afgana a cui sia stato riconosciuto il titolo di "Ustad", un termine di genere che viene usato nel mondo islamico per insegnanti e artisti, la cui connotazione sessista è problematica tanto quanto gli appellativi "Maestro" e "Master" in uso in Occidente.
