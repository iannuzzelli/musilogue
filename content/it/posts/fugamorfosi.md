# Fugamorfosi

Originariamente scritto per violoncello e clarinetto, questo pezzo è stato orchestrato durante i miei studi alla Berklee.

[Fugamorfosi](/audio/Fugamorfosi.mp3)

Questa opera si basa sull'idea di orchestrare un dialogo contrappuntistico usando una selezione di note che gradualmente si trasforma.

La selezione di note è composta da due esadi, ovveri due insiemi di 6 note ciascuno, in ordine casuale, usando tutte le 12 note della scala cromatica.

Il principale vincolo è che le note di un insieme sono sempre a un'intervallo di 6 semitoni (un tritono) dalla corrispondente nota nell'altro insieme. Ovvero questi due insiemi possono essere visti come due coesistenti modi esatonali.

La modulazione da una selezione all'altra avviene scambiando una nota da un insieme con il suo complementare tritono dall'altro insieme.

Maggiori dettagli sono disponibili nel [commento](/docs/fugamorfosi_commento.pdf)

[Spartito orchestrale](/docs/fugamorfosi.pdf)

![Fugamorfosi table](/images/inline/c/fugamorfosi_table.jpg){.hidden}
