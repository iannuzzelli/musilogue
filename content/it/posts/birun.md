# Maftirim - Ensemble Bîrûn

Bîrûn è una serie di workshop avanzati in musica classica ottomana, organizzati dall'Istituto Interculturale di Studi Musicali Comparati della Fondazione Giorgio, che si tiene ogni anno a Venezia sotto la direzione di Kudsi Erguner.

La sua quarta edizione dell'Aprile 2015 ha riunito musicisti professioni e semi-professionisti per lavorare sui Maftirim, ovvero le opere di compositori ebrei sefarditi del periodo ottomano.

Un CD comprendente 14 tracce di questo repertorio, registrato dai musicisti durante i giorni del workshop, è stato publicato da 
[Nota Edizioni](https://www.notamusic.com/prodotto/i-maftiri%CC%82m-e-le-opere-degli-ebrei-sefarditi-nella-musica-classica-ottomana/?lang=en). 
Maggiori dettagli sul progetto sono disponibili sul sito della [Fondazione Cini](http://www.cini.it/publications/maftirim-le-opere-degli-ebrei-sefarditi-nella-musica-classica-ottomana). 

Alcuni estratti del CD possono essere ascoltati nella puntata dell'11 Maggio 2016 del programma 
[Radio 3 Suite](http://www.radio3.rai.it/dl/portaleRadio/media/ContentItem-eea5fe5b-1619-4e6a-ba2f-583f9c987152.html).

