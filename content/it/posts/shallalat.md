# Shallalat - Jamil  Bashir

[embed:https://www.youtube.com/watch?v=Ca4z6vO3CcQ]

Ricordando Jamil Bashir nell'annniversario della sua morte (24/9/1977) con un arrangiamento per Oud a 7 cori della sua composizione [Shallalat](https://www.youtube.com/watch?v=TJWlWvRzFB8).

Spartiti:
- [trascrizione approssimata del tema originale](/docs/shallalat.pdf)
- [arrangiamento per Oud a 7 cori](/docs/shallalat7.pdf)

Liutaio: [Amjed Saeed](https://www.facebook.com/profile.php?id=100012533195529)

