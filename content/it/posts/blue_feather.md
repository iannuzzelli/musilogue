# Blue Feather

Una breve composizione, scritta sperimentando con alcune librerie digitali per archi in combinazione con Oud e contrabbasso

[Blue Feather](/audio/Blue%20Feather.mp3)

![Fawzi Monshed sound-hole](/images/inline/c/monshed_hole.jpg){.hidden}
