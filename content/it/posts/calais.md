# Festival "La saveur de l'autr" - Calais

Musicisti, cantanti e appassionati di musica araba posso incontrarsi regolarmente e fare musica insieme in un contesto informale alla SOAS (School of Oriental and African Studies) di Londra.

Nel 2015 un gruppo di msuicisti ha formato la SOAS Arabic Band, che si è esibita in vari eventi a Londra e in particolare al festival [La saveur de l'autr](http://www.psmigrants.org/site/festival-la-saveur-de-lautre-29-31-mai-2015-le-channel-calais/), organizzato da diverse associazioni attive con i migranti a Calais, in Francia.

In questa circostanza il gruppo era composto dalla cantante Christelle Madani, da Saied Silbak e Francesco Iannuzzelli all'Oud, e Remy Geoffroy alle percussioni.

Il programma comprendeva prevalentemente canzoni popolari arabe, come si può ascoltare nei video seguenti di due canzoni di Fairuz.

[embed:https://www.youtube.com/watch?v=aqBl8rQr3hU]

[embed:https://www.youtube.com/watch?v=Rbl67x_5Nj8]

La serata ha però preso una svolta significativa dopo la fine del concerto "ufficiale", quando un gruppo di migranti Sudanesi è salito sul palcoscenico per una performance improvvisata di loro canzoni.

[embed:https://www.youtube.com/watch?v=QqcsDR8jMKM]

[embed:https://www.youtube.com/watch?v=xUgNQQ_BVU4]

Per maggiori informazioni e video della SOAS Arabic band, si veda il suo [canale YouTube](https://www.youtube.com/channel/UCvpDn3-6KVDyethXHdVKivA) e la sua [pagina Facebook](https://www.facebook.com/arabicmusicsoas/)