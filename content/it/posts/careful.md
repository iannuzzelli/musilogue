# Careful - Jim Hall

[embed:https://www.youtube.com/watch?v=Cx32tU7piJs]

Questa interessante composizione del leggendario chitarrista jazz Jim Hall presenta diverse difficoltà (per questo si intitola "Careful", ovvero "Stai attento").

Si tratta di un inusuale blues in 16 battute in scala alternata diminuita semitono-tono. Una specie di studio, sia per l'aspetto musicale che chitarristico.

Nella sua esecuzione ho voluto provare ad esercitarmi nei passaggi tra flat-picking, hybrid-picking e finger-picking, nascondendo il plettro tra l'indice e il medio quando non necessario. Ho usato l'ottima trascrizione di [Miguel Mateu Solivellas](https://payhip.com/b/48Py) del video [Jim Hall's live performance in 1989](https://www.youtube.com/watch?v=5PzshdVRavk&t=0s). 

La chitarra è una Gibson ES175 del 1974, appartenuta al mio insegnante Alberto Pizzigoni.
