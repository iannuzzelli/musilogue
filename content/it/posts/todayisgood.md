# Today is Good!

Dal 2017 mi sono trasferito insieme a [Rachel](https://rachelbeckleswillson.com) in Sicilia per collaborare, come volontari, a vari progetti educativi e di accoglienza per minori stranieri non accompagnati, e in genere per migranti. Fra le altre cose, abbiamo da subito cominciato a svolgere dei regolari laboratori di musica presso alcuni centri di accoglienza, che si sono presto trasformati in un energetico e gioioso caos creativo da cui, dopo qualche mese di lavoro, è nato il CD "Today is Good!".

Le canzoni sono tutte originali, composte dai ragazzi, arrangiate e registrate con i limitati strumenti che avevamo a disposizione. Sono le loro storie, intrise di speranze, denunce, riflessioni e voglia di vivere.

Maggiori informazioni sono disponibili sul sito [Today is Good](https://www.todayisgood.org), dove si possono anche ascoltare e acquistare i brani del CD (tutti i profitti vanno ai ragazzi, autori di musica e parole). 