# Etude 6 - Julian Lage

[embed:https://www.youtube.com/watch?v=pTJOwqmEU80]

Julian Lage sta rivoluzionando il modo di suonare la chitarra. Il suo stile è unico: rapsodico, contrappuntistico, espressivo, non convenzionale e al tempo stesso profondamente radicato nella tradizione classica e jazzistica, unito a straordinarie capacità tecniche.

Questo studio racchiude tutte queste caratteristiche, ed ha appunto richiesto parecchio lavoro, dovendo anche trovare soluzioni alternative là dove la sua diteggiatura mi risultava semplicemente impossibile.
