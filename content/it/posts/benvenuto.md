# Benvenut*!

Il nuovo sito comincia oggi, con un lancio discreto.

Ancora molti aspetti sono in lavorazione e l'approccio visuale è un po' particolare; per indicazioni, puoi usare la guida o la mappa presenti in basso a destra.

Ho deciso di rilasciare tutti i miei materiali musicali (composizioni, scritto, spartiti) secondo la licenza [Creative Commons Attribuzione - Non commerciale - Condividi allo stesso modo 4.0 Internazionale](https://creativecommons.org/licenses/by-nc-sa/4.0/deed.it), quindi sei libero di utilizzarli in base alle condizioni stabilite dalla licenza stessa.

Maggiori dettagli sono disponibili nella pagina dei [termini e condizioni](/it/musilogue/info/).

Buona immersione

![Musilogue](/images/small/c/musilogue.png){.noshadow}
