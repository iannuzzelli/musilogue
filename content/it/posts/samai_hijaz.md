## Samai Hijaz

Questo Samai è stato originariamente composto per Oud solista ed in seguito adattato per un'orchestra occidentale. In questo processo ha subito numerose trasformazioni ritmiche e strutturali, descritte in dettaglio nel [commento](/docs/samai_hijaz_commento.pdf).

Rappresenta il mio primo tentatitvo di orchestrazione di musica mediorientale, e una registrazione con librerie digitali può essere ascoltata qui [Samai Hijaz](/audio/Samai%20Hijaz.mp3)

Lo [spartito orchestrale](/docs/samai_hijaz.pdf) può essere scaricato liberamente, mentre le parti sono disponibili su richiesta.

![Premio Internazionale di Composizione Musicale](/images/small/c/award.jpg){.img-left .noshadow}
Nel 2015 questo Samai ha vinto il prestigioso [Premio Internazionale di Composizione dell'Accademia Araba di Musica](https://www.arabmusicacademy.org/article/9), ed è stato eseguito pubblicamente durante la cerimonia di consegna del premio alla USEK University di Kaslik (Libano) il 25 Luglio 2016.
