# Iraqi Maqam Khanabat

[embed:https://www.youtube.com/watch?v=rlJ2Upz_RAs]

Nel giugno 2014 la scuola di musica Taqasim Music School ha organizzato un concerto a [St. Ethelburga's Centre](https://stethelburgas.org/), 
una bella chiesa sconsacrata nel centro di Londra, a cui hanno partecipato gli insegnanti della scuola e altri musicisti ospiti.

Il concerto, intitolato *"Alla scoperta dei Makam iracheni sull'Oud"*, ha prevalentemente presentato repertorio tradizionale iracheno. In questo video, Francesco Iannuzzelli (oud), Lucile Bellivau (contrabbasso), Julia Ana Katarina (violoncello) e Elizabeth Nott (percussioni ) suonano il makam iracheno Khanabat. 
