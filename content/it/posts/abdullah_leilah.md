# Abdullah e Leilah

Felice di aver collaborato, suonando l'Oud, alla colonna sonora di [Abdullah and Leilah](https://www.imdb.com/title/tt5991458/), un bel progetto della regista Ashtar Al Khirsan. Musica composta da [Tara Creme](http://taracreme.com/portfolio/abdullah/)
