# Storia della Musica Persiana

[embed:https://www.youtube.com/watch?v=GI79GQmMZNA]

Un mio piccolo contributo sull'importanza dell'Oud per questo documentario sulla storia della musica dell'Iran, prodotto da [Karen Sabaghi](https://www.facebook.com/KAR.SA2) e trasmesso da ManotoTV il 24 Agosto 2022.
