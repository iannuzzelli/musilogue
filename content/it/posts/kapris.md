# Kapris di Jamil Bashir - Guida allo studio

Questa guida fa parte di una raccolta di materiali per Oud che ho scritto per lezioni con i miei studenti.

"Kapris" è una composizione di Jamil Bashir, pubblicata nella prima edizione del suo metodo per Oud (1961) a pagina 123, all'interno dei pezzi avanzati per il sesto e ultimo anno di studio.

Qui è possibile scaricare la mia guida allo studio di questo breve ma significativo brano, che contiene molti degli aspetti tecnici del noto stile iracheno.

Per correzioni o domande, [scrivetemi](/it/musilogue/contatti/)

[Kapris di Jamil Bashir - Guida allo studio](/docs/jamil_bashir_kapris.pdf)

