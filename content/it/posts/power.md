# Power

L'idea alla basa di questo pezzo è di sconvolgere la tipica gerarchia di potere presente all'interno di una esecuzione musicale, dando ai voltapagine la possibilità, seppur limitata, di essere protagonisti.

Il ruole del direttore (qui detto *cronometrista*) è di supervisionare la performance, ovvero la temporanea manifestazione di potere dei voltapagine, per poi terminarla brutalmente. I musicisti suonano semplicemente quello che gli viene mostrato.

La musica può essere eseguita da qualsiasi gruppo di strumenti, sperimentando con l'imprevedibile sovrapposizione delle parti.

Alcune [istruzioni](/docs/power_istruzioni.pdf) sono fornite per l'ensemble e non devono essere rese pubbliche agli ascoltatori.

In questo esempio, lo [spartito](/docs/power_parts.pdf) è stato preparato per piano e chitarra.

Questo pezzo è stato originariamente scritto per il modulo *"Compose and Perform"* durante i miei studi al Goldsmiths College di Londra.

![Music stand](/images/inline/c/music_stand.jpg){.hidden}
