# Pomegranate Tiles

"Pomegranate Tiles" è il primo CD di [Dafarahn](/en/archive/dafarahn/), ovvero Francesco Iannuzzelli all'Oud e Lucile Belliveau al contrabbasso, in questa occasione accompagnati da Elizabeth Nott alle percussioni.

Il CD, registrato al SOAS Studio nel Luglio 2014, comprende una selezione di Makam iracheni e di repertorio tradizionale del Medio Oriente.

[Makam iracheno Khanabat](/audio/Iraqi%20Maqam%20Khanabat.mp3)

Maggiori informazioni, incluse le note del CD, sono disponibili sul sito [Dafarahn](https://dafarahn.musilogue.com/it#music)
