# De Sålde Sina Hemman (Dovettere vendere le loro case)

[embed:https://www.youtube.com/watch?v=25ktZCh0vfo]

Questa melodia popolare svedese appartiene al genere di canzoni popolari relative alla vasta migrazione dalla Svezia al Nord America che avvenne nella seconda metà dell'800.

Fu poi resa famosa nella versione jazz del quartetto del trombettista Art Farmer, accompagnato dal chitarrista Jim Hall, nell'album "To Sweden With Love" in 1964.

Questa mia versione si basa sull'arrangiamento di Erik van Heijzen, insegnante di chitarra jazz a SKVR Rotterdam.
