# Samai Nahawand - Jamil Bashir

[embed:https://www.youtube.com/watch?v=a8hOY3wkyrg]

Jamil Bashir (1920 – 24/9/1977) è stato un virtuoso e compositore iracheno che ha lasciato un'impronta fondamentale sullo sviluppo delle tecniche strumentali dell'Oud. Allievo di [Serif Muhiddin Haydar](/it/posts/haydar/) all'Istituto di Belle Arti di Baghdad, Jamil Bashir ha applicato il suo talento al repertorio tradizionale mediorientale, aprendo la strada per una nuova generazione di oudisti.

Ha scritto numerose opere originali ed esercizi tecnici, raccolti nel suo metodo di Oud che viene ancora oggi usato nei conservatori arabi. Questo Samai Nahawand è stato composto nel 1957 a imitazione del famoso Samai Nihavent di Mesut Cemil Bey, di cui era amico e con cui ha spesso suonato e insegnato a Baghdad. Si tratta di un Samai atipico, con passaggi molto tecnici e arpeggi che volteggiano sopra il canonico 10/8, mentro il quarto movimento è in un altrettanto atipico ritmo di 7/8.

Si tratta di una delle sue composizioni più difficili, chiedo scusa per gli inevitabili errori di questa registrazione casalinga.
