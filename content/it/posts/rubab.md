# Trascrizioni di musica afgana per Rubab

Pubblico su questo sito una collezione di trascrizioni per Rubab fatte, raccolte o riarrangiate nel corso dei miei studi.

Alcune sono basate su trascizioni fatte da altri (indicati nei crediti), ma la maggior parte sono state fatte da me e quindi probabilmente contentono degli errori. La translitterazione dei titoli e dei nomi è anch'essa approssimativa. Per favore [segnalatemi](/it/musilogue/contatti/) eventuali correzioni e aggiunte. 

La mia intenzione è prevalentemente rivolta alle caretteristiche strutturali e melodiche dei vari pezzi, senza dettagli riguardanti gli ornamenti e le ripetizioni che comunque variano notevolmente nelle diverse versioni. Considerate quindi queste trascrizioni come degli "scheletri" che necessitano di interpretazione.

Le condivido nella speranza che possano essere utili ad altri studenti di Rubab: stato state tutte scritte in notazione occidentale e in Sargam, considerando Do = Sa. Ove possibile, ho linkato dei video su YouTube, che non corrispondono necessariamente a quanto scritto nelle trascrizioni, ma vengono solo indicati per offrire un riferimento.

Ringraziamenti: 

- [Online Afghan Rubab Tutor](http://www.oart.eu/) di John Baily
- le accurate trascrizioni fatte da [Alessandro Bartolucci](https://www.facebook.com/alessandro.bartolucci2/media_set?set=a.10203463839590131&type=3)
- [Daud Khan Sadozai](https://www.daud-khan.art/), i cui seminari di Rubab presso [Labyrinth Musical Workshop](https://www.labyrinthmusic.gr/en/) sono stati un'importante fonte di informazioni
- [Nashenas Naujawân](http://learnpushtomusic.org/) per aver fornito dei titoli mancanti e alcune correzioni
