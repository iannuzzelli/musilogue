# Fingerpicking Paolo Conte

La musica di Paolo Conte è un intrigante miscuglio di armonie jazz sofisticate e melodie accattivanti. Ho voluto intraprendere il progetto di adattarla alla chitarra acustica come una sfida al tempo stesso educativa e affascinante.

La playlist completa può essere ascoltata su https://www.youtube.com/playlist?list=PLawHez4xr-AqLDZPL2cJUplq0L4ZKAe2k, mentre qui di seguito sono inclusi i video individuali.

[embed:https://www.youtube.com/watch?v=pkmvaNoLvwk]

[embed:https://www.youtube.com/watch?v=fyQz2JS18l4]

[embed:https://www.youtube.com/watch?v=lwK6sD3TSPY]

[embed:https://www.youtube.com/watch?v=vMpsI8n8EyQ]

[embed:https://www.youtube.com/watch?v=L_KFVaM2doo]

