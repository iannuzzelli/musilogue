# Dele Diwana

[embed:https://www.youtube.com/watch?v=TmtVngtisxs]

Ho studiato questo bell'adattamento della canzone afgana Dele Diwana con Ustad Homayoun Sakhi alla [Rubab Academy](https://rubabacademy.com/). Video-registrazione pubblicata sul [canale YouTube della Rubab Academy](https://www.youtube.com/channel/UCwELRtEr_ZMdwFDie9oddiQ).
