# Sarajevske Ruze

Crocevia culturale e geografico tra Oriente e Occidente, Sarajevo è famosa per la sua diversità religiosa e culturale. Attualmente capitale della Bosnia-Erzegovina, fu sottoposta ad un tragico assedio di 4 anni durante la guerra bosniaca nel 1992-96.

A causa degli intensi combattimenti, le sue strade furono pesantemente danneggiate da migliaia di colpi di mortaio, con numerosi morti. Le buche rimaste sull'asfalto furono ricoperte con una resina rossa e rappresentano una caratteristica molto particolare nella memoria visiva di quel conflitto armato. Sono note col nome di rose di Sarajevo ("Sarajevske Ruze").

I quattro movimenti di questa composizione musicale, in forma di suite, esplorano diverse fasi dell'assedio di Sarajevo e di cosa possino rappresentare le sue rose. Dall'angosciante attesa dello sviluppo degli eventi, alle corse per sfuggire ai cecchini; dal dolore per la perdita di vite umane alla forza di voler ricostruire una nuova vita.

Originariamente scritto per Oud, Ney, Viola, Kemenche e contrabbasso per il modulo di composizione durante i miei studi alla SOAS University di Londra.

Una versione realizzata con librerie digitali può essere ascoltata qui [Sarajevske Ruze](/audio/Sarajevske%20Ruze.mp3)

[Sarajevske Ruze - Spartito](/docs/sarajevske_ruze.pdf)

![Sarajevo Ramazan Festival](/images/inline/c/sarajevo_library.jpg){.hidden}