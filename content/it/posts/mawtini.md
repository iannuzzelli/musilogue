# Mawtini (La mia patria)

[embed:https://www.youtube.com/watch?v=dc9_qIawh9I]

Cantato ovunque in Palestina e nel mondo arabo, Mawtini è l'inno non ufficiale della Palestina, ma è anche attualmente l'inno nazionale dell'Irak.

Composto dal musicista libanese Mohammed Flayfel, con testo del poeta palestinese Ibrahim Tuqan.

[Arrangiamento per chitarra (spartito & tablatura)](/docs/mawtini.pdf)
