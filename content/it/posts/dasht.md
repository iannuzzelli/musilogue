# Il repertorio modale del Makam Dasht

In questo breve articolo ho scelto un modo musicale e ne ho seguito gli sviluppi in diverse tradizioni musicali appartenente al Medio Oriente.

Noto con nomi simili (Dastgah Dashti in Iran, Maqam Dasht in Iraq e Mugham Dasht in Azerbaijan), il modo Dasht ha delle specifiche caratteristiche melodiche e strutturali, qui presentate con l'analisi di esecuzioni vocali e strumentali.

Le tracce musicali citate nel testo sono disponibili su SoundCloud.

[Il repertorio modale del Makam Dasht](/docs/dasht.pdf)

