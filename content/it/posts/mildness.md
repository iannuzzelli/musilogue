# Mildness

[embed:https://www.youtube.com/watch?v=7U3_nBEkQT8]

In memoria di [Alberto Pizzigoni](https://it.wikipedia.org/wiki/Alberto_Pizzigoni), il mio insegnante di chitarra.

Questo è il tema introduttivo di Mildness, una delle sue belle composizioni per chitarra Jazz

[Trascrizione per chitarra](/docs/mildness_pizzigoni.pdf)