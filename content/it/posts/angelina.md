# Angelina, di Tommy Emmanuel

[embed:https://www.youtube.com/watch?v=e2jUb6HK9J8]

Volevo sperimentare il cosiddetto [hybrid-picking](https://en.wikipedia.org/wiki/Hybrid_picking), tecnica che non ho mai usato prima. Con un plettro normale, che non sopporto proprio il thumbpick. Ho quindi scelto questa composizione del famoso chitarrista australiano Tommy Emmanuel.
