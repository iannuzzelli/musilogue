# Bach e Rubab

[embed:https://www.youtube.com/watch?v=HiRsP0Hi-bo]

Ho spesso utilizzato le composizioni classiche di Bach per strumento solo (violino, violoncello, liuto) come esercizio. La loro estensione, le modulazioni, la straordinaria musicalità sono di grande ispirazione, oltre ad offrire delle sfide tecniche non indifferenti.

Ho quindi pensato di fare lo stesso con il Rubab afgano, con non poche difficoltà e con qualche inevitabile compromesso. Il risultato finale di quello che era cominciato come un esercizio è un video discreto che è stato accolto con grande interesse dal mio insegnante di Rubab Ustad Homayoun Sakhi e pubblicato sul [canale YouTube della Rubab Academy](https://www.youtube.com/@RubabAcademyOfficial/videos).

Per chi può essere interessato, condivido anche lo [spartito e l'intavolatura](/docs/bach_prelude.pdf).
