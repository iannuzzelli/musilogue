# Samai Rast - Jamil Bashir

[embed:https://www.youtube.com/watch?v=UwjlOuialIQ]

Esecuzione di Samai Rast, delicata composizione di Jamil Bashir, virtuoso dell'Oud iracheno scomparso 40 anni fa. Oud costruito da [Mohammed Reza](https://www.facebook.com/mohammed.jabar.16)
