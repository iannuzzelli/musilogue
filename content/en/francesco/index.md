
# francesco iannuzzelli

![Francesco Iannuzzelli](/images/small/c/francesco.jpg){.img-left .noshadow} 
Born in Milan, Italy, I have been living in London on and off since 2001, and I currently live in the Netherlands (Rotterdam).

Originally a guitarist, I perfected my classical and Jazz guitar skills with Alberto Pizzigoni in Milan, and I have been studying other Western instruments such as violin, double-bass and piano, until I discovered the Oud during my travels as a peace activist in the Middle-East in 2003.

Since then I have been studying Arabic, Ottoman and Persian classical music on the Oud with several Iraqi, Turkish and Iranian teachers, while completing a Master in Music Performance at SOAS University, specialising in Middle-Eastern music. I have been, and I still am, involved in a number of educational and performing activities, while expanding my music knowledge with other instruments (Setar, Tar, Rubab, Tambur) and traditions (Iran, Afghanistan).

I studied composition for several years with Aldo Rossi in Milan, and I have been writing, arranging and producing music for various projects and ensembles. I completed a certificate in orchestration for film and TV at Berklee Online and I am currently producing music which mixes my different influences and experiences. In 2015 I won the [International Music Composition Competition by the Arab Academy of Music](/en/posts/samai_hijaz/).

For more details about qualifications and experiences, please see my [CV](/docs/cv.pdf).
