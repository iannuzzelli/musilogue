
*You are welcome to re-use these images under [Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International Licence](https://creativecommons.org/licenses/by-nc-sa/4.0/).*

*More details on licensing in the [info page](/en/about/info/). For high-resolution pictures, please [get in touch](/en/about/contact/)*
