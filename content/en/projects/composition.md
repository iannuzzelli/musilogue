# composition

I love composing music and I have been travelling across several genres, from Progressive Rock in the early days to Modal non-Western music more recently.

I studied composition, harmony and counterpoint with Aldo Rossi in Milan, and continued my music studies in the UK (Goldsmiths College, SOAS) and specifically in composition and orchestration for film and tv at [Berklee Online](https://online.berklee.edu/).

All posts including original music works are tagged as [composition](/en/tags/composition/) and listed below.
