# current <br>projects

I am currently mainly active as a [composer](/en/projects/composition/), writing music that mix my experiences in modal music, counterpoint and orchestration.

I am particuarly interested in ensembles that use non-Western instruments, in writing for media, film, and in contributing to artistic and social projects.

I occasionally perform in [concerts](/en/events/), either as soloist on the [Oud](/en/oud/) and Rubab, or in ensembles based in the UK. I carry on my music studies, both independently and with various teachers, on my current music interests, such as Iraqi Maqam, Persian and Ottoman classical music, and regional instruments like Oud, Setar, and Afghan Rubab.

Hopefully the pages of this site can give an idea of my music interests and activities, please do [get in touch](/en/about/contact/) for proposals of collaboration.
