
## Help

Please do not be upset if you find this site a bit unusual or difficult to use :)

It is partially intentional, as the website is conceived as an open-space for visitors to move around and discover about my music.

You can **navigate in the four directions** (up, down, left, right) when an icon is visible on the relative edge of the screen.

You may also use the **keyboard arrow keys** instead of clicking on the navigation icons.

All audio links open in the player in the bottom-left corner, and you can carry on navigating through pages while the music track is playing. All external links open in a new browser window.

The section dedicated to the **posts** expands on the left of the [homepage](/en/) in chronological order. 

Posts are recognizable from their whitish / gray background images, while all the other pages have a dark background image.

Posts and archive pages are also tagged with **keywords**, offering an alternative approach to content navigation. All keywords are visible in the [tag page](/en/tags/), reachable from the icon in the bottom-right section.

The **sitemap**, whose icon is also always present in the bottom-right corner, offers an overall view of the website structure. Please note that for brevity individual posts and archive pages are not indicated in the sitemap.

The homepage is left intentionally blank.
