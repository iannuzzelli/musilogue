# Transcriptions of Afghan music for Rubab

I am publishing on this site a collection of various music transcriptions for Rubab that I have made, collected, or rearranged in the past few years of study.

Some are based on transcriptions made by others (credited in the score), but most of them have been made by me, and therefore they are likely to have some mistakes. The transliteration of titles, performers and ragas is also approximate and may not be accurate. Please [get in touch](/en/about/contact/) if you have any suggestions or amendments.

I have been mainly interested in capturing the essence of the various songs and melodies that I have transcribed, without all the ornaments and repetitions that in any case will vary between performances.
Please consider them as "skeletons" of melodies that need to be interpreted.
 
I am sharing them with the hope that they could be useful to other Rubab students: they are all written in Western notation and Sargam, assuming Do=Sa, and I have linked some recordings on YouTube, which are not necessarily those that I have used for the transcriptions.  

Credits: 

- [Online Afghan Rubab Tutor](http://www.oart.eu/) by John Baily
- the many accurate transcriptions made by [Alessandro Bartolucci](https://www.facebook.com/alessandro.bartolucci2/media_set?set=a.10203463839590131&type=3)
- [Daud Khan Sadozai](https://www.daud-khan.art/), whose Rubab seminars at [Labyrinth Musical Workshop](https://www.labyrinthmusic.gr/en/) have represented a valuable source of information
- [Nashenas Naujawân](http://learnpushtomusic.org/) for providing some missing titles and corrections 
