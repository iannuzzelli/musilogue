# Samai Nahawand - Jamil Bashir

[embed:https://www.youtube.com/watch?v=a8hOY3wkyrg]

Jamil Bashir (1920 – 24/9/1977) was a virtuoso Iraqi player and composer who left a fundamental mark in the development of Oud playing techniques. An early student of [Serif Muhiddin Haydar](/en/posts/haydar/) at the Baghdad Institute of Fine Arts, Jamil Bashir applied his extraordinary skills to the traditional Middle-Eastern repertoire, opening the path for a new generation of Oud players.

He wrote several works and exercises, collected in his 6-years Oud study method, still very much in use. In this Samai Nahawand, composed in 1957 and modeled upon Mesut Cemil Bey's Samai Nihavent, Jamil Bashir ventures in a non-traditional approach to the Samai form. Fast passages and arpeggios deceive the typical 10/8 rhythmic accents, while the 4th movement features an uncommon 7/8 meter. 

It is one of his most technically challenging compositions, please excuse the various small mistakes of this home recording.