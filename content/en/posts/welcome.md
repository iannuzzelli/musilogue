# Welcome!

The new site opens today, with a soft launch.

A few things are still unfinished, and the visual approach is a bit unorthodox; for directions please check the help and sitemap icons in the bottom-right section.

I have decided to make all my music-related materials (compositions, scores, writings) available under the [Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License](https://creativecommons.org/licenses/by-nc-sa/4.0/), therefore you are free to use them under the conditions of the CC licence above.

More details are available in the [info](/en/about/info/) page.

Enjoy!

![Musilogue](/images/small/c/musilogue.png){.noshadow}
