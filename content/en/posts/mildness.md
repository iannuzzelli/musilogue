# Mildness

[embed:https://www.youtube.com/watch?v=7U3_nBEkQT8]

In memory of [Alberto Pizzigoni](https://it.wikipedia.org/wiki/Alberto_Pizzigoni), my guitar teacher. 

This is the introduction theme of Mildness, one of his beautiful compositions for Jazz guitar

[Trascription for guitar](/docs/mildness_pizzigoni.pdf)
