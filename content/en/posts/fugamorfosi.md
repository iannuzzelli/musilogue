# Fugamorfosi

Originally written for Cello and Clarinet, this piece was orchestrated as part of my orchestration studies at Berklee.

[Fugamorfosi](/audio/Fugamorfosi.mp3)

This work is based on the idea of orchestrating a contrapuntal dialogue using a selection of pitches which gradually changes as the piece develops. 

The selection of pitches is made of two hexads, i.e. 2 sets of 6 notes, in no particular order, using all the 12 pitches of the chromatic scale.

The main constrain is that notes from one set are always at an interval of 6 semitones (tritone) with the corresponding one in the other set.

These sets can be seen as 2 different hexatonic modes, which coexist in a selection.

The modulation from one selection to the next is made by swapping one note from one set with the complementar tritone of the other one.

More details in the [commentary](/docs/fugamorfosi_commentary.pdf)

[Full orchestral score](/docs/fugamorfosi.pdf)


![Fugamorfosi table](/images/inline/c/fugamorfosi_table.jpg){.hidden}
