# Hear the Women Out

One of the many painful knock-on effects of lockdown has been a surge in abuse of women at home. This concert offers a moment to reflect on two intertwined qualities in the lives of women generally: their bodies are subjected to social control and violence, and their creative work is dismissed, suppressed, or appropriated by men. Our lovingly-selected programme emphasises the beauty in hidden voices and stories of women, taking listeners on a journey from Afghanistan to Sicily with stops in Iraq, Iran, Turkey and Greece.

With [Rachel Beckles Willson](https://www.rachelbeckleswillson.com/) (voice, sax, oud) and Francesco Iannuzzelli (oud, setar, guitar, rubab). Streamed live by the online music platform [Living Room Live](https://www.livingroom-live.com/)

[embed:https://www.youtube.com/watch?v=e7HQjWkLlhk]

### Eya fratres personemus

A song from the Tropario di Catania, a 12th-century codex created for the Cathedral of Catania in Sicily. It celebrates Saint Agatha, who rejected the amorous advances of the Roman prefect Quintianus, and was brutally tortured and sentenced to death as a result; she died in prison. She is the patron saint of rape victims, breast cancer patients, wet nurses, and bellfounders (due to the shape of her severed breasts). The song recalls the moment in 1126 when relics of Saint Agatha’s body were returned to Sicily (having been stolen and taken to Constantinople by the Byzantine General George Maniakes in 1040). 

### Souda Shlahhani (‘How could I have been so distracted?’) and Einah Shakabrah (‘Her alluring eyes’): two songs by Massuda Al-Amarathl.

Massuda Al-Amarathl was born in southern Iraq in the early 20th-century. A black woman of slave stock, also a lesbian, she was fortunate that her tribe, Al Abu Hammed, was liberal about social relations. She dressed as a man, and was celebrated as a singer and composer. Alas, she got caught in a love triangle with two women, and one of them murdered her.

### Semai Evcara by Dilhayat Kalfa

This classical instrumental piece from Ottoman Istanbul was composed by the first female composer in the Ottoman-era canon. (Gender is not always clear from the historical record, but only one other female composer has been confirmed with any certainty.) Dilhayat Kalfa lived in the 18th century, played the long-necked lute called tanbur, and is credited with having created the ‘makam’ (mode), Evcara, in which this piece is composed. 

### Gül olsam ya sünbül olsam beni koklar mıydın (‘Would you smell me if I were a rose?’) by Neveser Kökdeş 

Neveser Kökdeş made her career as composer in the early days of the Turkish Republic, and strove to create music reflecting the newly secularised world - for which she was criticised strongly. She had a tough personal life, losing her husband while pregnant, and then being disfigured by facial paralysis. She also suffered from the loss of her brother, shortly afterwards commencing to burn her many many compositions - only 100 survive. This flirtatious song reflects on physical beauty: “If I were a rose, would you smell me, with hidden looks, hidden appeals...? Would you caress me?” 

### Morgh-e Sahar (‘Dawn Bird’), by Mortażā Naydāwud with lyrics by the poet Moḥammad-Taqi Malek-al-Šoʿarāʾ Bahār

We chose this classic and music-loved Iranian song, because it was sung by the celebrated mezzo-soprano Qamar-al-Moluk Vaziri at her formal debut at Tehran’s Grand Hotel in 1924. On that legendary occasion, she became the first woman to sing in public in Iran without wearing a veil, an act that - up until the revolution of 1979 - was of lasting influence. She is known as "the Queen of Persian music". 

### Eisai Fantis (‘You are a knave’), by Grigoris Asikis

This song is associated most strongly with the rebetiko singer Rita Abatzi, who was born in Smyrna (now Izmir) towards the end of the Ottoman era, and then violently displaced in the destruction of Smyrna and population exchange dividing Greece and Turkey in 1923. Her career as a female recording artist of rebetiko was rivalled only by Roza Eskenazi; extremely popular in the 1930s, she stopped singing after WW2. This song speaks of a woman’s betrayal by a ‘rascal’ (berbadi) and her planned revenge with a (metaphorical?) bomb (bompa). 

### Cu ti lu dissi (‘Who told you?’), by Rosa Balistreri

This most famous song by the legendary Sicilian singer addresses first love, its agonising pain, and the difficulty of leaving it. Balistreri’s financially-impoverished life was as richly colourful as her voice, She married at 16, and when her husband lost their daughter’s trousseau she attempted to murder him and handed herself in (the husband survived); a later pregnancy stemmed from an affair with the son of the noble she served, she fled the household, was imprisoned after being accused of theft, lost the baby; when working as a sacristan she was harassed by the priest… and so on. 

### Koodake Afghan Lallo

We conclude our tumultuous set with a calming lullaby from Afghanistan, which we discovered among recordings by Farida Mahwash, a singer born in Kabul in 1947. Mahwash is the only female in Afghanistan ever granted the title of ‘Ustad’ (a gendered honorific title used throughout the Muslim world for teachers and artists - not unlike the gendered terms ‘Maestro’ and ‘Master’). 

