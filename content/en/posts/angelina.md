# Angelina, by Tommy Emmanuel

[embed:https://www.youtube.com/watch?v=e2jUb6HK9J8]

I wanted to experiment with [hybrid-picking](https://en.wikipedia.org/wiki/Hybrid_picking), a technique that I have never used before. With a standard flatpick, as I really don't like thumbpicks. So I chose this beautiful piece by the great Australian guitarist Tommy Emmanuel.
