# Restoration of an antique Afghan Rubab

This is the story of an old Afghan Rubab, found in an auction house in Berlin and brought back to life by the wonderful hands of [Javier González](https://www.facebook.com/javier.gonzalez.37454961)

When I acquired this Rubab in August 2021 at the [Historia Auktionhaus](https://www.the-saleroom.com/en-gb/auction-catalogues/historia-auktionhaus-berlin/catalogue-id-srhist10035/lot-c31a5973-554b-4a3b-8211-ad6e010f0aee) in Berlin, I was intrigued by its features. Clearly an old instrument, probably 19th century, of very good luthiery, but in really bad conditions.

It seemed to have suffered from some traumatic events in its life, and passed through various restorations. These restorations seemed to have been made by expert hands, trying to preserve the aesthetic and sound qualities of the instrument. I knew which pair of expert hands I could trust with the challenge to bring this instrument back to life: Javier González, who had already restored another old Rubab for me.

The original status of the instrument is witnessed in the [photos](https://www.facebook.com/media/set/?set=a.10222909193745449&type=3) taken by Javier itself. The upper part of the fingerboard had been smashed and severely damaged. Some of the bone inlays had gone lost, the pegs needed to be fixed, the skin of course replaced, and the sound box needed attention too. Javier took the challenge with amazing patience and dedication. 

One year later, the instrument is ready, back to life with a majestic sound. The quality of Javier's work is visible in this [photo album](https://www.facebook.com/media/set/?set=a.10222909320348614&type=3). The fingerboard is perfectly restored, the pegs for the resonant strings are working very smoothly, and the modern [pegheds](http://www.pegheds.com/) for the main strings provide an effortless and precise tuning experience. Javier has even replaced their heads with the original Rubab pegs, so that it looks aesthetically the same.

![Mechanical Rubab pegs](/images/inline/c/rubab_berlin_1.jpg)

The intricate bone inlays have all been cleaned and re-glued with a special resin. The wooden sound box cleaned and restored in its original beauty. And many more detailed improvements have been made.

![Detail of the restored Rubab fingerboard](/images/inline/c/rubab_berlin_2.jpg)

Following Javier advice, I have opted for a synthetic skin, for several reasons. Firstly, the ethical principle of avoiding the use of animal skin for musical instruments. Secondly, the advantage of a skin that is more reliable in the cold humid region where I live (Northern Europe). Javier has dedicated years to perfecting the use of synthetic skin for Rubabs, and he has reached a very high standard. I would say it's pretty much impossible to distinguish it from an animal skin. It even seems to provide a much better response in terms of dynamics. 

![Synthetic skin for Rubab](/images/inline/c/rubab_berlin_3.jpg)

The melodic strings are the [Aquila Afghan Rabab Sugar Set](https://aquilacorde.com/musica-moderna/afghan-rabab/).

A part for collecting this beautiful instrument, it has been wonderful to meet Javier in person and visit his workshop near Valencia. I want to express my deepest gratitude to him for his passion and dedication. The innovative solutions that he has found for the restoration and improvement of the Rubab are really impressive. 

Finally, let's listen to it. Here is a short recording, with a simple microphone and no additional effects, of the Afghan song "Yo Palom Yaar Lavangeen"

[embed:https://www.youtube.com/watch?v=LNGmKBCHEN4]

Our common love for the Rubab and for the music culture it represents made this story happen. And it is somehow significant that its first performance has been, a few days later, at an event for Afghan refugees in Manchester. Let's remember the people of Afghanistan, and continue to support them in these difficult times.
