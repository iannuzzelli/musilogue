# Careful - Jim Hall

[embed:https://www.youtube.com/watch?v=Cx32tU7piJs]

This interesting composition by the legendary jazz guitarist Jim Hall offers a few challenges (that's why it's called "Careful").

An unusual 16-bar blues, based on the octatonic scale ("*a weird scale with a bunch of half-tones*" as Jim Hall himself said), a.k.a. half-step/whole step diminished. It's almost like an etude, both from a musical and technical perspective.

In addition, I wanted to exercise in switching between flat-picking, hybrid-picking and finger-picking, with the technique of holding the pick, when not in use, between the index and middle finger. I have relied on the excellent transcription by [Miguel Mateu Solivellas](https://payhip.com/b/48Py) of [Jim Hall's live performance in 1989](https://www.youtube.com/watch?v=5PzshdVRavk&t=0s).

And sped it up a bit, to make it even more challenging :-)
