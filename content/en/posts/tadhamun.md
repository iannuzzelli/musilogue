# 10 years of Tadhamun

[embed:https://www.youtube.com/watch?v=zZZCQJszd4M]

To mark 10 years since its founding, Iraqi women’s organisation, Tadhamun, hosted an evening of talks, poetry and music, highlighting (from Iraq, Tunisia to Palestine) experiences of Arab women "Away from Home". 

In my music performance on the Oud I have tried to remember the little-known stories of some Iraqi female singers and composers, such as Masouda Al-Amarathly and Salima Murat.
