# Fingerpicking Paolo Conte

Paolo Conte's music is an intriguing blend of sophisticated jazz harmonies and catchy melodic lines. I found the challenge of transcribing and adapting his songs to acoustic guitar fascinating and educational at the same time. 

The full playlist can be found at https://www.youtube.com/playlist?list=PLawHez4xr-AqLDZPL2cJUplq0L4ZKAe2k, while below are the individual videos.

[embed:https://www.youtube.com/watch?v=pkmvaNoLvwk]

[embed:https://www.youtube.com/watch?v=fyQz2JS18l4]

[embed:https://www.youtube.com/watch?v=lwK6sD3TSPY]

[embed:https://www.youtube.com/watch?v=vMpsI8n8EyQ]

[embed:https://www.youtube.com/watch?v=L_KFVaM2doo]

