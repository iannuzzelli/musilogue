# Festival "La saveur de l'autr" - Calais

Arab musicians and singers - and their public - can regularly meet in a vibrant space and make music in an informal context at SOAS (School of Oriental and African Studies) University in London.

In 2015 a group of musicians joined to form the SOAS Arabic Band which performed on a number of occasions in London, and also at the festival [La saveur de l'autr](http://www.psmigrants.org/site/festival-la-saveur-de-lautre-29-31-mai-2015-le-channel-calais/), organised by various organisations working with immigrants in Calais.

On that occasion the band included the singer Christelle Madani, Saied Silbak and Francesco Iannuzzelli on Oud, Remy Geoffroy on percussion.

The set mainly consisted of popular Arabic songs, as shown in the two following videos of famous Fairuz songs.

[embed:https://www.youtube.com/watch?v=aqBl8rQr3hU]

[embed:https://www.youtube.com/watch?v=Rbl67x_5Nj8]

However the most interesting part of the evening happened after the "official" concert, when Sudanese refugees from the local camps took the stage for a lively and fully improvised jam session.

[embed:https://www.youtube.com/watch?v=QqcsDR8jMKM]

[embed:https://www.youtube.com/watch?v=xUgNQQ_BVU4]

For more videos and information about the SOAS Arabic band, check out its [YouTube channel](https://www.youtube.com/channel/UCvpDn3-6KVDyethXHdVKivA) and [Facebook page](https://www.facebook.com/arabicmusicsoas/)