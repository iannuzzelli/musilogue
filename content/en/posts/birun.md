# Maftirim - Ensemble Bîrûn

Bîrûn is a series of advanced workshops on Ottoman classical music, organised by the Intercultural Institute of Comparative Music Studies 
of the Fondazione Giorgio Cini, and held every year in Venice under the direction of Kudsi Erguner.

Its fourth edition in April 2015 gathered professional and semi-professional musicians, including myself, to work on Maftirîm, the works of Sephardi Jews in Ottoman classical music. 

A CD including 14 tracks of this repertoire, recorded by the musicians during the days of the workshop, has been published by 
[Nota Edizioni](https://www.notamusic.com/prodotto/i-maftiri%CC%82m-e-le-opere-degli-ebrei-sefarditi-nella-musica-classica-ottomana/?lang=en). 
More details about the project can be found on the website of the [Fondazione Cini](http://www.cini.it/publications/maftirim-le-opere-degli-ebrei-sefarditi-nella-musica-classica-ottomana) (in Italian). 

Some music samples can be heard in the radio program [Radio 3 Suite](http://www.radio3.rai.it/dl/portaleRadio/media/ContentItem-eea5fe5b-1619-4e6a-ba2f-583f9c987152.html), broadcast on Radio Rai 3.

