# Serif Muhiddin Haydar

A historical and social analysis of music education in Baghdad in the first half of the 20th century and the key role of Serif Muhiddin Haydar in the renaissance of the Oud. 

Serif Muhiddin, son of an Serif Ali Haydar, an influential Arab personality, later took the surname of Targan, and so is known in Turkey.

Educated in both Eastern and Western music, he was a proficient Cello and Oud player and directed the Institute of Fine Arts in Baghdad between 1937 and 1948, teaching exceptional Oud soloists like Jamil Bashir, Munir Bashir and Salman Shukur. 

Haydar's influence on the development of the Oud as a solo instrument in both Iraqi and Turkish music traditions has been accompanied by a strong belief in music education, rooted in his Western music studies. 

This short essay tries to present the combination of factors, including sheer chance, that contributed to the rise of the so-called Iraqi Oud generation during his time in Baghdad. It is also available for download in PDF format.

---

## The origins of the Iraqi Oud generation

_Historical and social analysis of music education in Baghdad in the 20th century and the role of Serif Muhiddin Haydar in the renaissance of the Oud._

[Download](/docs/serif_muhiddin_haydar.pdf)

The Oud, the short-necked and fret-less lute that has accompanied Middle-Eastern music for centuries, has always been the preferred instruments for intricate discussions by scholars and theorists, providing a clear representation of music intervals in addition to legendary qualities and fantastic stories about its origin. Similarly ubiquitous but seldom prominent in music practice, a part for the very occasional virtuoso, its role has been confined to accompanying instrument for the singer, for long considered the only artist capable of delivering emotions via music. 

However, starting in the second half of the twentieth century, a generation of Iraqi solo Oud players achieved extensive success, even beyond the Arab world. Musicians like Salman Shukur and the brothers Munir and Jamil Bashir brought the instrument to international fame, inspiring successive generations of a movement which can be still considered quite alive today, represented by musicians like Naseer Shamma, Ali Al-Imam, Khaled Mohammed Ali, Ehsan Emam, Ahmed Mukhtar, to name a few.  
The renaissance of the Oud has been accompanied by the introduction of advanced techniques, the composition of new repertoire and good commercial success across very diverse audiences. Nowadays the Oud is an instrument strongly identified with the Middle-Eastern tradition and well-respected in the international concert arena, with its own “classical” repertoire.

Leaving aside the well-documented, occasionally exaggerated, life trajectories of these musicians, specifically the first generation, whose success fit remarkably well the need for a new music identity by a nation quickly assembled from the crumbles of the Ottoman empire, their extraordinary talent did seem to emerge out of nowhere.  
It is probably thanks to the various Iraqi diasporas that more details have surfaced about the origin of this phenomenon, which can be traced to the opening of the Institute of Fine Arts (Ma'had al-Funun al-Jamila) in 1936 in Baghdad, in particular with the appointment as its director of an influential musician who directly taught this initial generation of musicians.  
In trying to investigate the factors that played a role in this successful forging of new musical talent, it seems useful to review the historical events that brought to the opening of the Mahad.

Iraq had been shaped by the colonial powers after the end of the First World War and the fall of the Ottoman empire. Faisal, who played an active role in the Hijaz rebellion against the Ottomans launched by his father Hussein in 1916, was appointed King of Iraq in 1921 after a short and unsuccessful nomination in Syria. Initially Faisal I represented an external entity for a nation, Iraq, who had not existed before, but he quickly proved to have the necessary skills to manage the complex relationship with Britain and with the internal Iraqi powers.  
Faisal moved to Baghdad with his entourage of Ottoman-educated officers who quickly occupied the positions of power. These so-called “Sharifians” had the mission to create and administer a nation from the ground up. Among them, the influential thinker Sati Al-Husri played a key role in establishing the Iraq education system (Tripp 2007:92). According to Al-Husri, education and military were the two pillars for building the necessary national identity. He pushed for a rapid expansion of primary schools' infrastructure, giving priority to teaching a common language (the Baghdadi Arabic) and a nationalistic re-interpretation of Arab history. Al-Husri imported teachers from Syria and Palestine and quickly upset both minorities, such as Kurds, and the Shia majority, suspicious of Sunni indoctrination (Simon 2004:75) and in general uninterested in the non-practical subjects imposed by the government in primary schools.  
Al-Husri, Director General of Education from 1921, frequently clashed not only with the British but also with the Ministry of Education himself (a Shia token in the government), due to his secular views on education, and finally had to leave in 1927. He has often been accused (Marr 2004: 52) of planting the seeds of nationalism in Iraqi education, thus providing a fertile environment for the uptake of the ultra-nationalist Baath party in the decades that followed.  
Al-Husri was succeeded by Mohammed Fadhel al-Jamali, who had studied in the U.S. and was influenced by American progressive views on education (Simon 2004:79). Son of a Shia cleric and a teacher himself, Al-Jamali adopted some changes in favour of the rural communities, in the direction of more mass-orientated and less elitist education. His approach was to introduce Western technical subjects, considered superior, and accompany them with Arabic culture, history and language, in line with with his predecessor.  
In parallel, Iraqi society was growing with a proliferation of clubs, societies and the appearance of the social-democratic movement Alahi (Baskin 2008: 1260), which encouraged a pluralistic debate in the public sphere, able to counterbalance, at least initially, the nationalistic tendencies.

With these preconditions, the Ministry turned its attention to the arts and decided to open the first Institute of Fine Arts in 1936, with focus on music, theatre and sculpture.  
Initially conceived by Faisal (Chabrier, in Warkov 1987:37) it was implemented by King Ghazi himself, who succeeded to his father in 1933.  
After the opening of the Ottoman Dârülelhan (Conservatory) in Istanbul in 1917, followed by the Institute of Arabic Music in Cairo in 1929, it had become important for an Arab nation to have its own Conservatory, at least in order to provide a military band for official ceremonies. Thus, when the Institute was opened in 1936 the first appointed teacher was Botros Hanna for brass.  
The opening of the Mahad was in line with the growth of a national identity through arts. It required skilled teachers and a competent director: for this role, the choice fell on Serif Muhiddin Haydar [^1], descendant of the prophet and therefore distant relative of the King himself.


Muhiddin was the second son of Serif Ali Haydar. His father, an influential Arab personality, had to spend most of his life “hostage” of the Sultan Abdul Hamid II in Istanbul while waiting to be appointed Emir of Mecca, a right he saw denied a number of times, due to the political manoeuvres of the Ottoman ruler. Despite all this, Ali Haydar stayed loyal to the Ottoman empire during the turbulent times of its fall.  
Ali Haydar crossed his path with Faisal's father Hussein during the dramatic circumstances of the Hijaz rebellion of 1916. In response to Hussein rebellion, the Ottoman Sultan finally nominated Ali Haydar Emir of Mecca, but he never managed to reach the holy town and, after various humiliating circumstances (Stitt 1948), saw his title removed in order to please the ambitions of Hussein and his sons, in the aftermath of the First World War.  
Ali Haydar hopes for Syria or Iraq were frustrated by the appointment of Faisal himself, while Hussein retained Mecca. Probably too honest or meek to play the political games of the time, Ali Haydar died in 1935 in poverty in Lebanon, after the last humiliation of not being allowed entry to Hijaz for his pilgrimage.  

Given these circumstances, it seems obvious that Ali Haydar's family looked at Faisal's with deep resentment, but apparently the relationship between the two families improved after Hussein's death in 1931, while in exile in Cyprus (Stitt 1948: 336).

A well educated man, with degrees in Law and Literature, Serif Muhiddin had shown his remarkable skills in music since a very early age and had private tuition in Cello and Oud.  
He had the opportunity to meet the best teachers and artists in Istanbul, but his father's circumstances often forced him to follow him in his journeys and interrupt his music studies for long periods.  
In 1924 he announced to his father the intention to go to the U.S. to pursue his music interests (Stitt 1948: 275). Ali Haydar let him go, and Muhiddin stayed in America 8 years, studying with high-profile musicians and performing in acclaimed concerts. A delicate operation to the thyroid forced him to interrupt his activities and ultimately to return to Istanbul in 1932, where he continued his music performances to a lesser degree.

His cross-cultural music skills, his exposure to Western music education and performing arts, his connections with Western musicians and his understanding of state affairs were an almost unique combination, which made him the perfect candidate for directing the Institute of Fine Arts in Baghdad.

Nevertheless, the call from King Ghazi must have stirred conflicting emotions in Muhiddin Haydar.  
We can only speculate on his motivations in accepting the post, and on the aspects he considered.  
His father's life events, which brought so many difficulties to the whole family, had a deep impact on his music studies as well. The same elements that created so much tension were now opening an opportunity. The relationship with the Arab world, clearly a mix of longing and concern in his father's diaries (Stitt 1948): a view possibly shared by Muhiddin. Finally, his health condition, which seemed to restrain his performing carrier and to demand a re-evaluation of his relationship with music.  
We can only infer from the passion he devoted in organising and teaching at the Mahad what his answer was.

A very interesting exchange of letters between Haydar and officials at the Iraqi Ministry of Education, at that time directed by Jamali, sheds some light on the ideas of both parties (Abbas 1993: 11-13), and in particular on the vision of Haydar regarding the objectives of the Mahad.

The government was adamant in making attendance to the Institute of Fine Arts free.  
Still a number of organisational issues needed to be solved, as nobody had experience in running such an organisation. It seems that this was actually the first case of a music Institute in the Middle-East whose activities were part of the national curriculum and directly managed by the Ministry of Education. Both Istanbul and Cairo had different characteristics, as they were dedicated to professional musicians (Mukhtar 2014).  
Questioned on criteria for accepting students, Haydar requested primary education. Attendance to primary schools had grown dramatically during Al-Husri years, and this constrain by Haydar seems mostly to ensure that music is not seen as a replacement for education, but as a form of vocational study to be conducted in addition and in parallel to other studies. An aspect that is confirmed by the classes schedule, as detailed by Haydar in his answer to the Ministry. He foresees two slots of 4 hours per day, in the afternoons (12 to 4pm and 4pm to 8pm), with specific days dedicated to specific instruments. Such schedule of classes could guarantee attendance by students of secondary schools and of other colleges, and the limited attendance (two days a week) made it sustainable together with other studies.  
Haydar does not explain how he came up with these criteria. Despite being exposed to private music education only, his advanced studies in Istanbul, the cultural environment he grew up in and his residency in the U.S., where he came in contact with its developed music education system, must have all played a role in shaping his ideas.

Another question raised by the Ministry's officer is quite revealing of the intended objectives of the Mahad, according to the government: “how can national music be revitalized?” (Abbas 1993:12).  
In line with Al-Husri ideas, arts were instrumental in building the national identity, so some national music needed to be built.  
Haydar's answer is straightforward: it will take a long time, as teachers are missing and they will need to be imported from abroad. Budget was limited, so Haydar seems to bounce back the issue and simply ask for time. The polite way in which he dismisses local Iraqi musicians (“they have already given what they could give” - Abbas 1993:12) is actually quite revealing of the low consideration he had for the local music tradition (Kojaman 2014).  
According to Haydar, the only way to revitalize national music is via “teaching world music” (where “world” refers to the West) with its theories and instruments, according to the artistic foundations of Western countries. His vision consists of a parallel system of education of both Western and national music, with the objective of creating talented artists, strongly rooted in the Western classical tradition and capable of adding national elements. He even names his models: Chopin in Poland, Grieg in Norway, Rimsky-Korsakov and Borodin in Russia.

Pressed about the possibility to provide musicians for radio broadcasting (which had been launched the previous year, in 1935), Haydar again asks for patience, as it will take time to form the musicians for this activity. “If it is really needed, and there is sufficient budget, a choir can be brought from Istanbul”. Haydar knows his ways with the officers of the Ministry of Education, fully aware of their financial limitations and of the directions they have to follow, and thus manages to preserve a significant degree of freedom in running the Mahad.


Music lessons began for the following instruments: Brass, Violin, Piano, Cello and the Oud, the only Eastern instrument, taught by Haydar himself. He and Boutros Hanna were the only non-Western teachers (Kojaman 2001: 62).

The school attracted students from all sort of backgrounds. In that time instrumental music was still perceived as controversial by some parts of the population, specifically Muslim. Some students, such as Salman Shukur, whose family was Fayli Shia, went to the Institute against the will of their parents (Kojaman 2014). For other religious communities, such as the Jews, music was a family profession, passed for generations from father to children. Females could also attend the Institute, for example the famous Iraqi poet Nazik Al-Malaika was a student of Sherif Muhiddin Haydar (Mukhtar 2014).  
Almost everybody who applied and had the primary education pre-requisite was accepted (Kojaman 2014). The Second World War and the various political troubles of Iraq, such as the coup d'etat of Rashid Ali in 1941, did not stop the activities of the Institute. Its access remained free, under the patronage of the government which increased its investments in education.

A new, educated middle-class began to emerge, in particular in the urban context of Baghdad. Contrary to the semi-closed class system of Ottoman times, now Iraqi could “move fast in the path of modern civilization” (Wardi 108). Education started to be accepted as essential by most social groups, eager to acquire new concepts and values, and to show confidence in their display, where arts could play a significant role. According to the sociologist Ali Wardi (105-12), this process was in part artificial and definitely too fast, creating issues of double character behaviour, as part of the population struggled to address the contrast between the new urban values and the old traditions they came from.

Baghdad attracted people from all the country, and so did the Mahad. Jamil Bashir, a talented young musician from Mosul, in Northern Iraq, convinced his father, himself a singer and modest Oud player, to send him to study in Baghdad. He went under direct tuition of Muhiddin Haydar and completed the full 6 years program of the Oud. He also graduated in Violin and became a teacher at the Institute. He was followed by his younger brother Munir, who studied with Haydar and with his brother Jamil.
Haydar's teaching methods for the Oud were quite revolutionary, as it emerged later from his writings published after his death (Targan 1995).
Extremely focused on technical skills, he pushed the Oud as a solo instrument, introducing a number of techniques: the use of all four fingers of the left hand, alternation of the strokes with the pick (risha) in order to achieve greater speed, the use of high positions on the fretboard, exploring the higher range of the instrument and its different timbres (Cevher 1993: 17-21).
He gave extreme importance to dynamics and to fluidity in fast passages (Chabrier 1978b: 135).
Haydar also imported various techniques from strings, such as double-stops, _glissando_, and _staccato_. 
A composer himself, Haydar wrote a number of technical studies which were carefully designed to highlight these new techniques.

From an educational perspective, a fundamental change which transpires from his notes was the introduction of sequential patterns and an overall progressive approach in the study of the instrument. Traditional tuition required students to sit with the teacher and somehow absorb the knowledge by example. Having a longer time range, as the Oud course lasted 6 years, Haydar designed his lessons accordingly, probably inspired by the pluriennial music education programs in Western conservatories.

His drive for advanced techniques needed to be accompanied with improvements in instrument construction. Haydar had several Ouds (Cevher 1993:13), and favoured those by the greek luthier Manol (Emmanuel Venios) – who had lived in Istanbul when Haydar was young - and the Nahat Damascene family. Haydar is occasionally pictured with an Oud by Ali Ajmi (Ali Khanbaba), an Oud maker of Iranian origin who lived in Baghdad in that period and that worked closely with Haydar in order to develop his instruments. The Ouds by Ali Ajmi have some interesting characteristics which differentiate them from others of the same period in Iraq. A part for the brilliant tone and the small size (corresponding to Ottoman style – Ali Ajmi had studied Oud-making in Istanbul), the fretboard extended over the soundboard without discontinuity, allowing to play in very high positions, even further the junction between the neck and the body.
This modification could have been requested only by a player capable of exploring that region of the fretboard, and Haydar had the opportunity to discuss and refine these improvements together with Ali Ajmi. Mohammed Fadel, another famous Oud maker, opened his first workshop in Baghdad in 1932 and later brought some radical changes to the instrument, in collaboration with Munir Bashir, such as the movable bridge in place of the fixed one.

![Serif Muhiddin Haydar playing his Ali Ajmi Oud. The fretboard extends over the soundboard. A small ornamentation marks the junction with the body, without creating any discontinuity in the playing surface. The Oud player is therefore able to play in very high positions and obtain a clean sound (Cover of CD “Şerif Muhiddin Targa” - Kaf Muzik - August 2001 - 8697408900159)](/images/inline/c/serif_cd.jpg)

Some of Haydar's students followed his technical approach very closely, notably Salman Shukur, who directed the Institute himself and became a prominent figure in representing Iraqi music abroad. 

Another branch of students, headed by Jamil Bashir, looked at the Iraqi tradition as a source of music to be adapted for the Oud. Not only the vast repertoire of folk songs, belonging to the different music cultures of Iraq, but also the respectful repertoire of Iraqi Maqam, long compositions passed for generations and usually sung to classical poetry.  
Jamil Bashir was also a fine composer, able to merge advanced techniques into musical moments. A similar path was followed by his ten-years younger brother, Munir Bashir, who achieved international fame after a tour in Europe in 1971 (Chabrier 1978a: 101).  
Munir Bashir first disc (Ocora OCR 63) was published in 1971 and included a selection of improvisations strongly rooted in the repertoire of Iraqi Maqam. Many other discs followed. A similar selection was present in the unique disc published by Jamil Bashir (Pathe C066-95160) in 1974.

Both brothers became masters in the adaptation of Iraqi music and in the art of improvisation, creating a unique style made of virtuosity, sparse fragments and exotic sonorities, with occasional references to known modal melodies. This style proved to be successful not only in Iraqi, but also in the rest of the Arab world as well as in the West.  
Jamil Bashir wrote a 6-years method for Oud (Bashir 1961) based on his direct experience as a teacher at the Mahad. Accurate and gradual exercises prepare the students to perform a challenging soloist repertoire drawn from all Middle-Eastern traditions, in addition to specific compositions by Jamil Bashir and his contemporaries.
Another relevant figure is Ghanam Hadad, student of Haydar himself, whose teaching method gives more attention to the Maqam music system while keeping all the characteristics of his teacher regarding technique and gradual development (Mukhtar 2014).

A few traumatic events for the Iraqi music scene happened at the end of the forties: Serif Muhiddin Haydar health worsened and in 1948 he had to resign from his post and move back to Istanbul.  
He left the Mahad in the safe hands of Boutros Hanna.  
Furthermore, the position of the Jews in Iraqi society became more critical, and after a number of violent episodes, accompanied by political decisions, most Jews migrated to Israel in 1951.  
A regrettable loss for the Iraqi society at various levels, music included.  
For decades Jewish musicians had performed in Chalghi ensembles in Baghdad, accompanying the Iraqi Maqam singers. Their disappearance could represent a fatal blow not only for the Iraqi music tradition, but also for the various national activities which relied on Jewish music professionals, such as the Radio orchestra.
This delicate transition was managed with the direct involvement of the prime minister Nuri Said, who made sure that two selected Arab musicians (Shaoubi Ibrahim on the Joza and Hashim al-Rejab on the Santur) were passed the whole Iraqi Maqam repertoire by selected Jewish musicians (Kojaman 2014). Both Ibrahim and Al-Rejab later wrote books on Iraqi Maqam and taught them in public institutions.  
This significant episode marked a key change in music education in Iraq, although due to regrettable circumstances, as the last family-based music professionals were replaced by a wider organised educational offering, capable of satisfying the growing demands of the music industry.  
An industry that in the years to come was able to move from Radio to TV - launched in 1957 - and commercialise national music and various Western adaptations to a wide audience, both in Iraq and abroad.

It is difficult to say whether Serif Muhiddin Haydar had envisaged all these developments.  
He had anticipated that national music would have benefited from musicians educated to Western standards, although he did not hide his preferences for some of his pupils, like Salman Sukur, who kept a pure soloist approach, distant from local traditions and strongly focused on technical virtuosity. Some of Haydar's more successful students are probably indebted to his determination in introducing Western techniques for their success as musicians out of Iraq.  
More in general, his vision of accessible and affordable music education proved fruitful, together with his idea of a mixed music identity, strong of both Western techniques and national roots.

Surely the institutional objective of “revitalizing national music” was reached when the Oud of Munir Bashir was acclaimed in other Arab countries and in Europe. Whether the Oud was part of the initial vision, it's questionable. In any case, government officials were quick to understand the situation and nominate some of these musicians (Munir Bashir and Salman Sukur) ambassadors of Iraqi music, giving them various high-profile positions in the arts sector. 

The role of state patronage had been key in making music education accessible and in replacing the old individual teaching approaches. Music education scaled up to the required needs, as urban population grew in the fifties, accompanied by a general higher government spending in education.  
Nationalism was present through-out this period: initially genuinely directed at creating a collective identity for the newly formed nation, later flirted shortly with pan-arabism before transforming into the ultra-nationalism of the Baath party.  
Education was controlled by the ruling elites, willing to rewrite history books and control other subjects. In the beginning, music escaped this controlling process for various reasons: the novelty of its introduction, its marginal reputation, and probably also the merit of an individual (Muhiddin Haydar) capable of defending his own independence, thus creating a fertile environment for a cross-cultural exchange between East and West.  
This situation changed after the fifties, as the ruling power paid more attention to how the arts could serve better its political aims.

The influence of Western music on the Arab music culture during the twentieth century is a complex subject which cannot be addressed in this short review. “Fusion” efforts between the two traditions have often produced questionable results, and Muhiddin Haydar's compositions may fall among these. At times baroque, often predictable, his music does not really stand out as Oriental. Something more interesting happened with some of his followers, who managed to maintain their musical identity with a sapient mix of Western and Eastern techniques and forms.  
After the revolution of 1968, Munir Bashir got increasingly involved in the political activities of the Baathist Ministry of Education. He made strong statements (Bashir 1978) in support of national music, claiming that political aspects had as much importance as artistic ones in the strategic development of Iraqi musical planning. Arguments well-rooted in the narrative of the Baath party.


A part for Western influence, the local cultural diversity had a fundamental influence considering that, at least until 1950, Baghdad music scene benefited from an influx of students and musicians willing to collaborate with each other in a number of contexts, from night clubs to cafe's, from Iraqi Maqam to Egyptian-style songs, from traditional ensembles to the newly started Radio orchestra. Examples of collaborations are ubiquitous and it is practically impossible to trace who influenced whom. 

The international recognition of Oud players like Munir Bashir can be considered one of the first answers by Arabs to the often felt, and quite unjustified, complex of inferiority towards Western music. Instrumental music, able to cross language barriers, acquired a new legitimacy in front of Muslim eyes in the Arab world. The echoes of pride and spirituality of Munir Bashir's improvisations managed to attract very diverse audiences: Westerners looking for exotic experience and Arabs longing for a modern reflection of their ancient dominance in the arts.


The emergence of the soloist is also another phenomenon that needs to be considered. Traditionally, the Artist was the protégé of the Sultan, performing in his Court, distant from people eyes. Courts were replaced with theatres, and Sultans with the new effendia, the educated middle-class looking for self-recognition in a shaky social context, still full of contradictions. A possible parallel can be drawn with the emergence of classical music soloists in Western music.

In both cases, music had to be transformed in order to impress the audience, with the necessary immediacy, virtuosity, unpredictability, merged with wisely hidden references to a common shared cultural identity. Elements that contrast with the fragility of the social context, influenced by centuries of abandonment and shocked by the fast approach of modernity. 

One flawless aspect is the educational excellence that must be recognised to all those involved in the Mahad. It was the first attempt at collective music education in the region, and it worked remarkably well. Serif Muhiddin Haydar not only had a vision and the skills, but also the ability to dose the ingredients of discipline and passion in his followers. The teachers and students that worked with him proved how music can be documented, transmitted and forged, making the most of a very rich and diverse cultural context.

All these aspects have contributed to the birth of the modern Iraqi Oud generation. Still, they may not be enough to explain its international success and still alive heritage. In addition to other factors that may have been missed in this limited research, it must be said that chance played a great role, as usually happens in matters related to music, but also found a very fertile environment to unfold its unpredictable manoeuvres.



[^1]: Sherif Muhiddin Haydar took the family name Targan after the Turkish Surname Law of 1934, therefore in Turkey he is known as Sherif Muhiddin Targan. In this essay the original Haydar surname is used.

[^2]: Scale is the distance from the bridge to the nut, that is the vibrating length of the strings. Modern Ouds are commonly distinguished between Arabic (scale of 60cm or more) and Turkish (less than 60, usually 57-58cm).


### References

- Abbas, Habib Zahir. 1994. _Al-Sherif Muhi Al-Din Haidar wa talamdhatihi: dirasah, mudawwanat, tahlil._ Baghdad: Dar al-Hurriyah lil tiba'ah.
- Bashir, Jamil. 1961. _Al-'ud wa tariqat radisihi._ Baghdad.
- Bashir, Mounir. 1978. _"Musical Planning in a Developing Country: Iraq and the Preservation of Musical Identity"_ World of Music 20(1): 74-7.
- Baskin, Orit. 2008. _The Other Iraq: Pluralism and Culture in Hashemite Iraq._ Stanford, Calif.: Stanford University Press.
(Kindle edition, hence text locations are referenced instead of pages)
- Cevher, Muharrem H. 1993. _Şerif Muhiddin Targan: hayatı, besteciliği, eserleri._ İzmir: Ege Üniversitesi Basımevi.
- Chabrier, Jean-Claude. 1978a. _“New Developments in Arabian Instrumental Music”_ World of Music 20(1): 94-105.
- Chabrier, Jean-Claude. 1978b. _“Un réformateur du 'Ûd: S̡erif Muhiddin” in Quand le crible était dans la paille..._ Hommage à Pertev Naili Boratav, eds. Nicolas & Dor, 133-50. Paris: Maisonneuve et Larose.
- Kojaman, Yeheskel. 2001. _The Maqam Music Tradition of Iraq._ London: Y. Kojaman.
- Marr, Phebe. 2004. _The Modern History of Iraq._ 2nd edition. Boulder, Colorado: Westview Press.
- Simon, Reeva S. 2004. _Iraq Between the Two World Wars: The Militarist Origins of Tyranny._ New York: Columbia University Press.
- Stitt, George. 1948. _A Prince of Arabia: the Emir Shereef Ali Haider._ London: Allen & Unwin.
- Targan, Şerif Muhiddin. 1995. _Ud Metodu._ Istanbul: Çağlar mûsiki Yayınları.
- Tripp, Charles. 2007. _A History of Iraq._ Cambridge: Cambridge University Press.
- Wardi, Ali. 2008. _Understanding Iraq: society, culture, and personality._ Lewiston, NY: Edwin Mellen Press.
- Warkov, Esther. 1987. _The Urban Arabic Repertoire of Jewish Professional Musicians in Iraq and Israel: Instrumental Improvisation and Culture Change._ The Hebrew University of Jerusalem: PhD thesis.

### List of people consulted

- Kojaman, Yeheskel  
Interviewed on 14 February 2014.  
Jewish Iraqi musicologist, author of “The Maqam Music tradition of Iraq”, published in 2001.
- Mukhtar, Ahmed  
Interviewed on 10 April 2014.  
Ahmed Mukhtar has studied Oud with Ghanam Haddad at the Institute of Fine Arts in Baghdad. He has published 4 CDs and performs regularly throughout Europe and the Middle East. Mukhtar regularly presents his own educational music program on Al Fayhaa Satellite TV. 

