# History of Persian Music

[embed:https://www.youtube.com/watch?v=GI79GQmMZNA]

My little contribution on the importance of the Oud for this documentary on the history of the music of Iran, written and produced by [Karen Sabaghi](https://www.facebook.com/KAR.SA2), and broadcast on ManotoTV on 24th August 2022.
