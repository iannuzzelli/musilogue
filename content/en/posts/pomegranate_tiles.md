# Pomegranate Tiles

"Pomegranate Tiles" is the first CD by [Dafarahn](/en/archive/dafarahn/), i.e. Francesco Iannuzzelli on Oud and Lucile Belliveau on Bass, in this occasion accompanied by Elizabeth Nott on percussion. 

The CD, recorded at SOAS Studio in July 2014, features a selection of Iraqi Maqam and traditional repertoire from the Middle-East. 

Sample track: [Iraqi Maqam Khanabat](/audio/Iraqi%20Maqam%20Khanabat.mp3)

More information, including CD liner notes, on [Dafarahn website](https://dafarahn.musilogue.com/#music)
