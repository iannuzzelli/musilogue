## Samai Hijaz

This Samai was originally composed for solo Oud and later adapted for a full Western orchestra. In this process, it underwent several structural and rhythmic changes, which are described in more detail in the [commentary](/docs/samai_hijaz_commentary.pdf).

It represents my first attempt at orchestration of Middle-Eastern music, and a digital mock-up recorded with EWQL library can be listened here [Samai Hijaz](/audio/Samai%20Hijaz.mp3)

The full [orchestral score](/docs/samai_hijaz.pdf) is also available for download, while parts are available on request.

![International Music Composition Competition Award](/images/small/c/award.jpg){.img-left .noshadow}
In 2015 this Samai has won the [International Music Composition Competition by the Arab Academy of Music](https://www.arabmusicacademy.org/article/9), and it has been performed during the award ceremony at USEK University in Kaslik (Lebanon) on 25 July 2016.
