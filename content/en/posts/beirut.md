# Beating for Beirut

[embed:https://www.youtube.com/watch?v=OhWeEeexhOw]

More than 15 artists and musicians around the world have been playing beats for Beirut and raising funds for [First Aid Renovation](https://www.facebook.com/FirstAidRenovation/), an initiative created by activist and academic Sally Hammoud, alongside her students and colleagues, to help families affected by Beirut explosion make their homes safe again. 

Funds have been raised via a [virtual event](https://www.live-actions.ardkon.com/BeatingForBeirut) and here is our musical contribution, playing a couple of compositions by Marcel Khalife.

With  [Rachel Beckles Willson](https://www.rachelbeckleswillson.com/) (udu) e Francesco Iannuzzelli (oud).
