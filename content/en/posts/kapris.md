# Kapris by Jamil Bashir - Study guide

This guide is part of a collection of study guides for Oud that I have written for myself and for lessons with my students. 

"Kapris" is a composition by Jamil Bashir, published in the first edition of his Oud method (1961) at page 123, as part of the pieces for the sixth and last year of study. 

Here you can download my study guide for this short but iconic piece, which showcases many aspects of the so-called Iraqi style.

For corrections or questions, please [get in touch](/en/about/contact/)

[Kapris by Jamil Bashir - Study guide](/docs/jamil_bashir_kapris.pdf)


