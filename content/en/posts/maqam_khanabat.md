# Iraqi Maqam Khanabat

[embed:https://www.youtube.com/watch?v=rlJ2Upz_RAs]

In June 2014 Taqasim Music School organised a concert at [St. Ethelburga's Centre](https://stethelburgas.org/), 
a beautiful venue in Central London, featuring teachers of the school and guest musicians.

The concert, titled *"Discovering the Iraqi Maqam on the Oud"*, presented traditional repertoire from Iraq. In this video, Francesco Iannuzzelli (oud), Lucile Bellivau (double-bass), Julia Ana Katarina (cello) and Elizabeth Nott (percussion) perform Iraqi Maqam Khanabat. 
