# Sarajevske Ruze

A cultural and geographical crossroad between East and West, Sarajevo is famous for its religious and cultural diversity. Currently the capital of Bosnia and Herzegovina, was besieged for almost 4 years during the Bosnian War in 1992-96.
Because of the intense urban warfare, its roads were heavily damaged by thousands of mortar shell's explosions, which resulted in many deaths. The scars left in the asphalt were filled with red resin and represent a unique feature of the city, known as Sarajevo Roses ("Sarajevske Ruze").

The four movements of this musical composition, in the form of a suite, explore different phases related to Sarajevo's Siege and to what its roses represent.
From the worried waiting for how the events will unfold, to the hurried run under the danger of the bombs; from the mourning for the loss of human lives to the powerful will to rebuild a new life.

Originally written for Oud, Ney, Viola, Kemenche and Double Bass for the *"Composition"* module at SOAS University.

A digital mock-up can be heard here [Sarajevske Ruze](/audio/Sarajevske%20Ruze.mp3)

[Sarajevske Ruze - Full score](/docs/sarajevske_ruze.pdf)

![Sarajevo Ramazan Festival](/images/inline/c/sarajevo_library.jpg){.hidden}