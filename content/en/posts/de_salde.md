# De Sålde Sina Hemman (They sold their homestead)

[embed:https://www.youtube.com/watch?v=25ktZCh0vfo]

This Swedish folk song belongs to the genre of emigrant songs associated with the extensive emigration from Sweden to North America in the second half of the 19th century.

Its jazz rendition by the Art Farmer Quartet, with Jim Hall, was published in the album "To Sweden With Love" in 1964.

Here is my guitar adaptation based on the chord melody by Erik van Heijzen, my jazz guitar teacher at SKVR Rotterdam. I transcribed part of the wondeful Art Farmer solo and played it with a walking bass line.
