# Samai Rast - Jamil Bashir

[embed:https://www.youtube.com/watch?v=UwjlOuialIQ]

Quick take of the beautiful Samai Rast by Jamil Bashir, Iraq Oud virtuoso who passed away 40 years ago. Oud by [Mohammed Reza](https://www.facebook.com/mohammed.jabar.16).
