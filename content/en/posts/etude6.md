# Etude 6 - Julian Lage

[embed:https://www.youtube.com/watch?v=pTJOwqmEU80]

Julian Lage has been redefining how to play modern guitar. His style, rhapsodic, contrapuntal, expressive, unconventional and at the same time so much rooted in the classical and jazz repertoires, is accompanied with extraordinary technical fluency.

This etude encompasses all these characteristics, and it indeed required a lot of work and study, having also to find alternative solutions where my fingers couldn't reproduce his approach.
