# Shallalat - Jamil  Bashir

[embed:https://www.youtube.com/watch?v=Ca4z6vO3CcQ]

Remembering Jamil Bashir on the anniversary of his death (24/9/1977) with an arrangement for 7-course Oud of his composition [Shallalat](https://www.youtube.com/watch?v=TJWlWvRzFB8).

Scores:
- [approximate transcription of the original theme](/docs/shallalat.pdf)
- [arrangement for 7-course Oud](/docs/shallalat7.pdf)

Oud by [Amjed Saeed](https://www.facebook.com/profile.php?id=100012533195529)
