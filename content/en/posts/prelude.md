# Bach on Rubab

[embed:https://www.youtube.com/watch?v=HiRsP0Hi-bo]

I have often used Bach classical compositions for indivual instruments (violin, cello, lute) as an exercise. The range, the modulations, the musicality of Bach's works provide great insipiration and at the same extraordinary technical challenges.

So I thought of doing the same on the Afghan Rubab, facing quite a few challenges and having to slightly adapt the composition to what's possible in terms of range and fingering.

The ultimate result, which started as a simple exercise, ended up as a decent video that has been welcomed by my teacher Ustad Homayoun Sakhi and published on the [Rubab Academy Youtube channel](https://www.youtube.com/@RubabAcademyOfficial/videos).

If you are interested, here is the [score and tablature](/docs/bach_prelude.pdf).
