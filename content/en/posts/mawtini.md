# Mawtini (My Homeland)

[embed:https://www.youtube.com/watch?v=dc9_qIawh9I]

Widely sung in Palestine, throughout the Palestinian diaspora and across the Arab world, Mawtini has been the unofficial anthem of Palestine and currently is the national anthem of Iraq.

Music by the Lebanese composer Mohammed Flayfel, poem by the Palestinian poet Ibrahim Tuqan.

[Guitar arrangement (score & tab)](/docs/mawtini.pdf)
