# Blue Feather

A short composition, written while testing some strings digital libraries and Oud-Bass combinations 

[Blue Feather](/audio/Blue%20Feather.mp3)

![Fawzi Monshed sound-hole](/images/inline/c/monshed_hole.jpg){.hidden}
