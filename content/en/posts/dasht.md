# The modal repertoire of Maqam Dasht

In this short essay I have chosen one music mode and followed its development in neighbouring music traditions of the Middle-East. 

Known with similar names in modern nations (Dastgah Dashti in Iran, Maqam Dasht in Iraq and Mugham Dasht in Azerbaijan), the Dasht mode has specific melodic and structural characteristics, presented through the analysis of vocal and instrumental pieces from the region.

The music tracks referenced in the text can be found on a dedicated playlist on SoundCloud.

[The modal repertoire of Maqam Dasht](/docs/dasht.pdf)

