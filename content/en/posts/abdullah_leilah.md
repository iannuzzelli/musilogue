# Abdullah and Leilah

Very glad to play Oud for the soundtrack of [Abdullah and Leilah](https://www.imdb.com/title/tt5991458/), a beautiful and touching project by the film director Ashtar Al Khirsan. Music composed by [Tara Creme](http://taracreme.com/portfolio/abdullah/)
