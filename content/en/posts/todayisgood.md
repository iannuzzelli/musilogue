# Today is Good!

In 2017 I have moved to Sicily, together with [Rachel](https://rachelbeckleswillson.com), in order to collaborate, as volunteers, with various educational projects for migrants, in particular unaccompanied minors. Among the various activities, we have been running regular music workshops in refugee camps, which soon turned into a joyful creative kaos, which led to several original songs now published in a CD titled "Today is Good!".

It includes 11 songs, whose music and lyrics have been written by asylum-seekers, guests of the camps, recorded and produced with the limited means at our disposal. 

More information is available on the website [Today is Good](https://www.todayisgood.org), where you can also listen to the songs and buy them. All profits go to the young African singers involved in this project.
