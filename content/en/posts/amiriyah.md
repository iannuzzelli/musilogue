# Happened at Al-Amiriyah

[embed:https://www.youtube.com/watch?v=91ZrY04yKiE]

On 13 February 1991 the [public air-raid shelter in the Amiriyah neighbourhood](https://en.wikipedia.org/wiki/Amiriyah_shelter_bombing) of Baghdad was hit by two GBU-27 bombs dropped by U.S. bombers.

The civil-defense shelter was used by hundreds of civilians, mainly women and children, during the frequent bombings of Baghdad. More than 400 people were killed, although numbers are probably much higher, as the registration book was incinerated in the blast. The U.S. military command was aware that it was a civilian facility, and upgraded it to military target without clear evidence.

The Iraqi Oud virtuoso [Naseer Shamma](https://www.shammamusic.com/) composed this piece, titled "Happened at Al-Amiriyah", to remind us, in music, about what happened on that day. When I discovered his recording, in a CD published by the [Institut Du Monde Arabe](https://www.discogs.com/release/2299199-Naseer-Shamma-Le-Luth-De-Bagdad-The-Baghdad-Lute), I was shocked by its emotional impact. I could not imagine an Oud could be so powerful.

On its anniversary, and at a time when we are still witnessing massacres of civilians in the Middle-East, I try to express similar feelings of sorrow and disdain against the brutal violence that continues to kill innocents, by going through this piece with the Oud. I don't have the spritual strength to deliver the powerful positive energy with which Shamma usually concludes this piece; I believe I am not even entitled to do that, and I prefer to finish this rendition with a softer part, just a flimsy sign of hope and encouragement.

I dedicate my humble rendition to the people of Iraq, and to the people of Palestine.

N.B. The 7 strings Oud I play with has been tuned to E, as I din't have the right string set for the typical Iraqi tuning in F.
