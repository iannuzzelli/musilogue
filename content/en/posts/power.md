# Power

The concept of the piece is to subvert the hierarchy of power in a performance context, giving page-turners the possiblity to act as performers for a limited time.

The role of the conductor (here called *timekeeper*) is to supervise the performance, that is the temporary exercise of power by the page-turners, and giving it a brutal end at a set time.

Instrumentalists are simply playing what is shown in front of them and do not have any performing role.

Music can be written for any set of instruments, composers can experiment with the unpredictable overlap of the parts.

[Instructions](/docs/power_instructions.pdf) are provided for the ensemble and should not be disclosed to the audience.

In this example, [score parts](/docs/power_parts.pdf) have been prepared for piano and guitar.

This piece was originally written for the *"Compose and Perform"* module at Goldsmiths College.

![Music stand](/images/inline/c/music_stand.jpg){.hidden}
