# garden of <br>remembrance

Garden of Remembrance is a concept-disc, recorded at home with the resources of that time, i.e. a sampler (EMU 3000), its thin digital libraries and one amplified classical guitar.

The music follows a 4-days timeline of events in London, and it is available on a dedicated SoundCloud playlist

<iframe id="sc-playlist" scrolling="no" frameborder="no" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/playlists/268569111&amp;color=ff5500&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false"></iframe>

![Garden of Remembrance - Cover](/images/inline/c/garden.jpg){.hidden}
