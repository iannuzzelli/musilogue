# commenti <br>musicali

In the 90s Fonit Cetra, the record-company part of the Italian state television RAI, published several CDs with original music to be used as background for TV programs.

A couple of these CDs featured compositions of my guitar teacher Alberto Pizzigoni, my composition teacher Aldo Rossi and a few of mine.

The first CD included Jazz tracks and the second one music in Baroque style. From the latter, here is a short dance [Loure](/audio/Loure.mp3)

![Commenti Musicali - Cover](/images/small/c/commenti_musicali.jpg){.noshadow}
