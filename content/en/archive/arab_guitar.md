# arab guitar

![Guitar](/images/inline/c/guitar.jpg){.shadow .img-left} 
The idea of using the guitar for Middle-Eastern repertoire is not new. Not only there are classical works inspired by Oriental music (such as the famous "Capricho Arabe" by Francisco Tarrega for classical guitar), but many guitar players have adapted Arabic modes and melodies to the guitar, often mixing them with Flamenco techniques and matching the parallel Arabic nostalgia for Andalusia.

I was more interested in finger-picking than flamenco techniques, and I have been looking for some time at how melodies from the Middle-East could be ported on the guitar. In the end I have abandoned this project, finding the presence of the frets too limiting for the microtonal characteristics of Arabic music, but for the record here are a few attemps.

[Chi Mali Wali](/audio/Chi%20Mali%20Wali.mp3) (Iraqi folk song)

[Chi Mali Wali - Guitar score](/docs/chi_mali_wali_guitar.pdf)


[Longa Hijaz Kar Kurd](/audio/Longa%20Hijaz%20Kar%20Kurd.mp3) (by Kemani Sebuh Efendi)

[Longa Hijaz Kar Kurd - Guitar score & tab](/docs/longa_hijaz_kar_kurd_guitar.pdf)
