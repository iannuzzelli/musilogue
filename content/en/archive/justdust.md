# just dust

![Just Dust al Baritono, CPM, Milano, 1992](/images/inline/c/jd_baritono.jpg){.shadow}

Just Dust were Massimo Morichelli on bass and voice, Francesco Iannuzzelli on guitar, Massimiliano Valerio on keyboards and Matteo Pozzi on drums.

For a few years in the 90s they proposed a mix of progressive rock and blues with classical influences to audiences in the underground scene of Milan, Italy.

![Preludio all'alba - Cover](/images/small/c/jd_preludio.jpg){.img-left .noshadow}

They released one work on tape, *"Preludio all'alba"* (whose cover is shown here on the left), published by M.A.P. in 1992. Here are a few tracks:

- [Hebel](/audio/Hebel.mp3)
- [Libellule Turchesi](/audio/Libellule%20turchesi.mp3)
- [Innocence](/audio/Innocence.mp3)

Here you can also listen to "Along the War", recorded live at Magia Music Meeting (Milano) on 2 April 1992: [Along the War](/audio/Along%20the%20War.mp3)
