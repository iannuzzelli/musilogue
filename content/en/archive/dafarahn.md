# dafarahn

Dafarahn was a Oud-Bass duo, initiated in 2014 by Francesco Iannuzzelli and the French double bass player Lucile Belliveau. In the past few years they have been performing in London and other European cities in collaboration with various percussionists.

Their reportoire includes modern renditions of classical repertoire from the Iraqi, Persian and Ottoman music traditions, alongside their own original compositions.

![Pomegranate Tiles](/images/small/c/pomegranate_tiles.jpg){.img-left .noshadow} In 2014 they released their first disc, [Pomegranate Tiles](https://dafarahn.musilogue.com/#music), recorded at SOAS Studio and including a selection of Iraqi Maqam and traditional repertoire from the Middle-East.

In 2015 they were selected to perform at Nour Festival, together with the Iranian percussionist Hadi Alizadeh.

More details, audio and video samples can be found on the website https://dafarahn.musilogue.com

![Dafarahn](/images/inline/c/dafarahn.jpg){.hidden}
