
Musilogue is a monologue that would like to become a dialogue via music.

Or, in other words, it's a way for me ([francesco](/en/francesco/)) to share music, writings and information about my music activity, for people to find out about it, and hopefully to make things happen. 

Musilogue is also a music production activity, a website project, possibly some day a platform open to collaboration from others.

For now, it is mainly a space to wander around and find some music of mine.

The website content is not presented in the usual structured way, with navigation bars or search facilities, but it is spread in an open space that can be browsed by clicking on the screen edges, using the keyboard arrow keys or by swiping on a touch-device. 

To orientate yourself, you may want to check the help instructions and the sitemap, both available in the bottom-right section.
