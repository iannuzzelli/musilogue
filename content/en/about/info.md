
# terms & conditions

Website design and implentation by Francesco Iannuzzelli. Source code available on https://gitlab.com/iannuzzelli/musilogue and released as GPLv3. Other software packages are credited in the [README](https://gitlab.com/iannuzzelli/musilogue/blob/master/README.md).

All content (images, text, music, videos) is &copy; Francesco Iannuzzelli.

Music tracks, compositions, scores and other written content such as music analysis and study guides are all released under [Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International Licence](https://creativecommons.org/licenses/by-nc-sa/4.0/), unless otherwise stated.

This means you are free to use, adapt and share them, as far as you give appropriate credit and share them in the same way. However, you cannot use them for commercial purposes. For commercial use, please [get in touch](/en/about/contact/).

There are a few credited images which are not mine (the author is specified in the alt text or in the caption when visible); they are *not* available for re-use. All *un-credited* images are by Francesco Iannuzzelli and, together with those directly credited to myself, are available under the same [Creative Commons Licence](https://creativecommons.org/licenses/by-nc-sa/4.0/). 

This site does not use cookies, so you don't get that boring disclaimer. Third parties such as YouTube and Google may do. 

I am the sole responsible for the content of this website. Complaints, suggestions, amendements, and why not, compliments, [are welcome](/en/about/contact/). 
