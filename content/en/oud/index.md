# oud

I discovered the Oud while travelling in Palestine, and through the influence of my many Arab friends. At the same time, the Oud has brought me to discover many more cultures and friends, and the journey carries on.

Always curious about other music traditions and instruments, I have fallen in love with the sound of the Oud and its history, specialising in the Iraqi style of playing while also exploring neighbouring regions and music traditions. 

Originally a classical guitarist, I have been studying the Oud for more than 20 years with several teachers from Iraq and Turkey, and still today I challenge my technique and playing style while working on transcriptions and analysis of past and current masters.

Several music projects and compositions of mine feature the Oud and are present on this site tagged as [oud](/en/tags/oud/).



