# oud <br>research & <br>materials

In addition to some works written during my studies at SOAS University, various study guides for Oud and music analysis are made available on this site, tagged with the [research](/en/tags/research/) keyword. Please [get in touch](/en/about/contact/) for corrections and suggestions, and of course for research proposals!

