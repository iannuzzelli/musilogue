# oud <br>lessons

I have been teaching music for decades, and I am always happy to work with students who are willing to learn. If you are interested, please prepare a proposal of what you would like to work on, then we can have a free and informal chat, in person or via phone.

Some of the subjects that I enjoy teaching are Oud styles (Arabic, Iraqi, Turkish, Persian), modal music systems in Near & Middle-East, Maqam musical analysis, Iraqi Maqam, ornamentation techniques, _taqasim_ (improvisation), Oud master repertoire and the works of Serif Muhiddin Haydar / Targan and Jamil Bashir.

I can provide individual lessons online (Skype / Zoom / Google Meet) and in person. I prefer non to teach absolute beginners, a minimum familiarity with music and with a music instrument (not necessarily the Oud) is welcome.

If you are interested, please [get in touch](/en/about/contact/)

