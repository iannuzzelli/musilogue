
### content guidelines

Souncloud.com links are sent to the player [Track name](https://soundcloud.com/musilogue/tracktitle)

Internal links are dealt via JavaScript [Oud lessons](/oud/lessons/)

All other external links open in a new window 
[Youtube channel](https://www.youtube.com/channel/UCKqw3EZ_rsWfUYdToOaFcqQ)

Youtube embeds

[embed:https://www.youtube.com/watch?v=KYAH9jEXtLk]


### content metadata

```
{
  "title": "The page title, used for links too",
  "title_short": "The page short title, used by navigation links (optional)",
  "description": "The description, used for meta",
  // the background image from the bg directory
  "background": {
    "image": "bg.jpg"
  },
  // image shown aside in posts, and also used as listing image, from the c directory
  "aside": {
	"image": "aside.jpg",
	"caption": "Caption on mouse-over"
  },
  // vertical level in site plan
  "level": 1,
  // do not show in links (optional, default to false)
  "hidden": true,
  // template to be used, default "default"
  "template": "post",
  // timestamp
  // if hour is specified, the day of the month will not be shown
  "date": "YYYY/MM/DD h:m",
  "tags": [ "tag1", "tag2"]
}
```


### markdown extra

<article markdown="1">

text that will be treated as markdown despite being inside an html tag

<article>

### images

Inline images

![Caption](/images/inline/c/filename.jpg)

also for SVGs

![Caption](/images/inline/c/filename.svg)

Add a *noshadow* class to avoid shadow and zoom-in

![Caption](/images/inline/c/filename.jpg){.noshadow}

Add a *hidden* class to hide images in content (i.e. use them as listing image only)

![Caption](/images/inline/c/filename.jpg){.hidden}


Available recipes:

- full (1920x1920)
- listitme (200x200)
- inline (300x300)
- inlinevert (200x400)
- inlinezoom (800x800)
- gallery (300)
- small (80)



### audio

[Track title](/audio/track.mp3)

[listen to playlist](/audio/playlist.json)

```
[{
        "title": "First track title",
        "filename": "track1.mp3"
},
{
        "title": "Second track titler",
        "filename": "track2.mp3"
}]
```

### documents

[Description](/docs/document.pdf)


### gallery

to add exif tags

exiftool -ImageDescription="Place, Town, Year" -overwrite_original image.jpg
exiftool -Artist="Francesco Iannuzzelli" -overwrite_original image.jpg
