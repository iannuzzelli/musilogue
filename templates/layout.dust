<!DOCTYPE html>
<!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="{page.lang}"> <![endif]-->
<!--[if IE 7]>    <html class="no-js ie7 oldie" lang="{page.lang}"> <![endif]-->
<!--[if IE 8]>    <html class="no-js ie8 oldie" lang="{page.lang}"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="{page.lang}"> <!--<![endif]-->
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="description" content="{page.metadata.description}">
	<meta name="robots" content="index,follow">
	<meta property="fb:app_id" content="325921934847411">
	<meta name="keywords" content="{page.keywords}{?page.metadata.tags}, {#page.metadata.tags}{.}{@sep}, {/sep}{/page.metadata.tags}{/page.metadata.tags}">
	<meta property="author" content="Francesco Iannuzzelli">
	<meta property="copyright" content="Francesco Iannuzzelli">	
	<meta name="twitter:card" content="summary" />
	<meta name="twitter:site" content="@musilogue" />
	<meta property="og:type" content="{page.content.type}">
	<meta property="og:url" content="{page.metadata.url}">
	<meta property="og:title" content="{page.metadata.title}">
	<meta property="og:description" content="{page.metadata.description}">
	<meta property="og:site_name" content="Musilogue">
	<meta property="og:locale" content="{page.locale}">
	<meta property="og:image" content="{page.content.image}">
	{?page.content.metadata.aside.real_width}<meta property="og:image:width" content="{page.content.metadata.aside.real_width}">{/page.content.metadata.aside.real_width}
{?page.content.video}
	<meta property="og:video:url" content="https://www.youtube.com/embed/{page.content.video}">
	<meta property="og:video:secure_url" content="https://www.youtube.com/embed/{page.content.video}">
	<meta property="og:video:type" content="text/html">
	<meta property="og:video:width" content="480">
	<meta property="og:video:height" content="360">
	<meta property="og:video:type" content="application/x-shockwave-flash">
{/page.content.video}
	<meta name="p:domain_verify" content="d3c527c261e3d2b32fe74206e726b6f7"/>
	<meta name="format-detection" content="telephone=no">
	<meta name="geo.placename" content="London, UK">
	<meta name="geo.country" content="gb">
	<meta name="dc.language" content="{page.lang}">
<script type="application/ld+json">
{
	"@context": "http://schema.org",
	"@type": "MusicGroup",
	"name": "Francesco Iannuzzelli",
	"url": "https://musilogue.com",
	"logo": "https://musilogue.com/images/inline/c/musilogue.png",
	"email": "mailto:info@musilogue.com",
	"image": "https://musilogue.com/images/inline/c/musilogue.svg",
	"description": "Musician & composer",
	"sameAs" : [
		"https://www.facebook.com/iannuzzelli",
		"https://instagram.com/musilogue/",
		"https://www.pinterest.co.uk/musilogue/",
		"https://www.linkedin.com/in/iannuzzelli/",
		"https://soundcloud.com/musilogue/",
		"https://www.youtube.com/c/FrancescoIannuzzelli"
	]
}
</script>	
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- build:js /js/main.min.js?CACHE_BUST -->
	<script src="/js/jquery-3.1.0.min.js"></script>
	<script src="/js/soundmanager2-nodebug-jsmin.js"></script>
	<script src="/js/soundmanager2-bar-ui.js"></script>
	<script src="/js/jquery.backstretch.min.js"></script>
	<script src="/js/featherlight.min.js"></script>
	<script src="/js/d3.v4.min.js"></script>
	<script src="/js/cloud.js"></script>
	<script src="/js/main.js"></script>
<!-- /build -->
	
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
	<link rel="alternate" hreflang="{page.metadata.url_lang_code}" href="{page.nav.url_lang}" />
	<link rel="apple-touch-icon-precomposed" sizes="57x57" href="/icon/apple-touch-icon-57x57.png" />
	<link rel="apple-touch-icon-precomposed" sizes="114x114" href="/icon/apple-touch-icon-114x114.png" />
	<link rel="apple-touch-icon-precomposed" sizes="72x72" href="/icon/apple-touch-icon-72x72.png" />
	<link rel="apple-touch-icon-precomposed" sizes="144x144" href="/icon/apple-touch-icon-144x144.png" />
	<link rel="apple-touch-icon-precomposed" sizes="60x60" href="/icon/apple-touch-icon-60x60.png" />
	<link rel="apple-touch-icon-precomposed" sizes="120x120" href="/icon/apple-touch-icon-120x120.png" />
	<link rel="apple-touch-icon-precomposed" sizes="76x76" href="/icon/apple-touch-icon-76x76.png" />
	<link rel="apple-touch-icon-precomposed" sizes="152x152" href="/icon/apple-touch-icon-152x152.png" />
	<link rel="icon" type="image/png" href="/icon/favicon-196x196.png" sizes="196x196" />
	<link rel="icon" type="image/png" href="/icon/favicon-96x96.png" sizes="96x96" />
	<link rel="icon" type="image/png" href="/icon/favicon-32x32.png" sizes="32x32" />
	<link rel="icon" type="image/png" href="/icon/favicon-16x16.png" sizes="16x16" />
	<link rel="icon" type="image/png" href="/icon/favicon-128.png" sizes="128x128" />
	<meta name="application-name" content="Musilogue"/>
	<link rel="canonical" href="{page.metadata.url}" />
	<link rel="alternate" type="application/rss+xml" title="Musilogue posts" href="/posts_{page.metadata.lang}.rss" />
<!-- build:css /css/style.min.css?CACHE_BUST -->
	<link type="text/css" rel="stylesheet" href="/css/featherlight.css" media="screen" />
	<link type="text/css" rel="stylesheet" href="/css/bar-ui.css" media="screen">
	<link type="text/css" rel="stylesheet" href="/css/style.css" media="all">
<!-- /build -->
	<title>{page.metadata.title}</title>
</head>
<body class="s-{page.section_en} a-{page.article_en} l-{page.metadata.level} t-{page.metadata.template}" data-level="{page.metadata.level}" data-sequence="{page.nav.sequence}" data-metadata="{page.metadata_encoded}">
{>"svgsprite"/}
	<div id="container" class="capture">
		<div id="content" class="content" tabindex="1">
			{>"{page.metadata.template}" markdown=page.content.markdown extra=page.content.extra metadata=page.metadata labels=page.content.labels press=page.content.press/}
		</div>
		<div id="content2" class="content" tabindex="2"></div>

		<div id="footer">
			{>"sm2"/}
			{>"navicon" target="home" title="Home" url="/{page.lang}/"/}
			{>"navicon" target="help" title="Help" url="#"/}
			{>"navicon" target="sitemap" title="Sitemap" url="#"/}
			{>"navicon" target="tags" title="Tags" url="/{page.lang}/tags/"/}
			{>"navicon" target="lang" title="{page.metadata.lang_label}" url="{page.nav.url_lang}" hreflang="{page.metadata.url_lang_code}" /}
			<div id="credits">{?page.metadata.background.credits}{page.metadata.background.credits}{/page.metadata.background.credits}</div>
		</div>

		{>"navbutton" target="up" title=page.nav.up.title url=page.nav.up.url/}
		{>"navbutton" target="left" title=page.nav.left.title url=page.nav.left.url/}
		{>"navbutton" target="right" title=page.nav.right.title url=page.nav.right.url/}
		{>"navbutton" target="down" title=page.nav.down.title url=page.nav.down.url/}

		<nav id="navsection">
{?page.navsection}
	{#page.navsection}
			<a {?selected}class="selected"{/selected} href="{url}">{title}</a>
	{/page.navsection}
{/page.navsection}
		</nav>

	</div>
	
<script>
  var _paq = window._paq = window._paq || [];
  _paq.push(['trackPageView']);
  _paq.push(['enableLinkTracking']);
  (function() {
	var u="//analytics.href.org/";
	_paq.push(['setTrackerUrl', u+'matomo.php']);
	_paq.push(['setSiteId', '6']);
	var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
	g.async=true; g.src=u+'matomo.js'; s.parentNode.insertBefore(g,s);
  })();
</script>

</body>
</html>
