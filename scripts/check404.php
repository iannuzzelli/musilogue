<?php
include_once (__DIR__ . '/../lib/content.php');

$debug = isset($argv[1]) && $argv[1]=='debug';

$urls = array();

// facebook events links
ProcessEvents();

// press links
ProcessPress();

// parse posts
ProcessArticles('posts');
ProcessArticles('archive');

$errors = array();
foreach($urls as $url) {
	echo "Checking {$url['destination']}... ";
	if (!(preg_match('/^(http|https|ftp):\/\/(([A-Z0-9][A-Z0-9_-]*)(\.[A-Z0-9][A-Z0-9_-]*)+)(:(\d+))?/i', $url['destination']))) {
		ErrorAdd($url,"Invalid url");
	} else {
		$handle = curl_init($url['destination']);
		curl_setopt($handle,  CURLOPT_RETURNTRANSFER, true);
		curl_setopt($handle,  CURLOPT_NOBODY, true);
		$response = curl_exec($handle);
		/* Check for 404 (file not found). */
		$httpCode = curl_getinfo($handle, CURLINFO_HTTP_CODE);
		echo "$httpCode\n";
		if($httpCode > 400) {
			ErrorAdd($url,$httpCode);
		}
		curl_close($handle);
	}
}

if(count($errors)>0) {
	$msg = count($errors) . " external links failed:\n\n";
	foreach($errors as $error) {
		$msg .= "In ".HOST_LIVE."{$error['origin']}\n{$error['destination']}\nreturned \"{$error['error']}\"\n\n";
	}
	if($debug) {
		echo $msg;
	} else {
		mail('francesco@musilogue.com','External links errors',$msg);
	}
}


function ErrorAdd($url,$description) {
	global $errors;
	$url['error'] = trim($description);
	$errors[] = $url;
}

function AddUrlToCheck($origin,$destination) {
	global $urls;
	if(strpos($destination,'api.soundcloud.com/playlists/')===false) {
		$found = false;
		foreach($urls as $url) {
			if($url['destination'] == $destination) {
				$found = true;
			}
		}
		if(!$found) {
			$urls[] = array('origin'=>$origin,'destination'=>$destination);
		}
	}
}

function ProcessArticles($directory) {
	foreach(Constants::$CONTENT_LANGUAGES as $lang) {
		$articles_files = glob(CONTENT_DIR . "/$lang/$directory/*.md");
		foreach($articles_files as $article_file) {
			$markdown = file_get_contents($article_file);
			$filename = pathinfo($article_file,PATHINFO_FILENAME);
			ExtractUrl($markdown, "/$lang/$directory/$filename/");
		}
	}
}

function ProcessEvents() {
	foreach(Constants::$CONTENT_LANGUAGES as $lang) {
		$events_array = json_decode(file_get_contents(CONTENT_DIR . "/{$lang}/events.json"));
		foreach($events_array as $event) {
			if(isset($event->fb)) {
				AddUrlToCheck("/$lang/events/","https://www.facebook.com/events/{$event->fb}/");
			}
			if(isset($event->description)) {
				ExtractUrl($event->description,"/$lang/events/");
			}
		}
	}
}

function ProcessPress() {
	$press_array = json_decode(file_get_contents(CONTENT_DIR . "/{$lang}/press.json"));
	foreach($press_array as $press) {
		if(isset($press->link)) {
			AddUrlToCheck("/$lang/francesco/press/",$press->link);
		}
	}
}

function ExtractUrl($content,$origin) {
	preg_match_all('#\bhttps?://[^\s()<>]+(?:\([\w\d]+\)|([^[:punct:]\s]|/))#', $content, $matches);
	if(isset($matches[0]) && count($matches[0])>0) {
		foreach($matches[0] as $match) {
			AddUrlToCheck($origin, $match);
		}
	}
}


?>
