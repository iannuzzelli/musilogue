#!/bin/bash

WORKDIR="/var/www/musilogue/master"
BRANCH="master"
LOCKFILE="/tmp/musilogue.lock"
OUTCOME="ERROR"

if [ ! -e $LOCKFILE ]; then
	trap "rm -f $LOCKFILE; exit" INT TERM EXIT
	touch $LOCKFILE

	cd $WORKDIR

	git fetch --all
	git remote update

	printf "Sync data...\n\n"
	rsync -e ssh -azpL --delete --stats --exclude ".*" zen:musilogue/ $WORKDIR/data/

	printf "Checking out branch $BRANCH...\n\n"
	git checkout $BRANCH

	printf "Checking out modified files...\n\n"
	git checkout templates/layout.dust

	printf "Pulling source\n"
	git pull origin $BRANCH

	# Gather version info
	GIT_MSG=$(git log --format=%B -1)
	GIT_REF=$(git log --pretty=%H -1)
	GIT_REF_SHORT=${GIT_REF:0:7}
	GIT_BRANCH="$(git symbolic-ref HEAD 2>/dev/null)" ||
	GIT_BRANCH="(unnamed branch)"     # detached HEAD
	GIT_BRANCH=${GIT_BRANCH##refs/heads/}
	DEPLOY_TS=$(date +"%d/%m/%Y %H:%M:%S")
	VERSION_INFO="$GIT_BRANCH $GIT_REF_SHORT - $DEPLOY_TS"

	printf "Write version as text file...\n"
	echo "$VERSION_INFO" | tee $WORKDIR/pub/version.txt

	# printf "Clear cache...\n\n"
	# rm -r $WORKDIR/cache/*

	cd $WORKDIR

	npm install
	npm update
	npm prune

	rm $WORKDIR/pub/css/style.min.css

	grunt

	printf "Update cache bust with git ref...\n\n"
	sed -i 's/CACHE_BUST/'$GIT_REF_SHORT'/g' $WORKDIR/templates/layout.dust
	
	BUILD=`php -d display_errors=on $WORKDIR/scripts/build.php`
	RETCODE=$?

	if [ "$RETCODE" == 0 ]; then
		OUTCOME="OK"
	fi

	MSG="$VERSION_INFO\n$GIT_MSG\n$BUILD"

	printf "$MSG" | mail -s "musilogue.com deployment $OUTCOME" "francesco@musilogue.com"

	rm $LOCKFILE
	trap - INT TERM EXIT

else
	# do nothing
	echo "Previous autodeploy still running"
fi
