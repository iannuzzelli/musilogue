#!/bin/bash
#
# This scripts sets the permissions for the directories that needs to be writable by the Apache user 
# (www-data.www-data)

CURRENTDIR=`dirname $0`
cd $CURRENTDIR

sudo chown -R www-data.www-data ../cache

sudo find ../cache -type d -exec chmod 777 {} \;
