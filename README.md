musilogue
=========

https://musilogue.com website

## licence

This repo contains both the application code and the website content.

Code is released under [GPLv3](License.txt)

Content is available under [Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International Licence](https://creativecommons.org/licenses/by-nc-sa/4.0/), please check http://musilogue.com/about/info/ for details


## credits

https://jquery.com/

http://fontawesome.io/

https://github.com/scottschiller/SoundManager2/

https://github.com/srobbin/jquery-backstretch

https://github.com/noelboss/featherlight/

https://github.com/vvvmax/unitegallery/

https://github.com/d3/d3

https://gist.github.com/curran/9b73eb564c1c8a3d8f3ab207de364bf4

https://github.com/jkphl/svg-sprite

https://github.com/gruntjs/grunt-contrib-cssmin

https://github.com/dciccale/grunt-processhtml

https://github.com/jasondavies/d3-cloud

https://github.com/Bloafer/dust-php

https://github.com/devgeniem/dust-php

https://github.com/erusev/parsedown

## installation

Possibly needed libraries:

apt-get install php5-dev imagemagick libmagickwand-dev php5-xml php5-mbstring

pecl install imagick

apt-get install php5-imagick

Install the grunt client globally

`npm install -g grunt-cli`

and then run `npm install` in the website root directory

