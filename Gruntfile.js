'use strict';

module.exports = function(grunt) {

	var cssFiles = [
		'pub/css/featherlight.css',
		'pub/css/bar-ui.css',
		'pub/css/style.css'
	];

	var jsFiles = [
		'pub/js/jquery-3.1.0.min.js',
		'pub/js/soundmanager2-nodebug-jsmin.js',
		'pub/js/soundmanager2-bar-ui.js',
		'pub/js/jquery.backstretch.min.js',
		'pub/js/featherlight.min.js',
		'pub/js/d3.v4.min.js',
		'pub/js/cloud.js',
		'pub/js/main.js'
	];

	grunt.initConfig({

		svg_sprite : {
			dist : {
				expand : true,
				cwd : 'content/icons',
				src : [ '**/*.svg' ],
				dest : 'build',
				options : {
					"mode" : {
						"symbol" : {
							inline : true,
							sprite : "svgsprite.dust"
						}
					}
				}
			}
		},
		
		cssmin: {
			target: {
				files: [{
					'pub/css/style.min.css': cssFiles
				}]
			}			
		},
		
		// Minify the JS
		uglify: {
			dist: {
				files: {
					'pub/js/main.min.js': jsFiles
				}
			}
		},
		
		processhtml: {
			options: {
			},
			dist: {
				files: {
					'templates/layout.dust': ['templates/layout.dust']
				}
			}
		}		
	});

	grunt.loadNpmTasks('grunt-svg-sprite');
	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-contrib-cssmin');
	grunt.loadNpmTasks('grunt-processhtml');
	
	grunt.registerTask('default', [ 'svg_sprite', 'cssmin', 'uglify', 'processhtml' ]);
};
